# FROM node:14.17.1-alpine3.11 AS builder

FROM php:7.4-apache

# ENV HTTP_PROXY=http://10.15.2.10:8080
# ENV HTTPS_PROXY=http://10.15.2.10:8080
# ENV http_proxy=http://10.15.2.10:8080
# ENV https_proxy=http://10.15.2.10:8080

RUN docker-php-ext-install pdo pdo_mysql

# update 
# RUN apt-get update
# # install curl 
# RUN apt-get install curl -y
# # get install script and pass it to execute: 
# # RUN curl -sL https://deb.nodesource.com/setup_14.x --proxy http://10.15.2.10:8080 | bash
# RUN curl -sL https://deb.nodesource.com/setup_14.x | bash
# # and install node 
# RUN apt-get install nodejs -y
# # confirm that it was successful 
# RUN node -v
# # npm installs automatically 
# RUN npm -v

# # Copy Over Frontend Except node_modules
# WORKDIR /tmp/front/
# COPY ./front/public/index.html /tmp/front/public/index.html
# COPY ./front/public/favicon.ico /tmp/front/public/favicon.ico
# COPY ./front/public/logo.svg /tmp/front/public/logo.svg
# COPY ./front/public/logoPNG.png /tmp/front/public/logoPNG.png
# COPY ./front/resources /tmp/front/resources
# COPY ./front/src /tmp/front/src
# COPY ./front/package.json /tmp/front/package.json

# Build Frontend
# RUN npm install
# RUN npm run build
# RUN mkdir /tmp/front/build/indscore
# RUN mv ./build/static ./build/indscore/

# Enable SSL
# RUN a2enmod rewrite
# RUN a2enmod ssl
# RUN service apache2 restart

# Copy Over Prod Web and API to Apache
WORKDIR /var/www/html
# RUN mv /tmp/front/build/* ./
COPY ./back/php/api /var/www/html/indscore/api/
RUN rm -rf /var/www/html/.htaccess
RUN chown -R www-data:www-data /var/www/html/
RUN ls -al /var/www/html

RUN ln -sf /dev/stdout /var/log/apache2/access.log
RUN ln -sf /dev/stderr /var/log/apache2/error.log
