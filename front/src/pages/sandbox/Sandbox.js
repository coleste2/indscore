import React, { useState, useEffect } from "react"
import axios from "axios";
import apiBase from "../../components/api"
import { Button, Form, InputGroup } from "react-bootstrap";
import { Dropdown } from "semantic-ui-react"
import Loading from "../../components/Loading"
import { useSnackbar } from "notistack"
import { Typeahead, Token } from "react-bootstrap-typeahead"
import Highlighter from "react-highlight-words"
import { useLocation } from "react-router-dom"
import { List, WindowScroller, AutoSizer, CellMeasurer, CellMeasurerCache } from 'react-virtualized';
import 'react-virtualized/styles.css'; // only needs to be imported once
import { isIE } from "react-device-detect"
import { ListItem, ListItemText, Backdrop, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom"
import { useSelector, useDispatch } from "react-redux"
import { setSandboxSearch } from "../../redux/actions"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../components/Error"

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
}));

const Sandbox = () => {
    return (
        <>
            <ErrorBoundary FallbackComponent={FallBack}>
                <SandboxSearch />
                <SandboxResults />
            </ErrorBoundary>
        </>
    )
}
export default Sandbox

const SandboxResults = () => {
    const [data, setData] = useState(null)
    const search = useSelector(state => state.sandbox_search)
    const { enqueueSnackbar } = useSnackbar();
    const dispatch = useDispatch()
    useEffect(() => {
        if (search && search.rules && search.method) {
            sendSearch()
        }
    }, [search])
    useEffect(() => {
        return () => dispatch(setSandboxSearch(null))
    }, [])

    const sendSearch = async () => {
        setData("wait")
        try {
            var body = {
                id: search.rules.map(item => item.rule_id)
            }
            if (search.method.type !== "specify") {
                body[search.method.type] = search.method.count
            }
            else body.specify = search.method.ids
            const { data } = await axios.post(apiBase + "sandboxScore.php", body)
            listCache.clearAll();
            if (Array.isArray(data) && data.length > 0) {
                setData(data)
            }
            else {
                throw new Error("Sandbox scoring did not return array")
            }
        } catch (error) {
            console.log(error)
            setData(null)
            enqueueSnackbar("Error while completing scoring", { variant: "error" });
        }
    }
    return (
        <div>
            { data && <>
                {data === "wait" ?
                    <Backdrop style={{ color: '#fff', zIndex: 999999999999 }} open={true} >
                        <Loading />
                    </Backdrop>
                    :
                    <ErrorBoundary FallbackComponent={FallBack}>

                        <Results rules={search.rules} data={data} />
                    </ErrorBoundary>
                }
            </>}
        </div>
    )
}

const SandboxSearch = () => {
    const [search, setSearch] = useState({ rules: [], method: { type: "psuedo", count: 25 } })
    const { enqueueSnackbar } = useSnackbar();
    const dispatch = useDispatch()


    //Prevent reload if URL ID parameters
    const { search: urlSearch } = useLocation();
    useEffect(() => {
        if (urlSearch !== "") getData()
    }, [])

    const sendScoreRequest = () => {
        if (search.rules.length < 1) {
            enqueueSnackbar("Please select at least one rule to score with", { variant: "error" });
            return;
        }
        if (search.method.type === "specify" && !search.method.ids) {
            enqueueSnackbar("Please specify evidence packages to score against", { variant: "error" });
            return;
        }
        else if (search.method.type !== "specify" && !search.method.count) {
            enqueueSnackbar("Please specify how many evidence packages you would like to score", { variant: "error" });
            return;
        }
        dispatch(setSandboxSearch(search))
    }

    const getData = async () => {
        try {
            const { data } = await axios.get(apiBase + "getRules.php/" + urlSearch);
            if (Array.isArray(data) && data.length > 0) {
                setSearch({ ...search, rules: data })
            }
        } catch (error) { }
    }

    return (
        <div>
            <h2 style={{ marginTop: "1rem" }}>Test Rules Against Evidence Packages</h2>
            <ErrorBoundary FallbackComponent={FallBack}>
                <RuleTypeahead selected={search.rules} setSelected={(data) => setSearch({ method: search.method, rules: data.sort() })} />
            </ErrorBoundary>
            <ErrorBoundary FallbackComponent={FallBack}>
                <SearchType method={search.method} setMethod={(data) => setSearch({ rules: search.rules, method: data })} />
            </ErrorBoundary>
            <div style={{ display: "flex", justifyContent: "space-between", flexDirection: "row-reverse", alignItems: "flex-center" }}>
                <Button onClick={() => sendScoreRequest()}>Run Test</Button>
            </div>
        </div>
    )
}



const Results = ({ data, rules }) => {
    return (
        <>
            <div style={{ display: "flex", alignItems: "flex-start", justifyContent: "flex-start" }}>
                <h3 className="m-0 p-0">Results</h3>
                {/* {rules.map(item=>item.hi)} */}
            </div>
            <ErrorBoundary FallbackComponent={FallBack}>

                <ResultList data={data} rules={rules} />
            </ErrorBoundary>
        </>)
}

const listCache = new CellMeasurerCache({
    fixedWidth: true,
    defaultHeight: 100
})

const ResultList = ({ data, rules }) => {
    const classes = useStyles();

    return (
        <WindowScroller>
            {({ height, scrollTop }) => (
                <AutoSizer disableHeight>
                    {({ width }) => (
                        <List
                            className={classes.root + " sandboxScroller"}
                            rowCount={data.length + 1}
                            autoHeight
                            scrollTop={scrollTop}
                            width={width}
                            height={height}
                            data={data}
                            rules={rules}
                            deferredMeasurementCache={listCache}
                            rowHeight={listCache.rowHeight}
                            rowRenderer={rowRenderer}
                            overscanRowCount={isIE ? 10 : 5}
                        />
                    )}
                </AutoSizer>
            )}
        </WindowScroller>
    )
}

const rowRenderer = (props) => {
    const { index, parent, key, style } = props;
    const { data, rules } = parent.props

    return (
        <CellMeasurer
            key={key}
            cache={listCache}
            parent={parent}
            columnIndex={0}
            rowIndex={index}
        >
            <div style={style}>
                <Row item={index === 0 ? rules : data[index - 1]} header={index === 0} inset={data[0].type === "evidencePackage"} />
            </div>
        </CellMeasurer>
    );
};

const Row = ({ item, inset, header }) => {
    if (header) {
        return (
            <ErrorBoundary FallbackComponent={FallBack}>
                <div className="sandboxHeader" style={inset ? { marginLeft: "356px" } : { marginLeft: "340px", paddingBottom: "10px" }}>
                    {item.map((rule, index) => {
                        return (<Paper key={index} elevation={4} className="headerPaper" >
                            <ListItemText style={{ marginLeft: "1rem", marginRight: "1rem" }} primary={`Rule: ${rule.name}`} secondary={<Link target="_blank" to={"/rules/" + rule.rule_id}>Rule ID:{" " + rule.rule_id}</Link>} />
                        </Paper>)
                    })}
                </div>
            </ErrorBoundary>
        )
    }
    else if (item.type === "evidencePackage") {

        return (<ListItem>
            <ErrorBoundary FallbackComponent={FallBack}>
                <ListItemText className="sandboxResultEvidence" primary={`Evidence Package`} secondary={
                    <div style={{ display: "flex", flexDirection: "column" }}>
                        <a target="_blank" rel="noopener noreferrer" href={"https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/files/" + item.id}>ID{" " + item.id}</a>
                        <div>Source ID:{" " + item.sourceId}</div>
                        <div>Source Title:{" " + item.sourceTitle}</div>
                    </div>} />
            </ErrorBoundary>
        </ListItem>)
    }
    else if (item.type === "indicator") {
        return <Indicator item={item} inset={inset} />
    }
    else return <div />
}

const Indicator = ({ item, inset }) => {
    return (
        <>
                    <ErrorBoundary FallbackComponent={FallBack}>

            <ListItem>
                <div style={{ display: "flex", alignItems: "flex-start", justifyContent: "flex-start" }}>
                    <Paper elevation={4} style={inset ? { marginLeft: "1rem" } : undefined} className="indicatorPaper" >
                        <ListItemText style={{ marginLeft: "1rem", marginRight: "1rem" }} primary={`Indicator`} secondary={
                            <div style={{ display: "flex", flexDirection: "column" }}>
                                <a target="_blank" rel="noopener noreferrer" href={"https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/indicators/" + item.id}>ID{" " + item.id}</a>
                                <div>Type:{" " + item.valtype}</div>
                            </div>
                        } />
                    </Paper>
                    <div style={{ display: "flex", alignItems: "flex-start" }}>

                        {item.results.map((result, index) => <IndicatorResult result={result} key={index} />)}
                    </div>
                </div>
            </ListItem>
            </ErrorBoundary>
        </>)
}
const IndicatorResult = ({ result }) => {
    var color
    if (result.score === 10) {
        color = "#fff6ba"
    }
    else if (result.score === 30) {
        color = "#ffd76b"
    }
    else if (result.score === 50) {
        color = "#ffa51f"
    }
    else if (result.score === 70) {
        color = "#ff7700"
    }
    else color = "#ff0000"
    return (
        <div className="ml-2 mr-2">
            <Paper elevation={2} style={{ backgroundColor: color }} className="p-2 paperIndicatorResult">
                <div><b>Score:</b> {result.score}</div>
                <div><b>Opinion:</b> {result.opinion}</div>
                <div><b>Score name:</b> {result.response}</div>
                <div><b>Score explanation:</b> {result.reason}</div>
            </Paper>
        </div>
        // <ListItemText style = {{height:"200px", width:"500px",backgroundColor:"red"}} className="ml-3" primary={"Score: " + result.score} secondary={<Link target="_blank" to={"/rules/" + result.id}>Rule ID:{" " + result.id}</Link>} />
    )
}
const SearchType = ({ method, setMethod }) => {
    return (
        <>
            <div className="sandboxForm mt-4">
                <Form.Group className="mr-4 dropdownSearchType" >
                    <InputGroup>
                        <InputGroup.Prepend >
                            <InputGroup.Text>Test Type</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Dropdown value={method.type} className="rulesSelectButton" selection options={testTypeOptions} onChange={(e, v) => setMethod({ ...method, type: v.value })} />
                    </InputGroup>
                </Form.Group>
                {method.type !== "specify" &&
                    <>
                        <Form.Group >
                            <InputGroup>
                                <InputGroup.Prepend >
                                    <InputGroup.Text>Amount of Evidence Packages</InputGroup.Text>
                                </InputGroup.Prepend>
                                <Dropdown value={method.count ? method.count : null} className="countSelectButton" selection options={method.type === "psuedo" ? psuedoCount : randomCount} onChange={(e, v) => setMethod({ ...method, count: v.value })} />
                            </InputGroup>
                        </Form.Group>
                    </>
                }
            </div>
            {method.type === "rapid" &&
                    <Form.Text  muted>
                        NOTE: This selection method will select a sequence of evidence packages will and not select indicators with over 10,000 mentions.</Form.Text>
            }
            {method.type === "random" &&
                    <Form.Text muted>
                        NOTE: If any of the rules being tested use source count, this selection method will likely take a while.</Form.Text>
            }
            {method.type === "specify" &&
                <Form.Group >
                    <Form.Label>Enter Evidence Package IDs in a comma or space delimited list</Form.Label>
                    <Form.Control as="textarea" rows="3" value={method.ids ? method.ids : ""} onChange={(e) => setMethod({ ...method, ids: e.target.value })} />
                    <Form.Text muted>
                        NOTE: Sandbox will only attempt to process the first 100 Evidence Packages provided.</Form.Text>
                </Form.Group>
            }
        </>
    )
}

function RuleTypeahead({ selected, setSelected }) {
    const [loading, setLoading] = useState(false)
    const [ruleOptions, setRuleOptions] = useState([])
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        getRules("", true)
    }, [])

    const getRules = async (text, filter = false) => {
        document.body.style.cursor = "progress";
        try {
            const { data } = await axios.get(apiBase + "getRules.php/?contains=" + text)
            setRuleOptions(data)
        } catch (error) {
            console.log(error)
            enqueueSnackbar("There was an error fetching the data", { variant: "error" });
        }
        setLoading(false)
        document.body.style.cursor = "auto";
    }

    return (
        <Form.Group className="mt-4">
            <InputGroup>
                <InputGroup.Prepend >
                    <InputGroup.Text>Rules</InputGroup.Text>
                </InputGroup.Prepend>

                <Typeahead
                    className="sandboxTypeahead"
                    id="rulesSandboxTyp"
                    filterBy={['rule_id', 'name', 'description', 'status', 'author']}
                    minLength={0}
                    selected={selected}
                    default
                    onChange={(props) => setSelected(props.sort((a, b) => a.rule_id < b.rule_id ? -1 : 1))}
                    isLoading={loading}
                    onInputChange={(text) => getRules(text)}
                    options={ruleOptions}
                    placeholder="Search for rules..."
                    clearButton
                    multiple
                    labelKey="rule_id"
                    renderMenuItemChildren={(option, props) => {
                        return (
                            <>
                                <div className="itemRenderChild">
                                    <div>
                                        <h4 className="mb-1">ID: <Highlighter highlightClassName="highlighter" textToHighlight={option.rule_id} searchWords={[props.text]} /></h4>
                                        <div>Name: <Highlighter highlightClassName="highlighter" textToHighlight={option.name} searchWords={[props.text]} /></div>
                                        <div>Description: <Highlighter highlightClassName="highlighter" textToHighlight={option.description} searchWords={[props.text]} /></div>
                                        <div>Author: <Highlighter highlightClassName="highlighter" textToHighlight={option.author} searchWords={[props.text]} /></div>
                                    </div>
                                    <div style={{ color: "#a5a5a5" }}>{<Highlighter highlightClassName="highlighter" textToHighlight={option.status} searchWords={[props.text]} />}</div>
                                </div>
                            </>
                        )
                    }}
                    renderToken={(option, { onRemove }, index) => {
                        return (
                            <Token
                                key={index}
                                onRemove={onRemove}
                                option={option}>
                                {`${option.name} (ID: ${option.rule_id})`}
                            </Token>)
                    }}
                />
            </InputGroup>

        </Form.Group>
    )
}

const testTypeOptions = [
    { key: '1', text: 'Psuedorandom Selection', value: 'psuedo' },
    { key: '2', text: 'Rapid Random Selection', value: 'rapid' },
    { key: '3', text: 'True Random Selection', value: 'random' },
    { key: '4', text: 'Specify Evidence Packages', value: 'specify' },
]

const randomCount = [
    { key: '1', text: '1', value: 1 },
    { key: '2', text: '5', value: 5 },
    { key: '3', text: '10', value: 10 },
    { key: '4', text: '25', value: 25 },
]

const psuedoCount = [
    { key: '1', text: '1', value: 1 },
    { key: '2', text: '5', value: 5 },
    { key: '3', text: '10', value: 10 },
    { key: '4', text: '25', value: 25 },
    { key: '5', text: '50', value: 50 },
    { key: '6', text: '100', value: 100 },
    { key: '7', text: '250', value: 250 },
    { key: '8', text: '500', value: 500 },

]