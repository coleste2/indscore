import React from "react"
import Sandbox from "./Sandbox"
import "./Sandbox.css"
import {useParams}from "react-router-dom"

function DisplaySandbox() {
const params = useParams()
    return (
        <div className="ScoreIndicatorsContainer mb-4">
            {/* Render results...the point of this component is to handle anything that would cause a change
            in the original data like a change in the scoring rule*/}
            <Sandbox baseSelected = {params && params.id ?[params.id] : [] } />
                    </div>
    )
}
export default DisplaySandbox

