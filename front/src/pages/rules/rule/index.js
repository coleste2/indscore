import React from "react"
import { useParams } from "react-router-dom"
import Rule from "./Rule"
import "./Rule.css"
function DisplayRule() {
    const params = useParams()
    return (
        <div className="ScoreIndicatorsContainer mb-4">
            {/* Render results...the point of this component is to handle anything that would cause a change
            in the original data like a change in the scoring rule*/}
            <Rule id={params.id} />
        </div>
    )
}
export default DisplayRule
