import React, { useState, useEffect } from "react"
import axios from "axios";
import apiBase from "../../../components/api"
import Error from "../../../components/Error"
import Loading from "../../../components/Loading"
import { useSnackbar } from "notistack"
import RuleGraph from "../../../components/RuleGraph"
import { StatusModal } from "../Rules"
import { Button, FormControl } from "react-bootstrap";
import { Comment, Header, Popup, Button as SemanticButton } from 'semantic-ui-react'
import { useHistory } from "react-router-dom"
import { useSelector } from "react-redux"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../../components/Error"

function Rule({ id }) {

    const [data, setData] = useState(null);
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        getData()
    }, [])

    const getData = async () => {

        document.body.style.cursor = "progress";

        try {
            const { data } = await axios.get(apiBase + "getRules.php/?id=" + id + "&full=true")
            setData(data)
        } catch (error) {
            setData(error)
            enqueueSnackbar("There was an error fetching the data", { variant: "error" });
        }
        document.body.style.cursor = "auto";

    }

    return (
        <div className="ScoreIndicatorsContainer mb-4">

            {data === null ? <Loading status={"Fetching rule " + id} />
                :
                data.length === 0 || data.error ? <Error error="400" title="Bad Request" message={data.error ? data.error.toString() : "That ID does not exist"} />
                    :
                    <>
                        <ErrorBoundary FallbackComponent={FallBack}>
                            <RuleRender id={id} update={() => getData()} data={data} />
                        </ErrorBoundary>
                    </>
            }
        </div>)

}
export default Rule;

const RuleRender = ({ data, update, id }) => {
    const [statusModal, setStatusModal] = useState(null)
    const history = useHistory()

    return (
        <ErrorBoundary FallbackComponent={FallBack}>

            <div className="rulePageContainer">
                <div className="ruleTitle">
                    <h2>Rule {data.name}</h2>
                    <p className="ruleChars">ID: {id} ● Author: {data.analyst} ● Status: {data.status}</p>
                    <p>Description: {data.description}</p>

                </div>
                <div className="ruleActionsContainer">
                    <Popup className="dropdownTooltip" content='Change the status of the rule' size='mini' inverted position='top center' trigger={
                        <Button className="mr-3" onClick={() => setStatusModal({ id: id, status: data.status })}>Set Status</Button>} />
                    <Popup className="dropdownTooltip" content='Bring the rule into the sandbox for testing' size='mini' inverted position='top center' trigger={
                        <Button onClick={() => history.push("/sandbox/?id[]=" + id)}>Open in Sandbox</Button>} />

                </div>
                <ErrorBoundary FallbackComponent={FallBack}>

                    <div className="ruleGraphContainer">
                        <RuleGraph rules={data.rule.rule} />
                    </div>
                </ErrorBoundary>
                <div>
                    <Header as='h3' dividing>
                        Status History
    </Header>
                    <ErrorBoundary FallbackComponent={FallBack}>
                        {data.history.map((item, index) => {
                            return (
                                <div key = {index} style={{ marginBottom: "1rem" }}>
                                    <b>New Status: {item.new_status}</b>
                                    <Comment.Group style={{ margin: ".35rem" }}>
                                        <Comment key={index}>
                                            {/* <Comment.Avatar src='https://react.semantic-ui.com/images/avatar/small/matt.jpg' /> */}
                                            <Comment.Content>
                                                <Comment.Author as='a'>{item.analyst}</Comment.Author>
                                                <Comment.Metadata>
                                                    <div>{new Date(item.posted).toDateString()} at {formatAMPM(new Date(item.posted))}</div>
                                                </Comment.Metadata>
                                                <Comment.Text>{item.message}</Comment.Text>
                                            </Comment.Content>
                                        </Comment>
                                    </Comment.Group>
                                </div>
                            )
                        })}
                    </ErrorBoundary>
                </div>

                <div style={{ marginTop: "3rem" }}>
                    <Header as='h3' dividing>
                        Comments
    </Header>
                    <ErrorBoundary FallbackComponent={FallBack}>

                        {data.comments.map((item, index) => {
                            return (
                                <ErrorBoundary FallbackComponent={FallBack}>

                                    <Comment.Group style={{ margin: ".5rem" }}>
                                        <Comment key={index}>
                                            {/* <Comment.Avatar src='https://react.semantic-ui.com/images/avatar/small/matt.jpg' /> */}
                                            <Comment.Content>
                                                <Comment.Author as='a'>{item.analyst}</Comment.Author>
                                                <Comment.Metadata>
                                                    <div>{new Date(item.posted).toDateString()} at {formatAMPM(new Date(item.posted))}</div>
                                                </Comment.Metadata>
                                                <Comment.Text>{item.message}</Comment.Text>
                                            </Comment.Content>
                                        </Comment>
                                    </Comment.Group>
                                </ErrorBoundary>
                            )
                        })}
                    </ErrorBoundary>
                    <div>
                        <ErrorBoundary FallbackComponent={FallBack}>

                            <WriteComment first={data.comments.length === 0} rule_id={id} update={() => update()} />
                        </ErrorBoundary>
                    </div>
                </div>
                <ErrorBoundary FallbackComponent={FallBack}>

                    <StatusModal update={() => update()} setStatusModal={(data) => setStatusModal(data)} statusModal={statusModal} />
                </ErrorBoundary>
            </div >
        </ErrorBoundary >
    )
}


const WriteComment = ({ first, rule_id, update }) => {
    const [value, setValue] = useState("")
    const { enqueueSnackbar } = useSnackbar();

    const post = () => {
        if (value === "") {
            return
        }
        try {
            axios.post(apiBase + "createComment.php", { message: value, rule_id: rule_id })
            setValue("")
            update()
        } catch (error) {
            enqueueSnackbar("There was an error posting your comment", { variant: "error" });
        }
    }
    return (
        <>
            <FormControl as="textarea" rows="3" value={value} onChange={(e) => setValue(e.target.value)} />
            <SemanticButton style={{ marginTop: "1rem" }} content={first ? "Post First Comment" : 'Post Reply'} labelPosition='left' icon='edit' primary onClick={() => post()} />
        </>
    )
}
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
