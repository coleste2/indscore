import React, { useEffect, useState, useRef } from "react"
import apiBase from '../../components/api'
import axios from "axios"
import { useSnackbar } from "notistack"
import Loading from "../../components/Loading"
import { Modal, Button, Spinner, FormControl, FormLabel } from "react-bootstrap"
import { ListItem, ListItemText, Checkbox } from "@material-ui/core"
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux"
import { Popup, Input, Icon as SemainticIcon, Label } from "semantic-ui-react"
import Highlighter from "react-highlight-words";
import Icon from "../../bootstrap/icons"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../components/Error"

const Rules = () => {

    const [rules, setRules] = useState(null)
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        getRules()
    }, [])

    const getRules = async () => {
        document.body.style.cursor = "progress";
        try {
            let { data } = await axios.get(apiBase + "getRules.php")
            for (var i = 0; i < data.length; i++) {
                data[i].created = new Date(data[i].created).toDateString()
            }
            setRules(data)
        }
        catch (err) {
            setRules(err)
            enqueueSnackbar("There was an error fetching the data", { variant: "error" });
        }
        document.body.style.cursor = "auto";
    }

    return (<div>
        {rules === null ? <Loading status="Fetching rules" />
            :
            <ErrorBoundary FallbackComponent={FallBack}>
                <RulesList data={rules} update={() => getRules()} />
            </ErrorBoundary>}
    </div>)
}
export default Rules


const RulesList = ({ data, update }) => {
    const [statusModal, setStatusModal] = useState(null)
    const [filteredData, setFilteredData] = useState(data)
    const [filter, setFilter] = useState("")
    const [exportArr, setExportArr] = useState(null)
    const history = useHistory()
    const filterFunc = (val) => {
        setFilter(val)
        setFilteredData(data.filter(item => objContains(item, val)))
    }
    const queryify = (arr) => {
        var str = ""
        for (var i = 0; i < arr.length; i++) {
            if (i !== 0) {
                str += "&"
            }
            str += ("id[]=" + arr[i])
        }
        return str;
    }
    return (<>
        <ErrorBoundary FallbackComponent={FallBack}>
            <div style={{ display: "flex", justifyContent: "space-between", alignItems: "flex-start", height: "1.5rem", marginBottom: "1.75rem" }}>
                <h2 style={{ marginTop: "1rem" }}>List of Rules</h2>
                <div style={{ display: "flex", alignItems: "center" }}>
                    <div style={{ marginRight: ".5rem" }}>
                        <Label as='a' onClick={() => exportArr === null ? setExportArr([]) : setExportArr(null)}>
                            {exportArr === null && <SemainticIcon name='external alternate' />}{exportArr === null ? "Export" : "Cancel"}</Label>

                        {exportArr !== null && <Label as='a' color='blue' onClick={() => history.push("/sandbox/?" + queryify(exportArr))}>
                            <SemainticIcon name='external alternate' />Open In Sandbox</Label>}
                    </div>
                    <Input icon='search' iconPosition='left' placeholder='Search...' value={filter} onChange={(e, v) => filterFunc(v.value)} />

                </div>
            </div>
            <div className="ruleContainer">
                {filteredData.length !== 0 ?
                    filteredData.map((item, index) => <Rule exportArr={exportArr} setExportArr={(data) => setExportArr(data)} key={item.rule_id+item.status} item={item} highlight={filter} setStatusModal={(data) => setStatusModal(data)} />)
                    :
                    <div style={{ display: 'flex', flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
                        <Icon type="frown glass" />
                        <h1>No Rules Found</h1>
                    </div>
                }
                <StatusModal setStatusModal={(data) => setStatusModal(data)} statusModal={statusModal} update={() => update()} />
            </div >
        </ErrorBoundary>
    </>
    )
}
export const StatusModal = ({ statusModal, setStatusModal, update }) => {
    const [loading, setLoading] = useState(false)
    const [input, setInput] = useState("")
    const [selected, setSelected] = useState(null)
    const inputRef = useRef(null)
    const user = useSelector(state => state.userInfo)
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    const setStatus = async (status) => {

        if (statusModal && status === statusModal.status) {
            setStatusModal(null)
            return
        }
        if (!selected || input.length > 200) return;
        if (input === "") {
            enqueueSnackbar("Please provide an explanation for the change", { variant: "error" });
            if (inputRef.current) {
                inputRef.current.focus()
            }
            return
        }
        setLoading(true)
        document.body.style.cursor = "progress";
        try {
            var numStatus = 0;
            if (selected === "Active") numStatus = 1
            else if (selected === "Trash") numStatus = -1
            else if (selected === "Retired") numStatus = 2

            var { data } = await axios.post(apiBase + "setRuleStatus.php", { id: statusModal.id, status: numStatus, explanation: input })
            closeSnackbar()
            if (data.error){
                enqueueSnackbar(data.error, { variant: "error" });
            }
            else{
                enqueueSnackbar(data + selected, { variant: "success" });
            }

            setSelected(null)
            setInput("")
            update()
        }
        catch (err) {
            enqueueSnackbar("Server error", { variant: "error" });
        }
        setLoading(false)
        setStatusModal(null)
        document.body.style.cursor = "auto";
    }
    var content = "Submit"
    if (!selected) content = "A status must be selected"
    if (statusModal && selected === statusModal.status) content = "Select a status other than the current one"
    if (input.length > 200) content = "Change explanation must be no more than 200 chars"

    return (
        <ErrorBoundary FallbackComponent={FallBack}>
            <Modal show={Boolean(statusModal)} onHide={() => setStatusModal(null)} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Rule {statusModal ? statusModal.id : ""} Status</Modal.Title></Modal.Header>
                <div className="pl-4 pr-4 mb-2">
                    <FormLabel className="mt-2">User Name</FormLabel>
                    <FormControl disabled value={user.username} />
                    <FormLabel className="mt-3">Reason for change <span style={{ color: "red" }}>*</span></FormLabel>
                    <FormControl ref={inputRef} as="textarea" rows="3" value={input} onChange={(e) => setInput(e.target.value)} placeholder="Why are you changing the status of this rule?" className={input.length > 200 ? " error" : ""} />
                    {input.length > 200 && <div style={{ color: "red" }}>Explanation cannot be longer than 200 chars. You are currently at {input.length}</div>}
                </div>
                {["Trash", "Draft", "Active", "Retired"].map((text, index) => {
                    const status = statusModal ? statusModal.status : ""
                    return (<ListItem selected={selected === text} key={index} disabled={(text === "Retired" && status !== "Retired") || (status === "Active" && text !== "Active") || ((text === "Trash" || text === "Draft") && status === "Retired")} autoFocus button onClick={() => setSelected(text)}>
                        <ListItemText primary={text} className={statusModal && text === statusModal.status ? "bold" : ""} />
                    </ListItem>)
                })}
                <Modal.Footer>
                    {loading ? <Spinner size="md" animation="border" /> : <Button variant="secondary" onClick={() => setStatusModal(null)}>Close</Button>}
                    <Popup position='top center' inverted content={content} className="inputTooltip" trigger={
                        <Button variant="success" onClick={() => setStatus()} className={!selected || (statusModal && selected === statusModal.status) || input.length > 200 ? "buttonDisable" : ""}>Submit</Button>
                    } />
                </Modal.Footer>
            </Modal>
        </ErrorBoundary>
    )
}

const Rule = ({ item, setStatusModal, highlight, exportArr, setExportArr }) => {
    const history = useHistory()

    const handleCheck = () => {
        if (exportArr.indexOf(item.rule_id) === -1) {
            setExportArr([...exportArr, item.rule_id])
        }
        else {
            setExportArr(exportArr.filter(id => id !== item.rule_id))
        }
    }

    return (
        <ErrorBoundary FallbackComponent={FallBack}>
            <div className="ruleBody" onClick={() => exportArr === null ? undefined : handleCheck()}>
                <div style={{ display: "flex" }}>
                    {exportArr !== null && <div style={{ display: "flex", alignItems: "center" }}>
                        <Checkbox
                            checked={exportArr.indexOf(item.rule_id) !== -1}
                            color='primary'
                        />
                    </div>}
                    <div className="nameDescRule">
                        <h5 className="ruleName" style={{ pointerEvents: "all" }} onClick={() => history.push("rules/" + item.rule_id)}>Name:{" "}<Highlighter
                            highlightClassName="highlighter"
                            searchWords={[highlight]}
                            textToHighlight={item.name}
                        />
                        </h5>
                        <div className="ruleDescription">Description:{" "}<Highlighter
                            highlightClassName="highlighter"
                            searchWords={[highlight]}
                            textToHighlight={item.description}
                        /></div>
                        <div className="ruleDescription">Author:{" "}<Highlighter
                            highlightClassName="highlighter"
                            searchWords={[highlight]}
                            textToHighlight={item.author}
                        /></div>
                    </div>
                </div>
                <div className="statusRulesFlex" style={{ pointerEvents: "all" }}>
                    <div className="statusLink" onClick={() => setStatusModal({ id: item.rule_id, status: item.status })}>Status:{" "}<Highlighter
                        highlightClassName="highlighter"
                        searchWords={[highlight]}
                        textToHighlight={item.status}
                    /></div>
                    <div>Id:{" "}<Highlighter
                        highlightClassName="highlighter"
                        searchWords={[highlight]}
                        textToHighlight={item.rule_id}
                    /></div>
                    <div>Date:{" "}<Highlighter
                        highlightClassName="highlighter"
                        searchWords={[highlight]}
                        textToHighlight={item.created}
                    /></div>
                </div>
            </div>
        </ErrorBoundary>
    )
}

const objContains = (obj, str) => {
    if (obj.rule_id.indexOf(str) !== -1) {
        return true;
    }
    if (obj.status.toLowerCase().indexOf(str.toLowerCase()) !== -1) {
        return true;
    }
    if (obj.author.toLowerCase().indexOf(str.toLowerCase()) !== -1) {
        return true;
    } if (obj.description.toLowerCase().indexOf(str.toLowerCase()) !== -1) {
        return true;
    } if (obj.name.toLowerCase().indexOf(str.toLowerCase()) !== -1) {
        return true;
    }
    if (obj.created.toLowerCase().indexOf(str.toLowerCase()) !== -1) {
        return true;
    }
    return false;
}