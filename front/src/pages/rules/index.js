import React from "react"
import "./Rules.css"

import Rules from "./Rules"
function DisplayRules() {

    return (
        <div className="ScoreIndicatorsContainer mb-4">
            {/* Render results...the point of this component is to handle anything that would cause a change
            in the original data like a change in the scoring rule*/}
            <Rules />
                    </div>
    )
}
export default DisplayRules

