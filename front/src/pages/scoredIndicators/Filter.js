import React, { useState } from "react"
import FilterModal from "./FilterModal"
import { Button } from "react-bootstrap"
import Icon from "../../bootstrap/icons"
import { useSelector, useDispatch } from "react-redux"
import Cookie from "./miniComponents/Cookie"
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import { setFilter } from "../../redux/actions"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../components/Error"

const Filter = ({ }) => {

    //Create state to display the fitler modal or not
    const [modal, setModal] = useState(false)
    //Get the filter from the store
    const filter = useSelector(state => state.results_filter)
    return (
        <div className="filterSegment">
            <ErrorBoundary FallbackComponent={FallBack}>

                {Object.keys(filter).length === 0 &&
                    //If there are no keys in the filter, then diplay message hat there is no active filter
                    <>
                        <h3>No Active Filter</h3>
                        <div className="displayIcon mr-2">
                            {/*Button to  open filter*/}
                            <Button onClick={() => setModal(true)} style={{ width: "max-content" }} variant="outline-dark"><span><Icon type="funnel" /></span> Create Filter</Button>
                        </div>
                    </>}

                {Object.keys(filter).length !== 0 && <>
                    <h3>Active Filter</h3>
                    <div className="filterHolder">{Object.keys(filter).map((item, index) => {
                        var prop = { key: item, value: filter[item] }
                        //If filter comes in format of array then map it
                        if (prop.key === "indicatorTextFilter") return (
                            prop.value.map((point, index1) => {
                                return <Cookie key={"b" + index1} itemKey={prop.key} itemValue={point} index={index1} />
                            }))
                        //else handle the filter itself
                        else return <Cookie key={"a" + index} itemKey={prop.key} itemValue={prop.value} index={index} />

                    })}</div>
                    <div className="displayIcon mt-3">
                        {/*Button to  open filter*/}
                        <Button onClick={() => setModal(true)} style={{ width: "max-content" }} variant="outline-dark"><span><Icon type="funnel" /></span> Edit Filter</Button>

                        {/*Button to clear filter--defined below--seperate becaus it has a click away listener so that does not need to be defined in this large area*/}
                        <ClearButton />
                    </div>
                </>}
                {/*This is the filter modal origin */}
            </ErrorBoundary>
            <ErrorBoundary FallbackComponent={FallBack}>
                <FilterModal modal={modal} setModal={(data) => setModal(data)} />
            </ErrorBoundary>



        </div>
    )
}
export default Filter

const ClearButton = () => {
    const [confirm, setConfirm] = useState(false)
    const dispatch = useDispatch()
    return (<>
        {confirm === false ?
            <Button onClick={() => setConfirm(true)} variant="outline-dark" className="ml-4" style={{ width: "max-content" }}><span><Icon type="trash" /></span> Clear Filter</Button>
            :
            <ClickAwayListener onClickAway={() => setConfirm(false)}>

                <Button variant="danger" onClick={() => dispatch(setFilter({}))} className="ml-4" style={{ width: "max-content" }}><span><Icon type="trash" /></span> Confirm Clear</Button>
            </ClickAwayListener>}
    </>)
}