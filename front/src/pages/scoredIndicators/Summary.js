import React, { useEffect, useState } from "react"
import { RadarChart, ScoreFrequencyChart, ScoreOverTimeChart } from "./miniComponents/Charts"
import { Accordion } from 'semantic-ui-react'
import { Stats } from "./miniComponents/Stats"
import Historics from "./miniComponents/Historics"
import apiBase from "../../components/api"
import axios from "axios"
import { useSnackbar } from "notistack"
import { useSelector } from "react-redux"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../components/Error"
import { Element } from 'react-scroll'
import { draw } from "patternomaly"

function Summary({ }) {
    const filter = useSelector(state => state.results_filter)
    //generate state to store stats for orignal data and the filtered data 
    const { enqueueSnackbar } = useSnackbar();

    const [original, setOriginal] = useState(null)
    const [fetching, setFetching] = useState(true)
    const [nonOriginal, setNonOriginal] = useState(null)


    // Clean original data for statistical analysis and prep it to be displayed in charts
    useEffect(() => {
        getDataOG()
    }, [])

    useEffect(() => {
        if (Object.keys(filter).length === 0) return
        getData()
    }, [filter])

    const getDataOG = async () => {
        setFetching(true)
        document.body.style.cursor = "progress";
        try {
            const data = await Promise.all([axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&bar=true`), axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&plot=true`), axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&statsp1=true`), axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&statsp2=true`), axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&quartiles=true`), axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&historics=true`), axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&radar=true`)])
            var obj = { bar: data[0].data, plot: data[1].data, stats: { ...data[2].data, ...data[3].data }, quartiles: data[4].data, historics: data[5].data, radar: data[6].data }
            setOriginal(obj)
        }
        catch (error) {
            console.log(error)
            enqueueSnackbar("There was an error fetching the data", { variant: "error" });
        }
        document.body.style.cursor = "auto";
        setFetching(false)
    }

    const getData = async () => {
        setFetching(true)
        document.body.style.cursor = "progress";
        try {
            const data = await Promise.all([axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&bar=true&filter=${JSON.stringify(filter)}`), axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&plot=true&filter=${JSON.stringify(filter)}`), axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&statsp1=true&filter=${JSON.stringify(filter)}`), axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&statsp2=true&filter=${JSON.stringify(filter)}`), axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&quartiles=true&filter=${JSON.stringify(filter)}`), axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&historics=true&filter=${JSON.stringify(filter)}`), axios.get(apiBase + `scoredIndicators.php/?numbersOnly=true&radar=true&filter=${JSON.stringify(filter)}`)])
            var obj = { bar: data[0].data, plot: data[1].data, stats: { ...data[2].data, ...data[3].data }, quartiles: data[4].data, historics: data[5].data, radar: data[6].data }
            setNonOriginal(obj)
        } catch (error) {
            console.log(error)
            enqueueSnackbar("There was an error fetching the data", { variant: "error" });
        }
        document.body.style.cursor = "auto";
        setFetching(false)
    }
    // Clean filtered data for statistical analysis and prep it to be displayed in charts

    const multi = Object.keys(filter).length !== 0 && nonOriginal

    const panels = [
        {
            key: "charts", title: { content: (<Element name="Results" className="homeTabTitle"><h2 className="homeTabTitle mt-4">Results</h2></Element>) }, content: {
                content: (<div style={{ position: "relative" }}>
                    {/* Show both the bar chart and the time series chart*/}
                    <div style={{ display: "flex", justifyContent: "space-between" }}>
                        <ErrorBoundary FallbackComponent={FallBack}>
                            <ScoreFrequencyChart fetching={fetching} data={original ? (!multi ? originalStateCount(original) : comboStateCount(original, nonOriginal)) : originalStateCount(null)} />
                        </ErrorBoundary >
                        <ErrorBoundary FallbackComponent={FallBack}>
                            <RadarChart fetching={fetching} data={original ? (!multi ? originalStateRadar(original) : comboStateRadar(original, nonOriginal)) : originalStateRadar(null)} />
                        </ErrorBoundary >
                    </div>
                    <ErrorBoundary FallbackComponent={FallBack}>
                        <ScoreOverTimeChart fetching={fetching} data={original ? (!multi ? originalStateTime(original) : ComboStateTime(original, nonOriginal)) : null} />
                    </ErrorBoundary>

                </div >)
            }
        },
        {
            key: "historics", title: { content: (<Element name="Historics" className="homeTabTitle"><h2 className="homeTabTitle">Historics</h2></Element>) }, content: {
                content: (<div style={{ position: "relative" }}>
                    <ErrorBoundary FallbackComponent={FallBack}>
                        <Historics fetching={fetching} original={original} multipleBlocks={Object.keys(filter).length !== 0} nonOriginal={!multi ? undefined : nonOriginal} />
                    </ErrorBoundary>
                </div>)
            }
        },
        {
            key: "stats", title: { content: (<Element name="Statistics" className="homeTabTitle"><h2 className="homeTabTitle">Statistics</h2></Element>) }, content: {
                content: (<div style={{ position: "relative" }}>
                    <ErrorBoundary FallbackComponent={FallBack}>
                        <Stats fetching={fetching} original={original} multipleBlocks={Object.keys(filter).length !== 0} nonOriginal={!multi ? undefined : nonOriginal} />
                    </ErrorBoundary>
                </div>)
            }
        }
    ]
    //Render a collapsable set of tabs with the with the charts and statistics
    return (<><Accordion className="filterAccordian" defaultActiveIndex={[0, 1, 2]} panels={panels} fluid exclusive={false} /></>
    )
}

export default Summary


//Below this line is simply configuration settings for the bar chart and the time series charts
//They both have 2 versions...one to show if the data is filtered and one to show if the data is not filtered
//The difference is that the filtered version will have both the original data and the temp version of the data
//as opposed to just the original data
const originalStateRadar = (original) => {
    return {
        labels: ["Domain", "Email", "File", "HTTPRequest", "IP", "IPv6", "Mutex", "String", "URL"],
        datasets: [buildRadar(original, "Regular")]
    }
}
const comboStateRadar = (original, nonOriginal) => {
    return {
        labels: ["Domain", "Email", "File", "HTTPRequest", "IP", "IPv6", "Mutex", "String", "URL"],
        datasets: [buildRadar(nonOriginal, "Filtered"), buildRadar(original, "Original"),]
    }
}
const originalStateCount = (original) => {
    return {
        labels: [["10", "Strongly Disagree"], ["30", "Disagree"], ["50", "Netrual"], ["70", "Agree"], ["90", "Strongly Agee"],],
        datasets: [
            {
                per: original && original.bar ? original.bar.per : 0,
                label: 'Indicators',
                borderColor: 'rgba(0,0,0,1)',
                borderWidth: 2,
                backgroundColor: ["#fff6ba", "#ffd76b", "#ffa51f", "#ff7700", "#ff0000"],
                data: original && original.bar ? original.bar.res : 0
            }
        ]
    }
}
const comboStateCount = (original, nonOriginal) => {
    return {
        labels: [["10", "Strongly Disagree"], ["30", "Disagree"], ["50", "Netrual"], ["70", "Agree"], ["90", "Strongly Agree"],],

        datasets: [
            {
                per: original && original.bar ? [...original.bar.per] : 0,
                label: 'Original Indicators',
                backgroundColor: "rgba(220,220,220, .1)",
                borderWidth: 2,
                borderColor: 'rgba(0,0,0,1)',
                data: original && original.bar ? [...original.bar.res] : 0
            },
            {
                per: nonOriginal && nonOriginal.bar ? nonOriginal.bar.per : 0,
                label: 'Filtered Indicators',
                backgroundColor: ["#fff6ba", "#ffd76b", "#ffa51f", "#ff7700", "#ff0000"],
                data: nonOriginal && nonOriginal.bar ? nonOriginal.bar.res : 0
            }
        ]
    }
}
const originalStateTime = (original) => {
    return {
        datasets: [...buildData(original, "reg")]
    }
}
const ComboStateTime = (original, nonOriginal) => {
    return {
        datasets: [...buildData(nonOriginal, "new"), ...buildData(original, "extraneous")]
    }
}


const buildData = (data, type) => {
    return data.plot.map(item => {
        var obj = {}
        obj.fill = false
        if (type === "extraneous") obj.label = `Original Data: Score of ${item.score}`
        else if (type === "new") obj.label = `Filtered Data: Score of ${item.score}`
        else if (type === "reg") obj.label = `Data: Score of ${item.score}`
        if (type === "extraneous") {
            obj.pointBackgroundColor = "rgba(220,220,220, 1)"
        }
        else {
            obj.pointBackgroundColor = function (context) {
                try {
                    var score = context.dataset.label.slice(-2)
                    var colors = { _10: "#fff6ba", _30: "#ffd76b", _50: "#ffa51f", _70: "#ff7700", _90: "#ff0000" }
                    return colors["_" + score]
                } catch (error) {
                    return "blue"
                }
            }
        }
        if (type === "extraneous") {
            obj.pointRadius = 2
        }
        else {
            obj.pointRadius = function (context) {
                try {
                    var tick = context.chart.scales["x-axis-1"].ticks[0]
                    if (tick.toLowerCase().indexOf("am") === -1 && tick.toLowerCase().indexOf("pm") === -1) return 2
                    else return 4
                } catch (error) {
                    return 3
                }
            }
        }
        if (type === "extraneous") obj.pointBorderColor = "rgba(0,0,0,0)"
        else obj.pointBorderColor = "rgba(0,0,0,1)"

        if (type === "extraneous") obj.pointBorderWidth = 0
        else obj.pointBorderWidth = 1

        obj.borderWidth = 2

        if (type === "extraneous") {
            obj.borderColor = "rgba(220,220,220, .8)"
        }
        else {
            obj.borderColor = function (context) {
                try {
                    var score = context.dataset.label.slice(-2)
                    var colors = { _10: "#fff6ba", _30: "#ffd76b", _50: "#ffa51f", _70: "#ff7700", _90: "#ff0000" }
                    return colors["_" + score]
                } catch (error) {
                    return "blue"
                }
            }
        }
        obj.data = item.data.map(pair => {
            pair.x = new Date(pair.x)
            return pair
        })
        return obj
    })
}

const buildRadar = (data, type) => {
    var obj = {}

    obj.label = type
    obj.counts = data && data.radar ? data.radar.counts : 0
    obj.ratios = data && data.radar ? data.radar.ratios : 0
    obj.data = data && data.radar ? data.radar.scores : 0
    obj.pointRadius = (chart) => {
        return (chart.dataset.ratios[chart.dataIndex] * 9) + 1
    }
    obj.pointHoverRadius = (chart) => {
        return (chart.dataset.ratios[chart.dataIndex] * 9) + 2
    }
    obj.pointBackgroundColor = type === "Original" ? "rgb(240,240,240)" : (chart) => {
        var score = chart.dataset.data[chart.dataIndex]
        if (score < 20) return "#fff6ba";
        else if (score < 40) return "#ffd76b";
        else if (score < 60) return "#ffa51f";
        else if (score < 80) return "#ff7700";
        else return "#ff0000";
    }
    obj.pointBorderColor = type === "Original" ? "rgb(200,200,200)" : "rgb(0,0,0)"
    obj.pointBorderWidth = 1
    obj.borderColor = "rgb(0,0,0)"//type === "original" ? "rgb(200,200,200)" : "rgba(0, 0, 0, 0.1)"
    obj.borderWidth = 0.5

    if (type !== "Original") {
        obj.backgroundColor = draw('diagonal', 'rgba(200, 200, 200, .4)')//    type === "original" ? "rgba(235,235,235, .65)" : "rgba(150,150,150, .65)"
    }
    else {
        obj.backgroundColor = draw('diagonal-right-left', 'rgba(43, 82, 131, .7)')//    type === "original" ? "rgba(235,235,235, .65)" : "rgba(150,150,150, .65)"
    }

    return obj
}