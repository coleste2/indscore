import React, { useState, useEffect } from "react"
import { Modal, Button } from "react-bootstrap"
import { Accordion } from 'semantic-ui-react'
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { useSelector, useDispatch } from "react-redux"
import { setFilter } from "../../redux/actions"
import { IndicatorFilter, ScoreFilter } from "./miniComponents/FilterPages"
import {ErrorBoundary} from "react-error-boundary"
import { FallBack } from "../../components/Error"

function FilterModal({ modal, setModal }) {

    //Get the filter and the piece of the store that handles the elimination of elements from the filter
    //Think of the knocker as a pipe that leads to making changes on the filter
    //The reason it has to go through the knocker though is because the modal needs to modify elements based on what has been
    //changed rather than simply not rendering anything
    const filter = useSelector(state => state.results_filter)
    const filterKnocker = useSelector(state => state.results_filterKnocker)

    const dispatch = useDispatch()

    //When the component closes, we want to clear the filter because the next time it opens the data selection might be different
    useEffect(() => {
        return () => dispatch(setFilter({}))
    }, [])

    //data displayed in each field
    const [selectedScores, setSelectedScores] = useState([])
    const [selectedOpinions, setSelectedOpinions] = useState([])
    const [selectedTypes, setSelectedTypes] = useState([])
    const [selectedDate, setSelectedDate] = useState([null, null]);

    //Bulk data collection displayed in prop fields
    const [selectedIndicatorFields, setSelectedIndicatorFields] = useState([{ filter: "Indicator ID/is", value: "" }])

    const panels = [
        {
            key: "scoreTab", title: 'Filter by Score Properties', content: {
                content: (<div>
                    {/**render the score filter tab */}
                    <ErrorBoundary FallbackComponent={FallBack}>
                        <ScoreFilter setSelectedOpinions={(data) => setSelectedOpinions(data)} selectedOpinions={selectedOpinions} setSelectedScores={(data) => setSelectedScores(data)} selectedScores={selectedScores} setSelectedDate={(data) => setSelectedDate(data)} selectedDate={selectedDate} />
                    </ErrorBoundary>
                </div>)
            }
        },
        {
            key: "ind", title: 'Filter by Indicator Properties', content: {
                content: (<div>
                    {/**render the indicator filter tab */}
                    <ErrorBoundary FallbackComponent={FallBack}>
                        <IndicatorFilter selectedFields={selectedIndicatorFields} setSelectedFields={(data) => setSelectedIndicatorFields(data)} selectedTypes={selectedTypes} setSelectedTypes={(data) => setSelectedTypes(data)} />
                    </ErrorBoundary>
                </div>)
            }
        },
    ]
    const applyFilter = () => {
        //Build filter off of selections
        var f = {}
        if (selectedScores.length !== 0) {
            f.scoreFilter = selectedScores
        }
        if (selectedOpinions.length !== 0) {
            f.opinionFilter = selectedOpinions
        }
        if (selectedDate && selectedDate[0] && selectedDate[1]) {
            f.dateFilter = ([...selectedDate]).slice()
        }

        var arr = []
        for (var i = 0; i < selectedIndicatorFields.length; i++) {
            var val = selectedIndicatorFields[i].value
            if ((val === null || val === "")) continue
            arr.push(selectedIndicatorFields[i])
        }
        if (arr.length > 0) f.indicatorTextFilter = JSON.parse(JSON.stringify(arr))
        else {
            arr.push({ filter: "Indicator ID/is", value: "" })
        }
        setSelectedIndicatorFields(arr)

        if (selectedTypes.length !== 0) {
            f.indicatorTypes = selectedTypes
        }

        dispatch(setFilter(f))
        setModal(false)
    }
    const reset = () => {
        //Reset the filter and close the modal
        dispatch(setFilter({}))
        setModal(false)
    }
    useEffect(() => {
        //IF the filter changes and it now has 0 keys, reset all of the fields to their base state
        if (Object.keys(filter).length === 0) {
            setSelectedDate([null, null])
            setSelectedScores([])
            setSelectedOpinions([])
            setSelectedTypes([])
            setSelectedIndicatorFields([{ filter: "Indicator ID/is", value: "" }])
        }
    }, [filter])

    useEffect(() => {
        //Handle knocking of the date filter
        if (filterKnocker.key === "dateFilter") {
            var f = JSON.parse(JSON.stringify(filter))
            delete f.dateFilter
            dispatch(setFilter(f))
            setSelectedDate([null, null])
        }
        else if (filter[filterKnocker.key] !== undefined) {
            f = JSON.parse(JSON.stringify(filter))

            //Knock entire prop of the filter
            if (filterKnocker.index === "all") {
                delete f[filterKnocker.key]
                if (filterKnocker.key === "scoreFilter") setSelectedScores([])
                else if (filterKnocker.key === "opinionFilter") setSelectedOpinions([])
                else if (filterKnocker.key === "indicatorTypes") setSelectedTypes([])
            }
            //We only want to knock a piece of the prop
            else {
                f[filterKnocker.key].splice(filterKnocker.index, 1)

                //if we are knocking the last piece of the prop in the filter
                if (f[filterKnocker.key].length === 0) {
                    delete f[filterKnocker.key]
                    if (filterKnocker.key === "scoreFilter") setSelectedScores([])
                    else if (filterKnocker.key === "opinionFilter") setSelectedOpinions([])
                    else if (filterKnocker.key === "indicatorTypes") setSelectedTypes([])
                    else if (filterKnocker.key === "indicatorTextFilter") setSelectedIndicatorFields([{ filter: "Indicator ID/is", value: "" }])
                }

                //if we are not knocking the last piece of the prop in the filter
                else {
                    if (filterKnocker.key === "scoreFilter") setSelectedScores(selectedScores.remove(filterKnocker.index))
                    else if (filterKnocker.key === "opinionFilter") setSelectedOpinions(selectedOpinions.remove(filterKnocker.index))
                    else if (filterKnocker.key === "indicatorTypes") setSelectedTypes(selectedTypes.remove(filterKnocker.index))
                    else if (filterKnocker.key === "indicatorTextFilter") setSelectedIndicatorFields(selectedIndicatorFields.remove(filterKnocker.index))
                }
            }
            dispatch(setFilter(f))
        }
        // eslint-disable-next-line 
    }, [filterKnocker])

    const close = () => {
        setModal(false)
    }

    //render the modal along with the tabs defined up top
    return (<div >
        <ErrorBoundary FallbackComponent={FallBack}>
            <Modal show={modal} onHide={close} size="xl" backdrop="static" className = "filterModal" centered>
                <Modal.Header closeButton>
                    <Modal.Title>Filter</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Accordion className="filterAccordian" defaultActiveIndex={[0, 1, 2]} panels={panels} fluid exclusive={false} />
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={close} variant="secondary">Close</Button>
                    <Button variant="danger" onClick={reset} >Clear Filter</Button>
                    <Button variant="success" onClick={applyFilter}>Save and Apply Filter</Button>
                </Modal.Footer>
            </Modal>
        </ErrorBoundary>

    </div>)
}
export default FilterModal





// eslint-disable-next-line 
Date.prototype.addMinutes = function (h) {
    return new Date(this.toString()).setMinutes(this.getMinutes() + h);
}
// eslint-disable-next-line 
Array.prototype.remove = function (index) {
    return [...this.slice(0, index), ...this.slice(index + 1)]
}