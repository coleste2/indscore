import React from "react"
import { Button, Dropdown, DropdownButton, ButtonGroup } from "react-bootstrap"
import Icon from "../../bootstrap/icons"
import { useSelector, useDispatch } from "react-redux"
import { setHighlightDetails, setDisplayDetails, setSortProp, setSortDirection } from "../../redux/actions"
import { Popup } from "semantic-ui-react"
import {ErrorBoundary} from "react-error-boundary"
import { FallBack } from "../../components/Error"

function Header() {
    return (
        <>
            <div className="filterBar mb-2">

                <span className="displayIcon mr-2">
                    {/*Sort control pannel */}
                    <ErrorBoundary FallbackComponent={FallBack}>
                        <Sort />
                    </ErrorBoundary>

                </span>

                <span className="displayIcon mr-2">
                    {/*Score highlight control pannel */}
                    <ErrorBoundary FallbackComponent={FallBack}>
                        <Highlight />
                    </ErrorBoundary>
                </span>

                <span className="displayIcon mr-2">
                    {/*Display format control pannel */}
                    <ErrorBoundary FallbackComponent={FallBack}>
                        <Display />
                    </ErrorBoundary>
                </span>
            </div>
        </>
    )
}
export default Header

const Sort = () => {
    const sort = useSelector(state => state.results_sort)
    const dispatch = useDispatch()

    return (<><ButtonGroup>
        <Popup className="dropdownTooltip" content={`Sort by property`} size='mini' inverted position='top center' trigger={
            <DropdownButton variant="outline-dark" as={ButtonGroup} title={<><Icon type="filter" />&nbsp;{(sort.prop === "blank" ? "" : " " + sort.prop + " ")}</>} id="bg-nested-dropdown">
                {Array.from(["Score", "divider", "Indicator Type", "Indicator ID", "divider", "divider", "Date Scored"], (sortField, index) => {
                    if (sortField === "divider") return <Dropdown.Divider key={index} />
                    else return (
                        <Dropdown.Item key={index} active={sort.prop === sortField} onClick={() => dispatch(setSortProp(sortField))}>{sortField}</Dropdown.Item>)
                })}
            </DropdownButton>}
        />

        {/*If there is a sorting property selected, then render the sorting direction button */}
        {sort.prop !== "blank" && <Popup className="dropdownTooltip" content='Sort Direction' size='mini' inverted position='top center' trigger={
            <Button onClick={() => dispatch(setSortDirection(sort.direction === "a" ? "d" : "a"))} className="sortButton" variant="outline-dark"><div className={"sortButtonInside " + sort.direction}><Icon type="upArrow" /></div></Button>} />}
    </ButtonGroup></>)
}

const Highlight = () => {
    const highlight = useSelector(state => state.results_highlightDetails)
    const dispatch = useDispatch()

    return (<>         <Popup className="dropdownTooltip" content={`Score Emphasis`} size='mini' inverted position='top center' trigger={

        <DropdownButton className="displayType" variant="outline-dark" title={<Icon type="flashlight" />} id="bg-nested-dropdown">
            <Popup className="dropdownTooltip" content='No Score Emphasis' size='mini' inverted position='left center' trigger={
                <Dropdown.Item active={highlight === "none"} onClick={() => dispatch(setHighlightDetails("none"))}><Icon type="none" /></Dropdown.Item>
            } />
            <Popup className="dropdownTooltip" content='Emphasize Score' size='mini' inverted position='left center' trigger={
                <Dropdown.Item active={highlight === "highlighter"} onClick={() => dispatch(setHighlightDetails("highlighter"))}><Icon type="highlighter" /></Dropdown.Item>
            } />
            <Popup className="dropdownTooltip" content='Heat Map Mode' size='mini' inverted position='left center' trigger={
                <Dropdown.Item active={highlight === "topography"} onClick={() => dispatch(setHighlightDetails("topography"))}><Icon type="topography" /></Dropdown.Item>
            } />
        </DropdownButton>} /></>)
}

const Display = () => {
    const displaySelected = useSelector(state => state.results_displayDetails)
    const dispatch = useDispatch()

    return (<>
        <Popup className="dropdownTooltip" content={`Display Layout`} size='mini' inverted position='top center' trigger={
            <DropdownButton className="displayType" variant="outline-dark" title={<><Icon type="display" />{" "}</>} id="bg-nested-dropdown">
                <Popup className="dropdownTooltip" content='List Display' size='mini' inverted position='left center' trigger={
                    <Dropdown.Item active={displaySelected === "list"} onClick={() => dispatch(setDisplayDetails("list"))}><Icon type="list" /></Dropdown.Item>
                } />
                <Popup className="dropdownTooltip" content='Table Display' size='mini' inverted position='left center' trigger={
                    <Dropdown.Item active={displaySelected === "table"} onClick={() => dispatch(setDisplayDetails("table"))}><Icon type="table" /></Dropdown.Item>
                } />
                <Popup className="dropdownTooltip" content='Grid Display' size='mini' inverted position='left center' trigger={
                    <Dropdown.Item active={displaySelected === "grid"} onClick={() => dispatch(setDisplayDetails("grid"))}><Icon type="collection" /></Dropdown.Item>
                } />
            </DropdownButton>} /></>)
}