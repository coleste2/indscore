import React, { useState, useEffect, useRef } from "react"
import Paginate from "./miniComponents/Paginate"
import TableHeader from "./TableHeader"
import TableContainer from "./miniComponents/Datatable"
import { useSelector } from "react-redux"
import Backdrop from '@material-ui/core/Backdrop';
import axios from "axios"
import apiBase from "../../components/api"
import { useSnackbar } from "notistack"
import Loading from "../../components/Loading"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../components/Error"
import Icon from "../../bootstrap/icons"
//We break apart the table header and rest of the table because the table header should not have to re-render on page change
function Table({ }) {
    const [pageSize, setPageSize] = useState(1)
    return (<>
        {pageSize > 0 &&
            <ErrorBoundary FallbackComponent={FallBack}>
                <TableHeader />
            </ErrorBoundary>
        }
        <ErrorBoundary FallbackComponent={FallBack}>
            <TableAndPages setPageSize={(data) => setPageSize(data)} />
        </ErrorBoundary>

    </>)
}
export default Table

//This component handles the table and the paginator 
const TableAndPages = ({ setPageSize }) => {
    const firstFetch = useRef(true)
    const { enqueueSnackbar } = useSnackbar();
    const [page, setPage] = useState({ size: 10, number: 1 })
    const [apiData, setApiData] = useState({ data: [], totalCount: 0 })
    const sort = useSelector(state => state.results_sort)
    const filter = useSelector(state => state.results_filter)
    const [backdrop, setBackdrop] = useState(true)

    useEffect(() => {
        setPage({ size: page.size, number: 1 })
    }, [filter])

    useEffect(() => {
        getData()
    }, [page, sort])

    const getData = async () => {
        setBackdrop(true)
        try {
            const { data } = await axios.get(apiBase + `scoredIndicators.php/?page=${page.number}&pageSize=${page.size}&sortBy=${sort.prop}&sortDir=${sort.direction}&filter=${JSON.stringify(filter)}`)
            setPageSize(data.totalCount)
            setApiData(data)
            firstFetch.current = false;
        } catch (error) {
            enqueueSnackbar("There was an error fetching the data", { variant: "error" });
            console.log(error)
        }
        setBackdrop(false)
    }
    return (<>
        {apiData.totalCount === 0 && firstFetch.current === false && <div style={{ display: "flex", alignItems: "center", justifyContent: "center", flexDirection: "column" }}>
            <Icon type="frown glass" />
            <h1>No Indicators Found</h1>
        </div>}

        {apiData.totalCount > 0 &&
            <>
                <ErrorBoundary FallbackComponent={FallBack}>
                    <TableContainer data={apiData.data} />
                </ErrorBoundary>

                <ErrorBoundary FallbackComponent={FallBack}>
                    <Paginate count={apiData.totalCount} page={page} setPage={(data) => setPage(data)} />
                </ErrorBoundary>
            </>}
        <Backdrop style={{ color: '#fff', zIndex: 999999999999 }} open={backdrop} >
            <Loading />
        </Backdrop>
    </>)
}
