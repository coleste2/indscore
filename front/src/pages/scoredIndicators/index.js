import React, { useEffect, useState } from "react"
import { Link } from 'react-scroll'
import { useScrollPosition } from '@n8tb1t/use-scroll-position'
import "./ScoreIndicators.css"
import Results from "./Results"
function ScoreIndicators() {

    return (
        <>
            <BookmarkHeader />

            <div className="ScoreIndicatorsContainer mb-4">
                {/* Render results...the point of this component is to handle anything that would cause a change
            in the original data like a change in the scoring rule*/}
                <Results />
            </div>
        </>
    )
}
export default ScoreIndicators


const BookmarkHeader = () => {

    const [visible, setVisible] = useState(true)

    useScrollPosition(
        ({ prevPos, currPos }) => {
            const isVisible = currPos.y > prevPos.y || currPos.y > -150
            if (isVisible !== visible){
                setVisible(isVisible)
            }
        },
        [visible]
    )
    
    return (
        <div className={'stickyDashHeader ' + (visible ? "headerStyleVisible" : "headerStyleHidden")}>
            <Link activeClass="activeLinkScroll" className="LinkScroll" to="Results" spy={true} smooth={true} offset={30} duration={500}  >
                Results
</Link>
            <Link activeClass="activeLinkScroll" className="LinkScroll" to="Historics" spy={true} smooth={true} offset={10} duration={500} >
                Historics
</Link>
            <Link activeClass="activeLinkScroll" className="LinkScroll" to="Statistics" spy={true} smooth={true} offset={10} duration={500} >
                Statistics
</Link>
            <Link activeClass="activeLinkScroll" className="LinkScroll" to="Filter" spy={true} smooth={true} offset={10} duration={500} >
                Filter
</Link>
            <Link activeClass="activeLinkScroll" className="LinkScroll" to="Details" spy={true} smooth={true} offset={10} duration={500} >
                Details
</Link>
        </div>
    )
}
