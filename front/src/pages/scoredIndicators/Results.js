import React from "react"
import 'semantic-ui-css/semantic.min.css'
import "./ScoreIndicators.css"
import Table from "./Table"
import Summary from "./Summary"
import CountDown from "./miniComponents/CountDown"
import { Accordion } from 'semantic-ui-react'
import Filter from "./Filter"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../components/Error"
import { Element } from 'react-scroll'

function Results() {
    return (
        <div style={{ position: "relative" }} >
            {/* Render the countdown to server update clock*/}
            <ErrorBoundary FallbackComponent={FallBack}>
                <CountDown />
            </ErrorBoundary>

            {/* Render the summary with the charts and that statistics*/}
            <ErrorBoundary FallbackComponent={FallBack}>
                <Summary />
            </ErrorBoundary>

            {/* Render the filter component and the table details display component*/}
            <Accordion className="filterAccordian" defaultActiveIndex={[0, 1]} panels={[{
                key: "filter", title: { content: (<Element name="Filter" className="homeTabTitle"><h2 className="homeTabTitle">Filter</h2></Element>) }, content: {
                    content: (
                        <ErrorBoundary FallbackComponent={FallBack}>
                            <Filter />
                        </ErrorBoundary>
                    )
                }
            }, {
                key: "ind", title: { content: (<Element name="Details" className="homeTabTitle"><h2 className="homeTabTitle">Details</h2></Element>) }, content: {
                    content: (
                        <ErrorBoundary FallbackComponent={FallBack}>
                            <Table />
                        </ErrorBoundary>
                    )
                }
            }]} fluid exclusive={false} />
        </div>

    )
}
export default Results

