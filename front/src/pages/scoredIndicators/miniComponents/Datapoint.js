import React, { useState } from "react"
import { Modal, Button, Spinner } from "react-bootstrap"
import ReactJson from 'react-json-view'
import axios from "axios"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../../components/Error"
import { useSelector } from "react-redux"
import { Link } from "react-router-dom"

//This component handles the data in the row of the table as well as the modal that will display the content
function Datapoint({  point }) {

    //State for if the content modal should be open or not
    const [modal, setModal] = useState({ value: false, data: {} })
    const highlighter = useSelector(state => state.results_highlightDetails)
    const displaySelected = useSelector(state => state.results_displayDetails)

    const getData = async () => {
        //Fetch the data from the appropriate link
        var link = ""
        if (process.env.NODE_ENV === "production") link = `${window.location.pathname}/api/getContent.php/?id=${point.id}`
        else link = `http://localhost/api/getContent.php/?id=${point.id}`

        //Notify spinner that the data is being fetched
        setModal({ value: false, data: "fetching" })
        document.body.style.cursor = "progress";

        //Try to get the data
        try {
            var { data } = await axios.get(link)
            setModal({ value: true, data: data })
        }
        catch (err) {
            setModal({ value: false, data: {} })
            console.log(err)
        }
        document.body.style.cursor = "auto";
    }
    var colors = { _10: "#fff6ba", _30: "#ffd76b", _50: "#ffa51f", _70: "#ff7700", _90: "#ff0000" }

    var color = colors["_" + point.score_value]
    return (
        //Render all of the fields that are needed to be in the table
        <>

            <div style={highlighter === "topography" && displaySelected !== "grid" ? { backgroundColor: color, border:"1px solid black" } : undefined} className={"datapointContainer datapointContainer" + displaySelected}>
                <ErrorBoundary FallbackComponent={FallBack}>

                    <div style = {{wordWrap: "break-word"}}><b>Indicator Id:</b><a target="_blank" rel="noopener noreferrer" href={"https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/indicators/" + point.indicator_id}>{" " + point.indicator_id}</a></div>
                    <div style = {{wordWrap: "break-word"}}><b>Indicator Value:</b> {point.indicator_value}</div>
                    <div style = {{wordWrap: "break-word"}}><b>Indicator Type:</b> {point.indicator_type}</div>

                    <div style = {{wordWrap: "break-word"}}><b>Score Value:</b> <span style={highlighter === "highlighter" ? { backgroundColor: color } : {}}>{point.score_value}</span></div>
                    <div style = {{wordWrap: "break-word"}}><b>Score Name:</b> {point.score_name}</div>
                    <div style = {{wordWrap: "break-word"}}><b>Score Explanation:</b> {point.score_explanation}</div>
                    <div style = {{wordWrap: "break-word"}}><b>Score Opinion Value:</b> {point.opinion_value}</div>
                    <div style = {{wordWrap: "break-word"}}><b>Rule:<Link target="_blank" to={"/rules/" + point.rule_id}>{" " + point.rule_name +" ("+point.rule_id+")"}</Link></b></div>
                    <div style = {{wordWrap: "break-word"}}><b>Date/Time:</b> {new Date(point.datetime_of_score).toString().substr(0, new Date(point.datetime_of_score).toString().lastIndexOf(":"))}</div>
                    <div>
                        {modal.data === "fetching" && <Spinner animation="border" size="sm" style={{ margin: "0", marginRight: ".4rem" }} />}
                        <b style={{ cursor: "pointer", color: "blue", textDecoration: "underline" }} onClick={() => getData()}>View content when scored</b></div>

                    {/**More empty row hackery for IE 11 Flex grid */}
                    <div className="IEHackFiller" />
                    <div className="IEHackFiller" />
                    <div className="IEHackFiller" />
                    <div className="IEHackFiller" />
                </ErrorBoundary>

            </div>
            <ErrorBoundary FallbackComponent={FallBack}>

                {/**Modal to display indicator content at time of scoring */}
                {modal.value && <Modal size="lg" show={modal.value} onHide={() => setModal({ value: false })} centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Data Available at Time of Scoring</Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{maxHeight: 'calc(100vh - 200px)', overflowY: 'auto'}}>
                        {/**If the data is defined then render it in the JSON viewer */}
                        {modal.data.error === undefined ? <ReactJson src={modal.data} /> :

                            /**The data is corrupt and we should thus display it as a serialized string because something is better than nothing */
                            <div style={{ width: "100%", overflowWrap: "break-word" }}><h2>This Data is Corrupt!</h2><h5>We will attempt to display the recovered serialized data </h5><p>{modal.data.error}</p></div>}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={() => setModal({ value: false })}>Close</Button>
                    </Modal.Footer>
                </Modal>}
            </ErrorBoundary>

        </>
    )
}
export default Datapoint