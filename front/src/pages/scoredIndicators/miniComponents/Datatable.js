import React, { useEffect, useState } from "react"
import { useSnackbar } from "notistack"
import Datapoint from "./Datapoint"
import { VariableSizeGrid as Grid } from 'react-window';
import { isIE } from 'react-device-detect'
import { CellMeasurer, CellMeasurerCache, List, AutoSizer, WindowScroller } from "react-virtualized";
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../../components/Error"
import VirtualTable from "./Table"
import { useSelector } from "react-redux"

//This is the high level component that manages the internal structure of the table
//The datatable is told whether it should virtualize and it always honors
//It either renders a virtualized table or a normal table according to this

// const Table = withResizeDetector(ActualTable);

const listCache = new CellMeasurerCache({
    fixedWidth: true,
    defaultHeight: 100

})
export default function Table({ data }) {
    const displaySelected = useSelector(state => state.results_displayDetails)

    const { enqueueSnackbar } = useSnackbar();
    useEffect(() => {
        if (isIE) enqueueSnackbar("Data cannot be efficiently displayed with IE", { variant: "warning" });
    }, [])

    return (<>
        {displaySelected === "list" &&

            <VirtualList data={data} />

        }

        {displaySelected === "table" &&

            <ErrorBoundary FallbackComponent={FallBack}>
                <VirtualTable data={data} tableData={tableData} />
            </ErrorBoundary>

        }

        {displaySelected === "grid" &&
            <>
                {
                    data.length > 20 ?

                        <VirtualGrid data={data} />

                        :
                        <StaticGrid data={data} />
                }
            </>
        }

    </>)
}


const StaticGrid = ({ data }) => {
    const highlighter = useSelector(state => state.results_highlightDetails)

    return (
        <ErrorBoundary FallbackComponent={FallBack}>
            <div className="tableContainerThumbnails">

                {data.map((item, renderCount) => {
                    var colors = { _10: "#fff6ba", _30: "#ffd76b", _50: "#ffa51f", _70: "#ff7700", _90: "#ff0000" }

                    var color = colors["_" + item.score_value]
                    //We splice the data because we only want the data that is supposed to be on that page
                    //We then map through all of it

                    //Create color that correlates to score
                    return (
                        <div key={renderCount} className="potentialOutlineContainer" style={highlighter === "topography" ? { backgroundColor: color } : renderCount % 2 === 0 ? { backgroundColor: "#FaFaFa" } : { backgroundColor: "#e0e0e0" }}>
                            <div style={{ height: "100%", border:"1px solid black" }}>
                                {/**Render that row */}
                                <Datapoint point={item} />
                            </div>
                        </div>
                    )
                })}
                {/**Empty row to hack IE limitations of flex grid */}
                <div className="IEHackFiller1" />
                <div className="IEHackFiller1" />
                <div className="IEHackFiller1" />
                <div className="IEHackFiller1" />
                <div className="IEHackFiller1" />
                <div className="IEHackFiller1" />
            </div>
        </ErrorBoundary >
    )
}

const VirtualGrid = ({ data }) => {
    return (<div style={{ height: "100%" }}>
        <ErrorBoundary FallbackComponent={FallBack}>
            <AutoSizer disableHeight>
                {({ width }) =>
                    (<Grid className="vTableScores"
                        columnWidth={() => 450}
                        rowHeight={() => 397}
                        columnCount={Math.max(Math.floor(width / 450), 1)}
                        height={window.innerHeight}
                        rowCount={Math.ceil(data.length / (Math.max(Math.floor(width / 450), 1)))}
                        width={width} itemData={{ data: data, totalCols: Math.max(Math.floor(width / 450), 1) }} >{Cell}</Grid>
                    )}
            </AutoSizer>
        </ErrorBoundary>
    </div>)
}

const VirtualList = ({ data }) => {

    const [widthTrack, setWidthTrack] = useState(0)

    useEffect(() => {
        listCache.clearAll()
    }, [widthTrack, data])

    return (
        <WindowScroller>
            {({ height, scrollTop }) => (
                <AutoSizer disableHeight>
                    {({ width }) => {
                        setWidthTrack(width);
                        return (
                            <List
                                rowCount={data.length}
                                autoHeight
                                scrollTop={scrollTop}
                                width={width}
                                height={height}
                                data={data}
                                deferredMeasurementCache={listCache}
                                rowHeight={listCache.rowHeight}
                                rowRenderer={rowRenderer}
                                overscanRowCount={isIE ? 10 : 5}
                            />)
                    }}</AutoSizer>
            )}
        </WindowScroller>
    )

}





const rowRenderer = (props) => {
    const { index, parent, key, style } = props;
    const data = parent.props.data

    return (

        <CellMeasurer
            key={key}
            cache={listCache}
            parent={parent}
            columnIndex={0}
            rowIndex={index}
        >
            <div style={style}>
                <Row index={index} data={data} />
            </div>
        </CellMeasurer>

    );
};

//Virtualized cell--for the table
const Cell = ({ data, columnIndex, rowIndex, style }) => {
    const highlighter = useSelector(state => state.results_highlightDetails)
    var index = data.totalCols * (rowIndex) + columnIndex

    if (index < data.data.length) {

        var item = data.data[index]
        var colors = { _10: "#fff6ba", _30: "#ffd76b", _50: "#ffa51f", _70: "#ff7700", _90: "#ff0000" }

        var color = colors["_" + item.score_value]
        return (
            <ErrorBoundary FallbackComponent={FallBack}>
                <div style={{ ...style, width: "100%" }}>
                    <div className="potentialOutlineContainer" style={highlighter === "topography" ? { backgroundColor: color } : index % 2 === 0 ? { backgroundColor: "#FaFaFa" } : { backgroundColor: "#e0e0e0" }}>
                        <div style={{ height: "100%", border: "2px solid black" }}>
                            {/**Render that cell */}
                            <Datapoint point={item} />
                        </div>
                    </div>
                </div>
            </ErrorBoundary>
        );
    }
    else return <div style={{ height: "0" }}></div>
}

//Virtualized row--for the list
const Row = ({ data, index }) => {
    if (index < data.length) {
        var item = data[index]

        return (
            <ErrorBoundary FallbackComponent={FallBack}>
                <div className="potentialOutlineContainer" style={index % 2 === 0 ? { backgroundColor: "#FaFaFa" } : { backgroundColor: "#e0e0e0" }}>
                    <Datapoint point={item} />
                </div>
            </ErrorBoundary>
        );
    }
    else return (null)
}




const tableData = {
    data: [
        { title: "Indicator ID", width: "175px", prop: "indicator_id", },
        { title: "Indicator Value", width: "175px", prop: "indicator_value", flexible: true },

        { title: "Indicator Type", width: "175px", prop: "indicator_type" },
        { title: "Score Value", width: "70px", prop: "score_value", },
        { title: "Score Name", width: "175px", prop: "score_name" },
        { title: "Score Explanation", width: "175px", prop: "score_explanation" },
        { title: "Score Opinion", width: "175px", prop: "opinion_value" },
        { title: "Date/Time", width: "175px", prop: "datetime_of_score", date: true },
    ]
}

