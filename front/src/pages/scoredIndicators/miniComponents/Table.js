import React, { useEffect } from "react"
import { ScrollSync, ScrollSyncPane } from 'react-scroll-sync';
import { withResizeDetector } from 'react-resize-detector';
import { isIE } from 'react-device-detect'
import { CellMeasurer, CellMeasurerCache, List } from "react-virtualized";
import { useSelector } from "react-redux";


function Table({ data, tableData }) {


    return (<div className="tableContainer" style={isIE ? { height: "100%" } : {}}>
        {data &&
            //Sync horizontal scrolling between the table and the header
            <ScrollSync>
                <>
                    <ScrollSyncPane group="vertical">
                        <Header data={tableData.data} />
                    </ScrollSyncPane>
                    <div className="ScrollSyncWrapper">
                        <div style={{ height: "100%" }}>
                            <TableInternals data={data} tableData={tableData} />
                        </div></div>
                </>
            </ScrollSync>
        }
    </div>)
}

export default Table

const cache = new CellMeasurerCache({
    fixedWidth: true,
    defaultHeight: 100
})
const TableInternals = withResizeDetector(TableInternalsInside);



function TableInternalsInside({ data, tableData, width }) {
    //If the width or height changes re render
    useEffect(() => {
        cache.clearAll()
    }, [width, data])

    //If there are less than 25 rows then render a normal table
    return (<>{data.length < 25 ?
        <div style={{ height: "100%" }}>
            <div style={{ height: "100%" }}>
                <ScrollSyncPane group="vertical">
                    <div className="baseTableBody">{data.map((item, index) => {
                        return <Row data={tableData.data} index={index} item={item} />
                    })}
                    </div>
                </ScrollSyncPane>
            </div>
        </div>

        :
        //Else render a virtualized table
        <div style={{ height: "100%" }}>
            <ScrollSyncPane group="vertical">
                <List
                    rowCount={data.length}
                    width={width}
                    height={window.innerHeight}
                    data={{ itemData: data, data: tableData.data }}
                    deferredMeasurementCache={cache}
                    rowHeight={cache.rowHeight}
                    rowRenderer={rowRenderer}
                    overscanRowCount={isIE ? 10 : 5}
                />
            </ScrollSyncPane>
        </div>

    }</>)
}
//Render method for the virtualized rows
const rowRenderer = ({ index, parent, key, style }) => {
    const { data, itemData } = parent.props.data
    return (
        <CellMeasurer
            key={key}
            cache={cache}
            parent={parent}
            columnIndex={0}
            rowIndex={index}
        >
            <div style={style}>
                <Row data={data} index={index} item={itemData[index]} />
            </div>
        </CellMeasurer>
    );
};

//Render method for header
const Header = ({ data }) => {
    return (<div className="headerRow" >
        {data.map(item => {
            return (<HeaderCol width={item.width} title={item.title} flexible={item.flexible} />)
        })}
    </div>)
}

//Render method for row
const Row = ({ item, data }) => {
    const highlighter = useSelector(state => state.results_highlightDetails)
    const style = {}
    
    var colors = { _10: "#fff6ba", _30: "#ffd76b", _50: "#ffa51f", _70: "#ff7700", _90: "#ff0000" }
    var color = colors["_" + item.score_value]

    if (isIE) {
        style.minWidth = data.accumulate((current) => parseInt(current.width.substr(0, current.width.length))) + "px";
    }
    if (highlighter === "topography") {

        style.backgroundColor = color;
    }

    return (
        <>
            {item &&
                <div className="tableRow" style={style} >
                    {data.map(prop => <RowCol date={prop.date} highlight={prop.prop === "score_value" && highlighter === "highlighter" ? color : undefined} className={prop.className} onClick={prop.onClick} width={prop.width} text={item[prop.prop]} flexible={prop.flexible} />)}
                </div>}
        </>
    );
}


const HeaderCol = ({ title, width, flexible, }) => {

    return (
        <div className="headerData" style={flexible ? { flexGrow: 1, width: width, minWidth: width } : { width: width, minWidth: width }}>
            {title}
        </div>

    )
}

//Render a row of the table and include a highlighter to highlight the search text
const RowCol = ({ className, width, text, flexible, onClick, date, highlight }) => {

    if (highlight) {
        return (<span className={"tableData" + (className ? " " + className : "")} onClick={onClick ? (e) => onClick(e, text) : undefined} style={flexible ? { flexGrow: 1, width: width } : { width: width }}><>
            <span style={{ backgroundColor: highlight }}>{text}</span>
        </></span>);
    }
    else return (<span className={"tableData" + (className ? " " + className : "")} onClick={onClick ? (e) => onClick(e, text) : undefined} style={flexible ? { flexGrow: 1, width: width } : { width: width }}><>
        {date ? new Date(text).toString().substr(0, new Date(text).toString().lastIndexOf(":")) : text}
    </></span>);
}





//New flat map method
Array.prototype.flatMap = function (func) {
    var temp = []
    for (var i = 0; i < this.length; i++) {
        var x = func(this[i])
        if (x) temp.push(x)
    }
    return temp
}

//Accumulator method
Array.prototype.accumulate = function (func) {
    var temp = 0
    for (var i = 0; i < this.length; i++) {
        temp += func(this[i])
    }
    return temp
}

