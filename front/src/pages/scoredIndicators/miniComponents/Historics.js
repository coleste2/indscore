import React from "react"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../../components/Error"
import CircularProgress from '@material-ui/core/CircularProgress';
import { Doughnut, Polar } from 'react-chartjs-2';
//The stats bar in the summary
export default function Stats({ original, nonOriginal, fetching, multipleBlocks }) {
    return (<>
        <div className="summaryStatsContainer">
            <ErrorBoundary FallbackComponent={FallBack}>
                {!multipleBlocks ?
                    <>
                        <StatBlock data={original} />
                    </>
                    :
                    <>
                        <h3 style={{ margin: "0", marginBottom: ".5rem" }}>Original</h3>
                        <StatBlock data={original} />
                        <h3 style={{ margin: "0", marginBottom: ".5rem", marginTop: "2rem" }}>Filtered</h3>
                        <StatBlock data={nonOriginal} fetching={fetching} />
                    </>
                }
            </ErrorBoundary>

        </div></>)
}

const StatBlock = ({ data, fetching }) => {

    return (<div className="summaryStats">
        <BlockStat value1={!data || fetching ? <CircularProgress color="primary" className="centerHistoricsLoading" /> : <Chart type="pie" data={data.historics.daily} />} label="Daily Average Scoring" />
        <BlockStat value1={!data || fetching ? <CircularProgress color="primary" className="centerHistoricsLoading" /> : <Chart type="polar" data={data.historics.daily} />} label="Daily Max Scores" />
        <BlockStat value1={!data || fetching ? <CircularProgress color="primary" className="centerHistoricsLoading" /> : <Chart type="pie" data={data.historics.weekly} />} label="Weekly Average Scoring" />
        <BlockStat value1={!data || fetching ? <CircularProgress color="primary" className="centerHistoricsLoading" /> : <Chart type="polar" data={data.historics.weekly} />} label="Weekly Max Scores" />
    </div>)
}
//The box for each individual stat
const BlockStat = ({ value1, label }) => {
    return (<div className="historicsGraphHolder">
        <ErrorBoundary FallbackComponent={FallBack}>
            <div className="">{value1}</div>
            <div className="historicsGraphLabel">{label}</div>
        </ErrorBoundary>
    </div>)
}

const Chart = ({ type, data }) => {
    var chartData = {}
    chartData.datasets = []

    var dataset = {}

    dataset.label = "Historics plot"
    dataset.backgroundColor = ["#fff6ba", "#ffd76b", "#ffa51f", "#ff7700", "#ff0000"]

    chartData.labels = [
        '10',
        '30',
        '50',
        '70',
        '90'
    ]

    var chartConfig = {}
    chartConfig.legend = { display: false }


    chartConfig.responsive = true
    chartConfig.tooltips = {
        callbacks: {
            title: function (tooltipItem, data) {
                var opinion = ["Stronly Disagree", "Disagree", "Netrual", "Agree", "Stronly Agree"]
                try {
                    var score = "Score: " + data.labels[tooltipItem[0].index];
                    score += "; Opinion: " + opinion[tooltipItem[0].index];
                    return score;
                }
                catch (err) {
                    return "Tooltip error caught"
                }
            }
        }
    }


    if (type === "polar") {
        dataset.data = data.max
        dataset.isWeek = data.isWeek
        chartConfig.tooltips.callbacks.label = function (tooltipItem, data) {
            try {
                var label = data.labels[tooltipItem.index] + "s given a max of "+ Math.round(tooltipItem.yLabel * 100) / 100 + " times"
                return label;
            } catch (error) {
                return "tooltip error"
            }

        }
    }
    else if (type === "pie") {
        dataset.data = data.avg
        chartConfig.tooltips.callbacks.label = function (tooltipItem, data) {
            try {
                var label = "Averge of " + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + " " + data.labels[tooltipItem.index] + "s given (" + Math.round(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] / data.datasets[tooltipItem.datasetIndex].data.reduce((total, item) => total + item) * 100) + "%)"
                return label;
            } catch (error) {
                return "tooltip error"
            }

        }
    }


    chartConfig.maintainAspectRatio = false
    chartConfig.animation = { duration: 500, easing: 'easeOutCubic' }
    chartData.datasets.push(dataset)

    if (type === "polar") {
        return (<ErrorBoundary FallbackComponent={FallBack}>
            <Polar data={chartData} options={chartConfig} />
        </ErrorBoundary>)
    }

    else if (type === "pie") {
        return (<ErrorBoundary FallbackComponent={FallBack}>
            <Doughnut data={chartData} options={chartConfig} /></ErrorBoundary>)
    }


}