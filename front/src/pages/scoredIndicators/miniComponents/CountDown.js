import React from 'react';
import { Popup } from "semantic-ui-react"
import CircleProgressBar from '../microTools/CircleSpinner';

function Timer() {
    return (
        <div className="countdownContainer">
             <Popup position='left center' inverted content="Amount of seconds until newest indicators from Illuminate are scored" trigger={
              <div className = "countdownObject">
              <CircleProgressBar
                 trailStrokeColor="lightgrey"
                 className="countdownTimer"
                 strokeColor="teal"
                 innerText=""
             /></div>} />
        </div>
    );
};
export default Timer
