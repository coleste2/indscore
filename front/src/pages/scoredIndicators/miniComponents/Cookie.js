import React from "react"
import Chip from "@material-ui/core/Chip"
import { useDispatch } from "react-redux"
import { knockFilter } from "../../../redux/actions"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../../components/Error"

//This is the component that renders the deletable filter pieces in the filter component
const Cookie = ({ itemKey, itemValue, index }) => {
    const dispatch = useDispatch();
    var key = ""
    var val = ""

    //Handle the intro of each cookie and what it should say based off of prop type
    //The first 3 types do not get values bcause their values are mini cookies
    if (itemKey === "scoreFilter") key = "Indicator received score of "
    else if (itemKey === "opinionFilter") key = "Indicator received opinion of "
    else if (itemKey === "indicatorTypes") key = "Indicator is of type "
    else if (itemKey === "dateFilter") {
        key = "Indicator was scored between "
        var d1 = new Date(itemValue[0]).toString()
        var d2 = new Date(itemValue[1]).toString()
        val = d1.substr(0, d1.lastIndexOf(":")) + " and " + d2.substr(0, d2.lastIndexOf(":"))
    }
    else if (itemKey === "indicatorTextFilter") {
        key = itemValue.filter.replace("/", " ") + " "
        key = key.replace("regex search", "matches regex pattern") + " "
        key += itemValue.caseSensitive ? "exactly " : ""
        val = itemValue.value
    }

    //These types have values of mini cookies so this renders those mini cookies
    if (itemKey === "opinionFilter" || itemKey === "indicatorTypes" || itemKey === "scoreFilter")
        return (
            <ErrorBoundary FallbackComponent={FallBack}>
                <div>

                    {/**Make the big cookie container */}
                    <Chip className="filterChip"
                        //Make the label all of the mini cookies
                        label={<div>{key} {
                            itemValue.map((miniItem, index) => {
                                return <MiniCookie key={index} itemKey={itemKey} itemValue={miniItem} index={index} />
                            })}
                        </div>}
                        //Send the whole prop to the knocker if you delete the big cookie
                        onDelete={() => dispatch(knockFilter({ key: itemKey, index: "all" }))}
                    /></div>
            </ErrorBoundary>
        )
    else return (<div>
        {/**Make the big cookie with the label created above because these props dont have mini cookies */}
        <ErrorBoundary FallbackComponent={FallBack}>
  <Chip className="filterChip"
            label={key + val}
            onDelete={() => dispatch(knockFilter({ key: itemKey, index: index }))}
        />
            </ErrorBoundary>

    </div>
    )
}
export default Cookie

//This component is the mini cookie that sits inside of the cookie container
const MiniCookie = ({ itemKey, itemValue, index }) => {
    const dispatch = useDispatch()
    return (<span>
        <ErrorBoundary FallbackComponent={FallBack}>

            <Chip style={index === 0 ? { marginLeft: ".2rem" } : { marginLeft: ".4rem" }} className="filterMiniChip"
                label={itemValue}
                //On delete, send the 
                onDelete={() => dispatch(knockFilter({ key: itemKey, index: index }))}
            />
        </ErrorBoundary>


    </span>)
}
