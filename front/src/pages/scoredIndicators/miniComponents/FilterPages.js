import React, { useEffect, useState } from "react"
import { Form, InputGroup } from "react-bootstrap"
import DateTimeRangePicker from '@wojtekmaj/react-datetimerange-picker';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import TextFilter from "../miniComponents/TextFilter"
import Icon from "../../../bootstrap/icons"
import { Popup } from "semantic-ui-react"
import apiBase from "../../../components/api"
import { useSnackbar } from "notistack"
import { Typeahead } from "react-bootstrap-typeahead"
import axios from "axios"
import { FallBack } from "../../../components/Error"
import { ErrorBoundary } from "react-error-boundary"

//This file renders the score filter and the indicator filter tabs in the filter modal
export function ScoreFilter({ selectedScores, setSelectedScores, setSelectedDate, selectedDate, selectedOpinions, setSelectedOpinions }) {
    //We want to order and sort the scores every time we change them
    return (<>
        <ErrorBoundary FallbackComponent={FallBack}>
            <Form.Group>
                <InputGroup>
                    <InputGroup.Prepend>
                        {/**Typeahead for the score options */}
                        <InputGroup.Text>Indicators with scores of:</InputGroup.Text>
                    </InputGroup.Prepend>
                    <ScoresTypeahead selected={selectedScores} setSelected={(data) => setSelectedScores(data)} />
                </InputGroup>
            </Form.Group>
        </ErrorBoundary>
        <ErrorBoundary FallbackComponent={FallBack}>
            <Form.Group>
                <InputGroup>
                    <InputGroup.Prepend>
                        {/**Typeahead for the score options */}
                        <InputGroup.Text>Indicators with opinion values of:</InputGroup.Text>
                    </InputGroup.Prepend>
                    <BasicTypeahead multiple text="Opinion Value" prop="opinion_value" selected={selectedOpinions} setSelected={(data) => setSelectedOpinions(data)} />

                </InputGroup>
            </Form.Group>
        </ErrorBoundary>
        <ErrorBoundary FallbackComponent={FallBack}>
            <div className="dateContainer">
                <Form.Group>
                    <InputGroup>
                        <InputGroup.Prepend>
                            {/**Date picker for when the indicators were scored */}
                            <InputGroup.Text>Indicators scored between:</InputGroup.Text>
                        </InputGroup.Prepend>
                    </InputGroup>
                </Form.Group>
                <DateTimeRangePicker
                    // format="M/dd/y&nbsp;HH:mm"
                    calendarIcon={<Popup position='top center' inverted content={"Open Calendar"} mouseEnterDelay={300} className="inputTooltip" trigger={<span><Icon type="calendar" /></span>} />}
                    clearIcon={<Popup position='top center' inverted content={"Clear Range"} mouseEnterDelay={300} className="inputTooltip" trigger={<span><Icon type="flat x" /></span>} />}
                    onChange={setSelectedDate}
                    value={selectedDate}
                    rangeDivider="&nbsp;to"
                />
            </div>
        </ErrorBoundary>
    </>
    )
}
export function IndicatorFilter({ selectedFields, setSelectedFields, selectedTypes, setSelectedTypes }) {
    return (<>
        <ErrorBoundary FallbackComponent={FallBack}>
            <Form.Group>
                <InputGroup>
                    <InputGroup.Prepend>
                        {/**Simple typeahaead for what the indicator's types are */}
                        <InputGroup.Text>Indicators with types of:</InputGroup.Text>
                    </InputGroup.Prepend>
                    <BasicTypeahead multiple text="Indicator Type" prop="indicator_type" selected={selectedTypes} setSelected={(data) => setSelectedTypes(data)} />
                </InputGroup>
            </Form.Group>
        </ErrorBoundary>
        <ErrorBoundary FallbackComponent={FallBack}>
            {/**For all of the elements in the fields of the text input, render them  */}
            {selectedFields.map((item, index) => {
                var field = item.filter.substr(0, item.filter.indexOf("/"))
                return <TextFilter closeable={index !== 0} key={index} selectedFields={selectedFields} setSelectedFields={(data) => setSelectedFields(data)} field={field} filter={item.filter.substr(item.filter.indexOf("/") + 1)} index={index} totalFields={["Indicator ID", "Indicator Value"]} />
            })}
        </ErrorBoundary>
    </>
    )
}

const ScoresTypeahead = ({ selected, setSelected }) => {
    const [loading, setLoading] = useState(false)
    const [options, setOptions] = useState([])
    const { enqueueSnackbar } = useSnackbar();

    const change = (arr) => {
        if (arr.length !== 0) {
            if (!arr.reduce((n, item) => n !== false && item && parseInt(item) >= parseInt(n))) {
                arr.sort((a, b) => parseInt(a) - parseInt(b))
            }
            setSelected(arr)
            return
        }
        setSelected([])
        // eslint-disable-next-line
    }
    useEffect(() => {
        getData("")
    }, [])

    const getData = async (text) => {
        document.body.style.cursor = "progress";
        setLoading(true)
        try {
            const { data } = await axios.get(apiBase + "scoredIndicatorsProps.php/?prop=score_value&contains=" + text)
            setOptions(data)
        } catch (error) {
            console.log(error)
            enqueueSnackbar("There was an error fetching the data", { variant: "error" });
        }
        setLoading(false)
        document.body.style.cursor = "auto";
    }

    return (
        <ErrorBoundary FallbackComponent={FallBack}>
            <Typeahead
            className="scoreTypeahead"
            id="scoreTypeahead"
            minLength={0}
            selected={selected}
            onChange={(props) => change(props)}
            isLoading={loading}
            onInputChange={(text) => getData(text)}
            options={options}
            placeholder="Search for a score..."
            clearButton
            multiple /></ErrorBoundary>
    )
}


const BasicTypeahead = ({ prop, multiple, selected, setSelected, text }) => {
    const [loading, setLoading] = useState(false)
    const [options, setOptions] = useState([])
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        getData("")
    }, [])

    const getData = async (text) => {
        document.body.style.cursor = "progress";
        setLoading(true)
        try {
            const { data } = await axios.get(apiBase + "scoredIndicatorsProps.php/?prop=" + prop + "&contains=" + text)
            setOptions(data)
        } catch (error) {
            console.log(error)
            enqueueSnackbar("There was an error fetching the data", { variant: "error" });
        }
        setLoading(false)
        document.body.style.cursor = "auto";
    }

    return (
        <ErrorBoundary FallbackComponent={FallBack}>
            <Typeahead
                className="scoreTypeahead"
                id={prop + "typeahead"}
                minLength={0}
                selected={selected}
                onChange={(props) => setSelected(props)}
                isLoading={loading}
                onInputChange={(text) => getData(text)}
                options={options}
                placeholder={"Search for an " + text + "..."}
                clearButton
                multiple={multiple} />
        </ErrorBoundary>
    )
}
