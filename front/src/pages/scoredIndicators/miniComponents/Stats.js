
import React from "react"
import { Statistic } from 'semantic-ui-react'
import { Boxplot } from 'react-boxplot'
import { Popup } from "semantic-ui-react"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../../components/Error"
import CircularProgress from '@material-ui/core/CircularProgress';

//The stats bar in the summary
export const Stats = ({ original, nonOriginal, fetching, multipleBlocks }) => {
    return (<>
        <div className="summaryStatsContainer">
            <ErrorBoundary FallbackComponent={FallBack}>
                {!multipleBlocks ?
                    <>
                        <StatBlock data={original} />
                    </>
                    :
                    <>
                        <h3 style={{ margin: "0", marginBottom: ".5rem" }}>Original</h3>
                        <StatBlock data={original} />
                        <h3 style={{ margin: "0", marginBottom: ".5rem", marginTop:"2rem" }}>Filtered</h3>
                        <StatBlock data={nonOriginal} fetching={fetching} />
                    </>
                }
            </ErrorBoundary>

        </div></>)
}


const StatBlock = ({data, fetching}) => {

    return (<div className="summaryStats">

        <BlockStat value1={!data || fetching ? <CircularProgress color="primary" /> : data.stats.mean} label="Average Score" />
        <BlockStat value1={!data || fetching ? <CircularProgress color="primary" /> : data.stats.mode.length === 0 ? "N/A" : data.stats.mode.reduce((total, curr, index) => {
            return total + (index !== 0 ? ", " : "") + curr
        })} label="Mode Score" />
        <BlockStat value1={!data || fetching ? <CircularProgress color="primary" /> : data.quartiles.median} label="Median Score" />
        <BlockStat value1={!data || fetching ? <CircularProgress color="primary" /> : data.stats.range} label="Range" />
        <BlockStat value1={!data || fetching ? <CircularProgress color="primary" /> : data.stats.stdev} label="Standard Deviation" />
        <BlockStat value1={!data || fetching ? <CircularProgress color="primary" /> : data.stats.count} label="Total Indicators" />

        {/**The SVG Box plot set*/}
        <Statistic size='small'>
            <Statistic.Value>
                {!data || fetching ?
                    <div className="progressBoxHolder"><CircularProgress color="primary" /></div>
                    :
                    <Popup className="medTooltip" content="Boxplot of Distribution" size='small' inverted position='top center' trigger={
                        <div className="boxplotHolder">

                            <Boxplot className="bPlotSingle" width={180} height={15} orientation="horizontal" min={-1} max={103} stats={{
                                whiskerLow: data.quartiles.minWhisker,
                                quartile1: data.quartiles.quartile25,
                                quartile2: data.quartiles.median,
                                quartile3: data.quartiles.quartile75,
                                whiskerHigh: data.quartiles.maxWhisker,
                                outliers: data.quartiles.outliers,
                            }} />
                        </div>}
                    />
                }
            </Statistic.Value>
            <Statistic.Label>
                <div >
                    <div className="bplotAxis">
                    {data && !fetching ? <> <span>0</span><span>50</span><span>100</span> </> : <div style = {{height:"15px"}}/>}
                    </div>
                        Measure of Position
                        </div>
            </Statistic.Label>
        </Statistic>
    </div>)
}
//The box for each individual stat
const BlockStat = ({ value1,  label }) => {
    return (<Statistic size='small'>
        <ErrorBoundary FallbackComponent={FallBack}>
            <Statistic.Value>{value1}</Statistic.Value>
            <Statistic.Label>{label}</Statistic.Label>
        </ErrorBoundary>
    </Statistic>)
}
