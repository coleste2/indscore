import React, { useState, useEffect } from "react"
import { InputGroup, Dropdown, DropdownButton, FormControl } from "react-bootstrap"
import { Popup } from "semantic-ui-react"
import RegexParser from "regex-parser"
import 'react-bootstrap-typeahead/css/Typeahead.css';
import Icon from "../../../bootstrap/icons"
import apiBase from "../../../components/api"
import { useSnackbar } from "notistack"
import { Typeahead } from "react-bootstrap-typeahead"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../../components/Error"
import axios from "axios"
//This file renders the typable filters with the dropdowns in the indicator tabs of the filter modal
function TextFilter({ closeable, field, filter, selectedFields, setSelectedFields, index, totalFields }) {

    //This function is used to handle a change in the dropdown...the field name or the searching type
    const setItem = (item, str) => {
        var items = [...selectedFields]
        items[index].filter = str === "field" ? item + "/" + filter : field + "/" + item
        setSelectedFields(items)
    }

    //This function is handling an added field
    const addField = () => {
        setSelectedFields([...selectedFields, { filter: field + "/" + filter, value: "" }])
    }
    //This function is for handling the removal of a field
    const removeField = () => {
        var arr = [...selectedFields]
        arr.splice(index, 1);
        setSelectedFields(arr)
    }

    //Thus function is for handling a change in the content of a field
    const setField = (value) => {
        var arr = [...selectedFields]
        arr[index].value = value
        setSelectedFields(arr)
    }

    //This function is for handling a change in the case sensitivity of a field, if applicable
    const setCase = () => {
        var arr = [...selectedFields]
        if (arr[index].caseSensitive) delete arr[index].caseSensitive
        else arr[index].caseSensitive = true
        setSelectedFields(arr)
    }

    //This function is for determining if the regex field should be red and convey invalid regex
    const validRegex = (regex) => {
        if (regex === "") return true
        try {
            RegexParser(regex)
            return true
        } catch (error) {
            return false
        }
    }
    return (
        <div style={{ position: "relative" }}>
            <ErrorBoundary FallbackComponent={FallBack}>

                <InputGroup className="mt-3">
                    {/**This is the first dropdown and gives the option to change the field */}
                    <DropdownButton as={InputGroup.Prepend} className="filterModalButton" variant="prepend-text" title={field + " "} id="input-group-dropdown">
                        {totalFields.map((item, key) => {
                            return <Dropdown.Item key={key} active={item === field} onClick={() => setItem(item, "field")}>{item}</Dropdown.Item>
                        })}
                    </DropdownButton>

                    {/**This is the second dropdown and gives the option to change the type of searching */}
                    <DropdownButton as={InputGroup.Prepend} className="filterModalButton" variant="prepend-text" title={filter + " "} id="input-group-dropdown-1">
                        {Array.from(["is", "starts with", "contains", "ends with", "regex search"], (searchType, index) => { return <Dropdown.Item key={index} active={searchType === filter} onClick={() => setItem(searchType, "filter")}>{searchType}</Dropdown.Item> })}
                    </DropdownButton>

                    {/**This is the field thatt the user will type into and depending on the type of input, the will eother get a blank search field or a typeahead */}
                    {filter !== "is" ? <><FormControl className={filter === "regex search" && !validRegex(selectedFields[index].value) ? "error" : ""} placeholder="Enter query here" value={selectedFields[index].value} onChange={(e) => setField(e.target.value)} /></> :
                        // <SimpleTypeahead caseSensitive={selectedFields[index].caseSensitive} field={field} selected={[selectedFields[index].value]}  onInputChange={(e) => setField(e, "input")} onChange={(e) => { if (e[0] !== undefined) { setField(e[0], "select") } }} />
                        <SimpleTypeahead caseSensitive={selectedFields[index].caseSensitive} field={field} id={field + "/" + filter + index} options={[]} selected={[selectedFields[index].value]} onInputChange={(e) => setField(e, "input")} onChange={(e) => { if (e[0] !== undefined) { setField(e[0], "select") } }} />
                    }

                    {/**These are the buttons that appear at the end of the field */}
                    { /**This is either the regex info or the case button depending on if the field type is regec or not because regex must not be case insensitive */}
                    {filter === "regex search" ? <Popup className="ModalTooltip" on={'click'} position='top center' inverted content={<div>Use regular expressions to enhance your filtering power! <a rel="noopener noreferrer" target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions/Cheatsheet">View regex syntax here</a> and <a rel="noopener noreferrer" target="_blank" href="https://regex101.com/">visualize your expression here</a></div>} trigger={<span ><InputGroup.Text className="noCurve"><Icon type="info" /></InputGroup.Text></span>} /> :
                        <Popup className="ModalTooltip inputTooltip" position='top center' inverted content={"Case Sensitive"} mouseEnterDelay={300} trigger={<span onClick={() => setCase()} className={selectedFields[index].caseSensitive ? "darken" : "filterModalButton"}><InputGroup.Text className="noCurve"><Icon type="case sensitive" /></InputGroup.Text></span>} />
                    }
                    { /**This is the add field button */}
                    <Popup position='top center' inverted content={"Add Field"} mouseEnterDelay={300} className="ModalTooltip inputTooltip" trigger={<span className="filterModalButton" onClick={(field) => addField(field)}><InputGroup.Text className={closeable ? "noCurve" : "noCurveLeft"}><Icon type="plus flat" /></InputGroup.Text></span>} />

                    { /**This is the remove button and is only available if it is not the first field in the list */}
                    {closeable && <Popup position='top center' inverted content={"Remove Field"} mouseEnterDelay={300} className="ModalTooltip inputTooltip" trigger={<span className="filterModalButton" onClick={() => removeField()}><InputGroup.Text className="noCurveLeft"><Icon type="flat x" /></InputGroup.Text></span>} />}



                </InputGroup>
                {filter === "regex search" && !validRegex(selectedFields[index].value) && <div className="invalidRegex">Invalid Regex</div>}
            </ErrorBoundary>

        </div>
    )
}
export default TextFilter

const SimpleTypeahead = ({ selected, onChange, caseSensitive, field, id, onInputChange }) => {
    const [loading, setLoading] = useState(false)
    const [options, setOptions] = useState([])
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        getData("", false)
    }, [field])

    const getData = async (text, update = true) => {
        if (update) onInputChange(text)
        var prop = ""
        if (field === "Indicator ID") prop = "indicator_id"
        else if (field === "Indicator Value") prop = "indicator_value"
        document.body.style.cursor = "progress";
        setLoading(true)
        try {
            const { data } = await axios.get(apiBase + "scoredIndicatorsProps.php/?prop=" + prop + "&contains=" + text + (caseSensitive ? "&matchCase" : ""))
            setOptions(data)
        } catch (error) {
            console.log(error)
            enqueueSnackbar("There was an error fetching the data", { variant: "error" });
        }
        setLoading(false)
        document.body.style.cursor = "auto";
    }
    return (<>
        <ErrorBoundary FallbackComponent={FallBack}>
            <Typeahead
                id={id}
                minLength={0}
                selected={selected}
                onChange={onChange}
                isLoading={loading}
                onInputChange={(text) => getData(text)}
                options={options}
                placeholder="Enter query here"
                caseSensitive={caseSensitive}
            />
        </ErrorBoundary>
    </>
    )
}
