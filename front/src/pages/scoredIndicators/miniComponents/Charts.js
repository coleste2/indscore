import React, { useEffect, useState, useRef } from "react"
import { Bar, Line, Radar } from 'react-chartjs-2';
import Icon from "../../../bootstrap/icons"
// eslint-disable-next-line 
import * as zoom from '../microTools/zoom'
import { Button, ButtonGroup } from "react-bootstrap"
import Slider from "@material-ui/core/Slider"
import { Popup } from "semantic-ui-react"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../../components/Error"
import { Loader } from 'react-loaders';
import 'loaders.css/loaders.min.css';

//THis component renders the table for the score frequency. Learn more about the chart in the react-chartjs-2 and chartjs documentation 


export const RadarChart = ({ data, fetching }) => {
    return (<div style={{ width: "50%", position: "relative" }}>
        <h3 style={{ textAlign: "center", marginBottom: "5px", pointerEvents: "none" }}>Average Score and Counts of Indicator Types</h3>

        <BlockUi blocking={!data || fetching} Loader={<Loader type="ball-triangle-path" color="black" className="graphLoaderAnimation" />}>
            <div className="graphHolder radarGraphHolder" >
                <ErrorBoundary FallbackComponent={FallBack}>
                    <Radar data={data}
                        options={{
                            responsive: true,
                            tooltips: {
                                callbacks: {
                                    title: () => "",
                                    label: function (tooltipItem, data) {
                                        try {
                                            var str = ""
                                            if (data.datasets[tooltipItem.datasetIndex].label === "Original") {
                                                str = "Original "
                                            }
                                            else if (data.datasets[tooltipItem.datasetIndex].label === "Filtered") {
                                                str = "Filtered "
                                            }
                                            var label = data.labels[tooltipItem.index]
                                            return str + label + "s: average score: " + tooltipItem.yLabel + ", Indicators: " + data.datasets[tooltipItem.datasetIndex].counts[tooltipItem.index] + " (" + Math.round(data.datasets[tooltipItem.datasetIndex].ratios[tooltipItem.index] * 100) + "%)"
                                        }
                                        catch (err) {
                                            return "Tooltip error caught"
                                        }
                                    }
                                }
                            },
                            legend: { position: "bottom" },
                            // legend: { display: false },
                            maintainAspectRatio: false,
                            animation: { duration: 500, easing: 'easeOutCubic' },
                            scale: {
                                pointLabels: {
                                    fontSize: 12
                                }
                            }
                        }}
                    />
                </ErrorBoundary>
            </div>
        </BlockUi>
    </div>)
}

export const ScoreFrequencyChart = ({ data, fetching }) => {
    return (<div style={{ width: "50%", position: "relative" }}>
        <h3 style={{ textAlign: "center", marginBottom: "0", pointerEvents: "none" }}>Score Distribution of Indicators</h3>

        <BlockUi blocking={!data.datasets[0].data || fetching} Loader={<Loader type="ball-triangle-path" color="black" className="graphLoaderAnimation" />}>
            <div className="graphHolder" >
                <ErrorBoundary FallbackComponent={FallBack}>
                    <Bar data={data}
                        options={{
                            responsive: true,
                            tooltips: {
                                callbacks: {
                                    title: function (tooltipItem, data) {
                                        try {
                                            var label = tooltipItem[0].label.split(",")
                                            var score = "Score of " + label[0];
                                            score += " with Opinion of " + label[1]
                                            return score;
                                        }
                                        catch (err) {
                                            return "Tooltip error caught"
                                        }
                                    },
                                    label: function (tooltipItem, data) {
                                        try {
                                            var str = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + " scores given ("
                                            str += data.datasets[tooltipItem.datasetIndex].per[tooltipItem.index] + "%)"
                                            return str;
                                        }
                                        catch (err) {
                                            return "Tooltip error caught"
                                        }
                                    }
                                }
                            },
                            legend: { display: false },
                            scales: {
                                yAxes: [{ stacked: false, scaleLabel: { display: true, labelString: 'Indicator Count' }, ticks: { beginAtZero: true, precision: 0 } }],
                                xAxes: [{ stacked: true, scaleLabel: { display: true, labelString: 'Score and Opinion' } }],
                            },
                            maintainAspectRatio: false,
                            animation: { duration: 500, easing: 'easeOutCubic' },
                        }}
                    />
                </ErrorBoundary>
            </div>
        </BlockUi>
    </div>)
}

//THis component renders the table for the score over time. Learn more about the chart in the react-chartjs-2 and chartjs documentation 
export const ScoreOverTimeChart = ({ data, fetching }) => {
    //We have a ref attatched to the table so that we can control things like the zoom
    var graph = useRef(null)

    //Track chart settings
    const [viewType, setViewType] = useState("read")
    const [cursor, setCursor] = useState("auto")
    const [zoom, setZoom] = useState(50)
    const prevZoom = useRef(null)
    const [pan, setPan] = useState(50)
    const prevPan = useRef(null)
    const [sliders, setSliders] = useState(false)

    //Set the cursor for chart based on setting
    useEffect(() => {
        if (viewType === "drag") setCursor("crosshair")
        else if (viewType === "zoom") setCursor("grab")
        else setCursor("auto")
    }, [viewType])

    //If there is a change in zoom, update the chart
    useEffect(() => {
        try {
            if (zoom.current !== null && sliders) {
                //Only update the chart if the sliders were moved by the zoom reset button
                if (zoom !== 50.1234) {
                    if (zoom > prevZoom.current) graph.chartInstance.setZoom(graph.chartInstance, 1.13, 0, undefined, "x", undefined)
                    else graph.chartInstance.setZoom(graph.chartInstance, .87, 0, undefined, "x", undefined)
                }
            }
        } catch (error) { }

        prevZoom.current = zoom
        // eslint-disable-next-line
    }, [zoom])

    //If there is a change in pan, update the chart
    useEffect(() => {
        if (pan.current !== null && sliders) {
            //Only update the chart if the sliders were moved by the zoom reset button
            try {
                if (pan !== 50.1234) {
                    if (pan > prevPan.current) graph.chartInstance.setPan(graph.chartInstance, -100, 0)
                    else graph.chartInstance.setPan(graph.chartInstance, 100, 0)
                }
            } catch (error) { }
        }
        prevPan.current = pan
        // eslint-disable-next-line
    }, [pan])

    //Make it imposible to focus on the sliders button if the sliders are off so that it is clears that the sliders are turned off
    useEffect(() => {
        try {
            if (sliders === false && document.getElementById("slideActivator")) document.getElementById("slideActivator").blur()
        } catch (error) { }
    }, [sliders])

    return (<div style={{ position: "relative" }}>
        <ErrorBoundary FallbackComponent={FallBack}>

            {sliders && <>
                <span className="vertSlider">
                    <Slider orientation="vertical" value={zoom} onChange={(e, v) => setZoom(v)} />
                </span>
                <span className="regSlider">
                    <Slider value={pan} onChange={(e, v) => setPan(v)} />
                </span>
            </>}
        </ErrorBoundary>

        <h3 style={{ pointerEvents: "none", width: "100%", textAlign: "center", position: "absolute" }} className="mt-3">Scores Given To Indicators Over Time</h3>
        <div className="graphButtonHolder" style={{ marginBottom: "5px" }} >
            <ErrorBoundary FallbackComponent={FallBack}>

                {/**simply a button to set the chart viewing setting */}
                <ButtonGroup className="mr-2" >
                    <Popup className="graphButtonTooltip" content='Read Only' size='mini' inverted position='top center' trigger={
                        <Button variant="outline-dark" active={viewType === "read"} onClick={() => setViewType("read")}><Icon type="book" /></Button>
                    } />

                    <Popup className="graphButtonTooltip" content='Zoom and Pan' size='mini' inverted position='top center' trigger={
                        <Button variant="outline-dark" className="binosButton" active={viewType === "zoom"} onClick={() => setViewType("zoom")}><Icon type="binos" /></Button>
                    } />

                    <Popup className="graphButtonTooltip" content='Crop to Zoom' size='mini' inverted position='top center' trigger={
                        <Button variant="outline-dark" active={viewType === "drag"} onClick={() => setViewType("drag")}><Icon type="crop" /></Button>
                    } />

                </ButtonGroup>

                {/**simply a button to turn the sliders on */}
                <ButtonGroup className="mr-2" >
                    <Popup className="graphButtonTooltip" content='Manual Controls' size='mini' inverted position='top center' trigger={
                        <Button id="slideActivator" variant="outline-dark" active={sliders === true} onClick={() => setSliders(!sliders)}><Icon type="sliders" /></Button>
                    } />
                </ButtonGroup >

                {/**Reset char zoom button; when we click, we set sliders to obscure values near middle so that we can block changes because of chart reset */}
                <ButtonGroup >
                    <Popup className="graphButtonTooltip" content='Reset Plot' size='mini' inverted position='top center' trigger={
                        <Button onClick={() => { setZoom(50.1234); setPan(50.1234); graph.chartInstance.resetZoom() }} variant="outline-dark"><Icon type="refresh" /></Button>
                    } />
                </ButtonGroup>
            </ErrorBoundary>

        </div>

        {/**Refer to chartjs, chartjs-zoom, or react-chartjs-2 documentation to learn more */}
        <BlockUi blocking={(!data || fetching)} Loader={<Loader type="ball-triangle-path" color="black" className="graphLoaderAnimation" />}>
            <div className={"graphHolder"} style={sliders === true ? { width: "99%", cursor: cursor } : { cursor: cursor }} >
                <ErrorBoundary FallbackComponent={FallBack}>
                    {/* {(!data || fetching) && <div className="loading">
                    <CircularProgress color="primary" />
                </div>} */}
                    <Line
                        data={data}
                        options={{
                            tooltips: {
                                callbacks: {
                                    label: function (tooltipItem, context) {
                                        try {
                                            var label = 'Score of ';

                                            label += context.datasets[tooltipItem.datasetIndex].label.slice(-2) + " given "
                                            label += tooltipItem.yLabel + " time" + (tooltipItem.yLabel !== 1 ? "s" : "")
                                            return label;
                                        } catch (error) {
                                            return "Tooltip error caught";
                                        }

                                    }
                                }
                            },
                            responsive: true,
                            legend: { display: false },
                            scales: {
                                yAxes: [{ stacked: false, scaleLabel: { display: true, labelString: 'Indicator Score Count' }, ticks: { beginAtZero: true, precision: 0 } }],
                                xAxes: [{ type: 'time', scaleLabel: { display: true, labelString: 'Time' }, }],
                            },
                            maintainAspectRatio: false,
                            animation: { duration: 500, easing: 'easeOutCubic', },
                            plugins: {
                                zoom: {
                                    pan: {
                                        enabled: viewType === "zoom",
                                        mode: 'x',
                                        rangeMin: { x: new Date("01/01/1970") },
                                        rangeMax: { x: new Date("01/01/2500") },
                                        speed: 20,
                                        threshold: 10,
                                        onPan: () => { if (viewType === "zoom") setCursor("grabbing") },
                                        onPanComplete: () => { if (viewType === "zoom") setCursor("grab") }
                                    },
                                    zoom: {
                                        enabled: viewType === "zoom" || viewType === "drag",
                                        drag: viewType === "drag" ? { borderColor: 'rgba(225,225,225,0.3)', borderWidth: 5, backgroundColor: 'rgba(225,225,225, .5)', animationDuration: 300 } : false,
                                        // Drag-to-zoom effect can be customized
                                        mode: 'x',
                                        rangeMin: { x: new Date("01/01/1970") },
                                        rangeMax: { x: new Date("01/01/2500") },
                                        speed: 0.08,
                                        threshold: 3,
                                        sensitivity: 3,
                                        onZoom: () => { if (viewType === "zoom") setCursor("grabbing") },
                                        onZoomComplete: () => { if (viewType === "zoom") setCursor("grab") }
                                    }
                                }
                            }
                        }}
                        ref={(reference) => (graph = reference)} />
                </ErrorBoundary>
            </div>
        </BlockUi>
    </div>

    )
}


const BlockUi = ({ blocking, Loader, children }) => {
    return (
        blocking ?
            <><div className="blockUI"><div className="blockUIScreen">{children}</div>{Loader}</div></>
            :
            <>{children}</>
    )
}