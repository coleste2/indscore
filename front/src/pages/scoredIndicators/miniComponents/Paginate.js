import React from "react"
import { DropdownButton, Dropdown, Button, ButtonGroup, ButtonToolbar } from "react-bootstrap"
import { FallBack } from "../../../components/Error"
import { ErrorBoundary } from "react-error-boundary"
function Paginate({ page, setPage, count }) {

    //Calculate how many pages there will need to be to display all of the data
    var pages = Math.ceil(count / page.size)
    const buttons = (pages) => {

        //IF there are 1-6 pages then simply show those 6 pages and call it a day
        if (pages < 7) {
            return (
                [...Array(pages).keys()].map((item, index) => <Button key={index} active={page.number === index + 1} onClick={() => setPage({ size: page.size, number: index + 1 })} variant="outline-primary">{index + 1}</Button>)
            )
        }

        //If there are more than 6 pages, then we dont want to show all of them
        else {
            //If we are on one of the first 3 /7+ pages then keep range starting at one
            if (page.number < 4)
                return (<>
                    {[...Array(7).keys()].map((item, index) => <Button key={index} active={page.number === index + 1} onClick={() => setPage({ size: page.size, number: index + 1 })} variant="outline-primary">{index + 1}</Button>)}
                </>)

            //If we are on one of the first last n-2 /n where n > 6 pages then keep range ending at n
            else if (page.number > pages - 3) return (<>
                {[...Array(7).keys()].map((item, index) => <Button active={page.number === pages - 6 + index} onClick={() => setPage({ size: page.size, number: pages - 6 + index })} variant="outline-primary">{pages - 6 + index}</Button>)}
            </>)

            //Else just show us in the middle of the range
            else return (
                <>
                    {[...Array(3).keys()].map((item, index) => <Button onClick={() => setPage({ size: page.size, number: page.number - 3 + index })} variant="outline-primary">{page.number - 3 + index}</Button>)}
                    <Button active variant="outline-primary">{page.number}</Button>
                    {[...Array(3).keys()].map((item, index) => <Button onClick={() => setPage({ size: page.size, number: page.number + index + 1 })} variant="outline-primary">{page.number + index + 1}</Button>)}
                </>
            )
        }
    }

    return (<>
        <div className="paginateTable">
            <ErrorBoundary FallbackComponent={FallBack}>
                {/**This section renders the bottom left options of being able to change how many options are on the table */}
                <div className="tableSummary"> <div>Showing scores {numberWithCommas((page.number - 1) * page.size + 1)} to {(page.number * page.size) > count ? numberWithCommas(count) : numberWithCommas(page.number * page.size)} of {numberWithCommas(count)}</div>
                    <div className="flex-center">Page size of&nbsp;
                <DropdownButton variant="none" className="paginateDrop" id="dropdown-item-button" title={numberWithCommas(page.size) + " "}>
                            {Array.from([1, 10, 50, 100, 500, 1000, 5000, 10000], (x, index) => {
                                return <Dropdown.Item key={index} as="button" active={page.size === x} onClick={() => setPage({ size: x, number: 1 })}>{numberWithCommas(x)}</Dropdown.Item>
                            })}
                        </DropdownButton></div>
                </div>
                <ButtonToolbar aria-label="Toolbar with button groups">
                    <ButtonGroup className="mr-2" aria-label="First group">
                        {page.number > 3 && pages > 6 && <Button onClick={() => setPage({ size: page.size, number: 1 })} variant="outline-primary">«</Button>}
                        {buttons(pages)}
                        {page.number + 3 < pages && pages > 6 && <Button onClick={() => setPage({ size: page.size, number: pages })} variant="outline-primary">»</Button>}
                    </ButtonGroup>
                </ButtonToolbar>
            </ErrorBoundary>
        </div></>
    )
}
export default Paginate

function numberWithCommas(x) {
    return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : "0";
}