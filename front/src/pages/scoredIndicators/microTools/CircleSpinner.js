import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

//Internals of the clock
const INITIAL_OFFSET = 25;
const circleConfig = {
  viewBox: '0 0 38 38',
  x: '19',
  y: '19',
  radio: '15.91549430918954'
};

const CircleProgressBarBase = ({
  className,
  strokeColor,
  strokeWidth,
  trailStrokeWidth,
  trailStrokeColor,
  trailSpaced
}) => {


  const [progressBar, setProgressBar] = useState(0);

  useEffect(() => {
      var nextFetch = new Date()
      nextFetch.setMinutes(Math.ceil((nextFetch.getMinutes() + nextFetch.getSeconds() / 60) / 10) * 10)
      nextFetch.setSeconds(0)
      setProgressBar((nextFetch - new Date()) / 1000)
  }, [])

  useEffect(() => {
      let interval = null;
      if (progressBar < 0) setProgressBar(60 * 10)
      interval = setInterval(() => {
        setProgressBar(progressBar => progressBar - 1);
      }, 1000);

      return () => clearInterval(interval);
  }, [progressBar]);

  return (
      <svg className={className}viewBox={circleConfig.viewBox}>
        <circle
          className="donut-ring"
          cx={circleConfig.x}
          cy={circleConfig.y}
          r={circleConfig.radio}
          fill="transparent"
          stroke={trailStrokeColor}
          strokeWidth={trailStrokeWidth}
          strokeDasharray={trailSpaced ? 1 : 0}
        />

        <circle
          className="donut-segment"
          cx={circleConfig.x}
          cy={circleConfig.y}
          r={circleConfig.radio}
          fill="transparent"
          stroke={strokeColor}
          strokeWidth={strokeWidth}
          strokeDasharray={`${progressBar/6} ${100 - (progressBar/6)}`}
          strokeDashoffset={INITIAL_OFFSET}
        />

        <g transform="translate(0 3)" className="chart-text">
        <text x="50%" y="50%" transform="translate(0 -7.5)" className="chart-top">
            {"Fetching in"}
          </text>
          <text x="50%" y="50%"transform="translate(0 1)" className="chart-number">
            {Math.round(progressBar)}
          </text>
          <text x="50%" y="50%"transform="translate(0 6)" className="chart-label">
            {"Seconds"}
          </text>
          
        </g>
      </svg>
  );
};


const CircleProgressBar = styled(CircleProgressBarBase)`
  max-width: ${props => props.maxSize};
  vertical-align: middle;
  
  .chart-text {
    fill: ${props => props.textColor};
    transform: translateY(0.35em);
  }
  
  .chart-number {
    font-size: 0.7em;
    line-height: 1;
    text-anchor: middle;
    transform: translateY(-0.1em);
  }
  
  .chart-label {
    font-size: 0.25em;
    text-transform: uppercase;
    text-anchor: middle;
    transform: translateY(1em);
  }
  .chart-top {
    font-size: 0.2em;
    text-transform: uppercase;
    text-anchor: middle;
    transform: translateY(-.72rem);
  }
  .figure-key [class*='shape-'] {
    margin-right: 8px;
  }
  .figure-key-list {
    list-style: none;
    display: flex;
    justify-content: space-between;
  }
  .figure-key-list li {
    margin: 5px auto;
  }
  .shape-circle {
    display: inline-block;
    vertical-align: middle;
    width: 21px;
    height: 21px;
    border-radius: 50%;
    background-color: ${props => props.strokeColor};
    text-transform: capitalize;
  }
`;


export default CircleProgressBar;