import React from "react"
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { Ingredient } from "./miniComponents/Ingredients"
import StatementList from "./miniComponents/Statement"
import Icon from "../../bootstrap/icons"
import PrepBox from "./miniComponents/PrepBox"
import RuleListItems, { FinalButtons } from "./miniComponents/RuleListItems"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../components/Error"
//This is the exportable wrapper that enables drag and drop--it is simply the component wrapper
function AddLogic() {
    return (<DndProvider backend={HTML5Backend}>
        <AddLogicInternal />
    </DndProvider>)
}
export default AddLogic

//Here is the internals of the adding and building logic component
function AddLogicInternal() {
    return (
        <ErrorBoundary FallbackComponent={FallBack}>

            <div className="logicPage">
                <div className="addLogicContainer">
                    <div className="buildComponent">
                        <PrepIngredients />
                        <CombineStatements />
                    </div>
                    <RuleList />
                </div>
                <div className="logicPageButtons">
                    <FinalButtons />
                </div>
            </div>
        </ErrorBoundary>
    )
}

//Render the rules that sit on the right with drag recievers
const RuleList = () => {

    return (<div className="ruleList">
        <div className="ruleListContainer">
            <RuleListItems />
        </div>
    </div>)
}

//This is the component for the first two boxes--Ingredients and Prep
const PrepIngredients = () => {
    return (
        <ErrorBoundary FallbackComponent={FallBack}>

            <div className="ingredientsContainer" style={{ backgroundColor: "whitesmoke" }}>
                <div className="topIngredients">
                    <h3>Ingredients</h3>
                    <div className="ingredientsDragContainer">
                        {/**Map all of the ingredients that are in the array below. These ingredients are movable cookies */}
                        {ingredientList.map((item, index) => <Ingredient key={index} ingredient={item} />)}
                    </div>
                </div>
                <div className="ingredientArrow" >
                    <Icon type="down arrow" />
                </div>
                <div className="topIngredients">
                    <h3>Prep</h3>
                    <PrepBox />
                </div>
            </div>
        </ErrorBoundary>)
}

//Wrapper for the combine statements component
const CombineStatements = () => {
    return (
        <div className="statementContainer" style={{ backgroundColor: "whitesmoke" }}>
            <div className="statementsBox" >
                <h3 >Statements</h3>
                <StatementList />
            </div>
        </div>
    )
}

//Ingredient list
const ingredientList = [
    { property: "id", displayName: "Indicator ID", dataType: "int" },
    { property: "type", displayName: "Indicator Type", dataType: "string" },
    { property: "value->name", displayName: "Indicator Value", dataType: "string" },
    { property: "status", displayName: "Indicator Status", dataType: "enum", values: ['u', 'aab', 'aw', 'aal', 'ar', 'rc'].map((v, i) => { return { value: v, text: v, key: i + 1 } }) },
    { property: "active", displayName: "Is Active", dataType: "bool" },
    { property: "benign->value", displayName: "Benign Status", dataType: "bool" },
    { property: "lastHit", displayName: "Days Since Last Hit", dataType: "int" },
    { property: "hitCount", displayName: "Hit Count", dataType: "int" },
    { property: "reportCount", displayName: "Report Count", dataType: "int" },
    { property: "verified", displayName: "Was Verified", dataType: "bool" },
    { property: "tasked", displayName: "Tasked Status", dataType: "bool" },
    { property: "allowlist", displayName: "Present in Allowlist", dataType: "bool" },
    { property: "ignorelist", displayName: "Present in Ignorelist", dataType: "bool" },
    { property: "sourceTitle", displayName: "Source Title", dataType: "string" },
    { property: "sourceCount", displayName: "Unique Source Count", dataType: "int" },
]

