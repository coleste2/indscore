import React, { useState, useRef } from "react"
import { Button, Modal, Spinner } from "react-bootstrap"
import { useSelector, useDispatch } from "react-redux"
import { setBuildPage, clearRules } from "../../redux/actions"

import axios from "axios"
import { useSnackbar } from "notistack"
import apiBase from '../../components/api'
import { useHistory } from "react-router-dom"

import RuleGraph from "../../components/RuleGraph"
import { FormControl, FormLabel } from "react-bootstrap"
//Component that renders the chart for users to review their rule before publication
const ReviewRule = () => {
    const rules = useSelector(state => state.buildPage_buildRule)
    const dispatch = useDispatch()
    const [sendModal, setSendModal] = useState(false)

    return (<>
        <div className="reviewHolder">
            <RuleGraph rules={rules} />
        </div>
        <div className="ReviewPageButtons">
            {/**Buttons to change pages */}
            <Button onClick={() => dispatch(setBuildPage(1))}>Return to logic</Button>
            <Button onClick={() => setSendModal(true)}>Create Rule</Button>
        </div>
        <SendModal rules={rules} show={sendModal} close={() => setSendModal(false)} />
    </>)

}
export default ReviewRule

const SendModal = ({ rules, show, close }) => {
    const [loading, setLoading] = useState(false)
    const user = useSelector(state => state.userInfo)
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    const dispatch = useDispatch()
    const history = useHistory();
    const name = useRef(null)
    const description = useRef(null)
    const [input, setInput] = useState({ name: "", description: "" })

    const sendToBackEnd = async () => {
        try {
            if (input.name === "") {
                enqueueSnackbar("Please enter a name for the rule", { variant: "error" });
                name.current.focus()
                return;
            }
            if (input.description === "") {
                enqueueSnackbar("Please enter a description for the rule", { variant: "error" });
                description.current.focus()
                return;
            }
            if (input.description.length > 250 || input.name.length > 50) return;
            document.body.style.cursor = "progress";
            setLoading(true)
            const body = { rule: rules, name: input.name, description: input.description }
            let { data } = await axios.post(apiBase + "createRule.php", body)
            dispatch(setBuildPage(0))
            dispatch(clearRules())
            closeSnackbar()
            enqueueSnackbar("Rule created with id " + data.rule_id, { variant: "success" });
            history.push('/rules')

        } catch (error) {
            enqueueSnackbar("There was an error posting your rule to the server", { variant: "error" });
            console.log(error)
        }
        document.body.style.cursor = "auto";
        setLoading(false)
    }
    return (<Modal show={show} onHide={() => close()} centered>
        <Modal.Header closeButton><Modal.Title>Create Rule</Modal.Title></Modal.Header>
        <Modal.Body><h4>Confirm rule creation</h4>
            <FormLabel>Author</FormLabel>
            <FormControl disabled value = {user.username} style = {{marginBottom:".75rem"}}/>
            <FormLabel>Rule Name</FormLabel>
            <FormControl ref={name} placeholder="Name" value={input.name} className={input.name.length > 50 ? "error" : ""} onChange={(e) => setInput({ name: e.target.value, description: input.description })} />
            {input.name.length > 50 && <div style={{ color: "red" }}>Rule name cannot be longer than 50 chars. You are at {input.name.length}</div>}
            <FormLabel className="mt-3">Rule Description</FormLabel>
            <FormControl ref={description} className={input.description.length > 250 ? " error" : ""} as="textarea" rows="4" placeholder='Description' value={input.description} onChange={(e) => setInput({ description: e.target.value, name: input.name })} />
            {input.description.length > 250 && <div style={{ color: "red" }}>Rule description cannot be longer than 250 chars. You are at {input.description.length}</div>}

        </Modal.Body>
        <Modal.Footer>
            {loading ? <Spinner /> : <Button variant="secondary" onClick={() => close()}>Cancel</Button>}
            <Button variant="success" disabled={input.description.length > 250 || input.name.length > 50} onClick={() => sendToBackEnd()}>Confirm Creation</Button>
        </Modal.Footer>
    </Modal>)
}


