import React from "react"
import "./BuildRule.css"
import Build from "./BuildRule"

//head of Build rule component as a whole
function BuildRule() {
    return (
        <div className="BuildRuleContainer mb-4">
           <Build/>
                    </div>
    )
}
export default BuildRule

