import React from "react"
import { Button, Form } from "react-bootstrap"
import { Dropdown } from 'semantic-ui-react'
import Icon from "../../bootstrap/icons"
import Fab from '@material-ui/core/Fab';
import { Popup } from "semantic-ui-react"
import { useSelector, useDispatch } from "react-redux"
import { sortableContainer, sortableElement, sortableHandle } from 'react-sortable-hoc';
import { moveRule, addRule, deleteRule, setRuleBase, setBuildPage } from "../../redux/actions"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../components/Error"
//This is the component for stage 1 and building the outline of the rule 
function SelectRules() {
    const dispatch = useDispatch()
    const rules = useSelector(state => state.buildPage_buildRule)
    const onSortEnd = ({ oldIndex, newIndex }) => dispatch(moveRule(oldIndex, newIndex))
    return (<>
        <ErrorBoundary FallbackComponent={FallBack}>
            <div className="totalScoresContainer">
                <div className="selectScores">
                    <div className="selectScoresBody">
                        <h3>Score Outcomes</h3>
                        <div className="activeScores">
                            <ErrorBoundary FallbackComponent={FallBack}>

                                <SortableContainer lockToContainerEdges lockAxis="y" onSortEnd={(a) => onSortEnd(a)} useDragHandle>
                                    {
                                        rules.slice(0, rules.length - 1).map((item, index) => <SortableItem key={`item-${index}`} itemIndex={index} item={item} index={index} rules={rules} />)
                                    }
                                </SortableContainer>
                            </ErrorBoundary>

                            <div className="scoreBreak" />
                            <Score item={rules[rules.length - 1]} />
                        </div>
                        <Popup className="dropdownTooltip" content={rules.length > 1 ? "Continue to add logic" : 'Please add at least one score besides the default to continue'} size='mini' inverted position='top center' trigger={
                            //continue to next page button
                            <Button className={"nextButton " + (rules.length > 1 ? "" : "buttonDisable") + " mb-4"} onClick={() => dispatch(setBuildPage(1))}>Continue to add logic</Button>} />
                    </div>
                </div>
                <Popup className="dropdownTooltip" content={rules.length > totalOptions.length - 1 ? 'All possible scores added' : 'Add score outcome'} size='mini' inverted position='top center' trigger={
                    //Floating action button for adding a rule outline
                    <Fab aria-label="add" className={"addButton" + (rules.length > totalOptions.length - 1 ? " buttonDisable" : "")} style={rules.length > 4 ? { backgroundColor: "#a5a5a5" } : { backgroundColor: "#2b5283", color: "white" }} onClick={() => dispatch(addRule(totalOptions.filter(e => !rules.some(item => item.score === e.score))[0]))}>
                        <Icon type="thick plus" />
                    </Fab>} />
            </div >
        </ErrorBoundary>
    </>)
}

export default SelectRules
//
//This simply renders the score component but sortable
const SortableItem = sortableElement(({ item, rules, itemIndex, index }) => {
    return <div style={{ position: "relative" }}><DragHandle /><Score isEditable remaining={totalOptions.filter(e => !rules.some(item => item.score === e.score))} index={itemIndex} item={item} /></div>
})

//This simply renders the sortable container
const SortableContainer = sortableContainer(({ children }) => <div>{children}</div>);
//const DragHandle = sortableHandle(() => <span>::</span>);

const DragHandle = sortableHandle(() => {
    return <span style={{ position: "absolute", zIndex: "2", color: "white", top: "35px", left: "5px", cursor: "pointer" }} ><Icon type="grip" /></span>
});

//The render of the actual score outline
export const Score = ({ index, item, remaining, isEditable }) => {
    const dispatch = useDispatch()
    const del = () => dispatch(deleteRule(index))
    const change = (value) => dispatch(setRuleBase(getRule(value), index))
    return (
        <ErrorBoundary FallbackComponent = {FallBack}>

    <div className="scoreChipContainer">
        <span className="scoreChip">

            <div style={{ display: "flex", flexDirection: "column", alignItems: "flex-start", justifyContent: "flex-start" }}>
                <Form.Label className="white-text">Score Value</Form.Label>
                {isEditable ? <Dropdown disabled={item.isDefault === 1} id={`name-${index}`} value={item.scoreName} selection options={[{ key: "key", value: item.scoreName, text: item.scoreName }, ...remaining.map(val => { return { key: val.scoreName, value: val.scoreName, text: val.scoreName } })]} onChange={(e, v) => change(v.value, index)} /> :
                    <h3 style={{ margin: 0, marginBottom: "1rem", color: "white" }}>{item.scoreName}</h3>
                }
            </div>

            <div style={{ display: "flex", flexDirection: "column", alignItems: "flex-start", justifyContent: "flex-start" }}>
                <Form.Label className="white-text">Score</Form.Label>
                <h3 style={{ margin: 0, marginBottom: "1rem", color: "white" }}>{item.score}</h3>
            </div>

            <div style={{ display: "flex", flexDirection: "column", alignItems: "flex-start", justifyContent: "flex-start" }}>
                <Form.Label className="white-text">Opinion</Form.Label>

                <h3 style={{ margin: 0, marginBottom: "1rem", color: "white" }}>{item.opinion}</h3>
            </div>

        </span>
        {isEditable && !item.isDefault && <Popup className="whiteDropdownTooltip" content='Delete Score Outcome' size='mini' position='top center' trigger={
            <span style={{ position: "absolute", right: "10px", cursor: "pointer" }} onClick={() => del()} className="removeIconScore"><Icon type="circle x" /></span>} />}
    </div>
    </ErrorBoundary>
    )
}
const getRule = (value) => {
    for (var i = 0; i < totalOptions.length; i++) {
        if (totalOptions[i].scoreName === value) return totalOptions[i]
    }
    return totalOptions[0]
}
//Options for types of scores that can be added
const totalOptions = [
    {
        category: 'Logical',
        score: 10,
        scoreName: "Improbable",
        opinion: "Strongly Disagree"
    },
    {
        category: 'Logical',
        score: 30,
        scoreName: "Doubtful",
        opinion: "Disagree"
    },
    {
        category: 'Logical',
        score: 50,
        scoreName: "Possibly True",
        opinion: "Neutral"
    },
    {
        category: 'Consistent',
        score: 70,
        scoreName: "Probably True",
        opinion: "Agree"
    },
    {
        category: 'Confirmed',
        score: 90,
        scoreName: "Confirmed",
        opinion: "Strongly Agree"
    },
]

