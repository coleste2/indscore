import React from "react"
import Stepper from "./miniComponents/Stepper"
import SelectScores from "./SelectScores"
import AddLogic from "./AddLogic"
import ReviewRule from "./Review"
import {useSelector} from "react-redux"

//Build rule page selector 
function BuildRule() {
const pageNum = useSelector(state=>state.buildPage_page)
    return (
        <div >
            <Stepper />
            {pageNum === 0 &&<div>
                <SelectScores /> 
            </div>}
            {pageNum === 1 &&<div>
                <AddLogic />
            </div>}
            {pageNum === 2 &&<div>
               <ReviewRule />
            </div>}
        </div>
    )
}
export default BuildRule

