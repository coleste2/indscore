import React, { useEffect } from "react"
import { useDrag, useDrop } from 'react-dnd'
import DropTypes from "../DropTypes"
import Chip from "@material-ui/core/Chip"

//This component renders a dragable cookie
export const Ingredient = ({ ingredient }) => {

    //drag configuration
    const [{ isDragging }, drag] = useDrag({
        item: { ...ingredient, type: DropTypes.ingredient },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
    })
    const opacity = isDragging ? 0.4 : 1
    return (
        //The cookie that is dragable
        <span ref={drag} className="ingredientCookie" style={{ opacity: opacity, cursor: "pointer" }}>{ingredient.displayName}</span>
    )
}

// The cookie reception
export const IngredientReception = ({ setIngredient, ingredient }) => {
    //Drop setup
    const [{ canDrop, isOver, dropResult }, drop] = useDrop({
        accept: DropTypes.ingredient,
        drop: (data) => ({ ...data }),
        canDrop: () => ingredient === null,
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
            dropResult: monitor.getDropResult()
        }),
    })

    //If we get a drop and it is not null then set it as the attatched ingredient
    useEffect(() => {
        if (dropResult !== null && dropResult.type === "ingredient") setIngredient(dropResult)
        // eslint-disable-next-line
    }, [dropResult])

    //different styles of box based on the state
    let style = { backgroundColor: 'silver' }
    if (ingredient !== null) {
        style = { backgroundColor: 'transparent' }
    }
    else if (canDrop && isOver) {
        style = { backgroundColor: 'green', color:'white' }
    } else if (canDrop) {
        style = { backgroundColor: 'yellow' }
    }
    return (
        //ref to control the dropzone with
        <div ref={drop} className="dropBoxPrep">

            {ingredient === null ?
                //Show drop zone if there is no active ingredient
                <div style={{ padding: "2rem" }}><div style={{ padding: "1rem", ...style, width: "100%", height: "100%", textAlign:"center" }}> {canDrop && isOver ? 'Release to drop ingredient' : 'Drag an ingredient here'}</div></div>
                :
                //Else show the active ingredient
                <Chip className="prepChip" label={ingredient ? ingredient.displayName : ""} onDelete={() => setIngredient(null)} />
            }

        </div>
    )
}

