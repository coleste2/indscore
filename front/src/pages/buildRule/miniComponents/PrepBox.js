import React, { useState } from "react"
import { IngredientReception } from "./Ingredients"
import { Dropdown, Input, Popup } from 'semantic-ui-react'
import { useSnackbar } from "notistack"
import { useDispatch } from "react-redux"
import { addStatement } from "../../../redux/actions"
import { Button } from "react-bootstrap"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../../components/Error"

//The renderer of the mini logical rule creator
const PrepBox = () => {
    //the ingredient that is attatched

    const dispatch = useDispatch()
    const [logicDetails, setLogicDetails] = useState({ ingredient: null, negate: null, operation: null, value: null })
    const { enqueueSnackbar } = useSnackbar();

    const clear = () => {
        setLogicDetails({ ingredient: null, negate: null, operation: null, value: null })
    }
    const textOperation = (value) => {
        for (var i = 0; i < operations.int.length; i++) {
            if (operations.int[i].value === value) {
                return operations.int[i].text
            }
        }
        for (i = 0; i < operations.string.length; i++) {
            if (operations.string[i].value === value) {
                return operations.string[i].text
            }
        }
        return "==="
    }
    //export to the builder component
    const exportIngredient = () => {
        if (!logicDetails.ingredient) enqueueSnackbar("Please drag in an ingredient and prepare before exporting", { variant: "error" });
        else if (!logicDetails.operation) enqueueSnackbar("Please assign an operation before exporting", { variant: "error" });
        else if (!logicDetails.value) enqueueSnackbar("Please assign a value before exporting", { variant: "error" });
        else {
            var label = logicDetails.ingredient.displayName + (logicDetails.negate ? " is NOT " : " is ") + textOperation(logicDetails.operation) + " " + logicDetails.value
            dispatch(addStatement({ label: label, displayProp: logicDetails.ingredient.displayName, prop: logicDetails.ingredient.property, negate: logicDetails.negate, operation: logicDetails.operation, value: logicDetails.value }))
            clear()
        }
    }

    return (
        <ErrorBoundary FallbackComponent={FallBack}>

            <div style={{ position: "relative" }}>
                {/**Render the component that converts the the ingredient into a mini logical statement */}
                <div className="prepBox">
                    <IngredientReception setIngredient={(data) => setLogicDetails({ ingredient: data, negate: false, operation: data ? operations[data.dataType][0].value : null, value: data ? (data.dataType === "enum" ? data.values[0].value : values[data.dataType][0].value) : null })} ingredient={logicDetails.ingredient} />
                    {logicDetails.ingredient && <>
                        <div >
                            {logicDetails.ingredient.dataType === "bool" ? <span className="mr-2">is</span> : <Dropdown inline className="dropdownPrep" options={negation} value={logicDetails.negate !== null ? logicDetails.negate : negation[0].value} style={{ marginRight: "1rem" }} onChange={(e, v) => setLogicDetails({ ...logicDetails, negate: v.value })} />}
                            {logicDetails.ingredient.dataType === "int" || logicDetails.ingredient.dataType === "string" ? <Dropdown inline className="dropdownPrep" options={operations[logicDetails.ingredient.dataType]} value={logicDetails.operation ? logicDetails.operation : operations[logicDetails.ingredient.dataType][0].value} onChange={(e, v) => setLogicDetails({ ...logicDetails, operation: v.value })} /> :
                                <span>equal to</span>}
                        </div>
                        <div style={{ marginLeft: "1rem" }}>
                            {logicDetails.ingredient.dataType === "bool" &&
                                <Dropdown inline className="dropdownPrep" options={values.bool} value={logicDetails.value} onChange={(e, v) => setLogicDetails({ ...logicDetails, value: v.value })} />
                            }
                            {logicDetails.ingredient.dataType === "enum" &&
                                <Dropdown inline className="dropdownPrep" options={logicDetails.ingredient.values} value={logicDetails.value} onChange={(e, v) => setLogicDetails({ ...logicDetails, value: v.value })} />
                            }
                            {logicDetails.ingredient.dataType === "string" &&
                                <Input placeholder='String' value={logicDetails.value} onChange={(e, v) => setLogicDetails({ ...logicDetails, value: v.value })} />
                            }
                            {logicDetails.ingredient.dataType === "int" &&
                                <Input placeholder='Number' type="number" style={{ width: "8rem" }} value={logicDetails.value} onChange={(e, v) => setLogicDetails({ ...logicDetails, value: v.value })} />
                            }
                        </div></>}
                </div>
                <div>
                    {/**Button to clear this component */}
                    <Popup className="dropdownTooltip" content={logicDetails.ingredient === null ? "Box is already empty" : "Clear box"} size='mini' inverted position='top center' trigger={

                        <Button onClick={() => clear()} className={logicDetails.ingredient === null ? "buttonDisable" : ""}>Clear</Button>} />

                    {/**Button to export the logical component to the builder component */}
                    <Popup className="dropdownTooltip" content={logicDetails.ingredient === null ? "Cannot export empty statement" : "Export statement"} size='mini' inverted position='top center' trigger={
                        <Button onClick={() => exportIngredient()} className={logicDetails.ingredient === null ? "buttonDisable" : ""} style={{ position: "absolute", right: "0", bottom: "0" }}>Export</Button>} />
                </div>
            </div>
        </ErrorBoundary>

    )
}

export default PrepBox

const negation = [
    {
        key: '1',
        text: 'is',
        value: false,
    },
    {
        key: '2',
        text: 'is not',
        value: true,
    }]
const operations = {
    int: [{
        key: '1',
        text: 'greater than',
        value: '>',
    },
    {
        key: '2',
        text: 'greater than or equal to',
        value: '>=',
    },
    {
        key: '3',
        text: 'less than',
        value: '<',
    },
    {
        key: '4',
        text: 'less than or equal to',
        value: '<=',
    },
    {
        key: '5',
        text: 'equal to',
        value: '===',
    }],
    string: [{
        key: '1',
        text: 'equal to string',
        value: '===',
    },
    {
        key: '2',
        text: 'matching pattern',
        value: 'regex',
    }],
    bool: [{ value: "===" }],
    enum: [{ value: "===" }]
}
const values = {
    bool: [
        {
            key: '1',
            text: 'true',
            value: 'true',
        },
        {
            key: '2',
            text: 'false',
            value: 'false',
        }],
    int: [{ value: "" }],
    string: [{ value: "" }]
}
