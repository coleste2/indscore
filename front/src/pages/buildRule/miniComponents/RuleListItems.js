import React, { useEffect } from "react"
import { useDrop } from 'react-dnd'
import DropTypes from "../DropTypes"
import Chip from "@material-ui/core/Chip"
import { useDispatch, useSelector } from "react-redux"
import { setRuleRule, deleteRuleRule, setBuildPage } from "../../../redux/actions"
import { Button } from "react-bootstrap"
import { Popup } from "semantic-ui-react"
import { ErrorBoundary } from "react-error-boundary"
import { FallBack } from "../../../components/Error"
//Component that acts as a landing pad for all statements
const RuleListItems = () => {
    const rules = useSelector(state => state.buildPage_buildRule)

    //render all rules
    return (<>
        <ErrorBoundary FallbackComponent={FallBack}>

            {rules.map((item, index) => {
                //if the rule is the default rule, then simply say that and dont allow users to drop in a rule
                if (item.isDefault) return (<div key={index} className="ruleListItem default">
                    <div className="ruleListItemTitle mb-2">
                        <div>Category: {item.category}</div>
                        <div className="ml-2 mr-2">Score: {item.score}</div>
                        <div>Opinion: {item.opinion}</div>
                    </div>
                    <Chip className="ruleChip" label="Default" />
                </div>)
                //Otherwise, create a landing page in each other rule for users to drag rule into
                else return (<div key={index} className="ruleListItem mb-4">
                    <div className="ruleListItemTitle">
                        <div>Category: {item.category}</div>
                        <div className="ml-2 mr-2">Score: {item.score}</div>
                        <div>Opinion: {item.opinion}</div>
                    </div>
                    {/**The actual reciever */}
                    <RuleListItemDrop item={item} index={index} />
                </div>)

            })}
        </ErrorBoundary>
    </>
    )
}
export default RuleListItems

const RuleListItemDrop = ({ item, index }) => {
    const dispatch = useDispatch()
    const [{ canDrop, isOver, dropResult }, drop] = useDrop({
        accept: DropTypes.statement,
        options: { index: index },
        drop: (data) => ({ source: index, ...data }),
        canDrop: () => !item.rule,
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
            dropResult: monitor.getDropResult()
        }),
    })
    useEffect(() => {
        //If we get a new drop result, add it to the statement but remove the info we dont need any more
        if (dropResult !== null && dropResult.type === "statement" && dropResult.source === index) {
            var x = dropResult
            delete x.dropEffect
            delete x.source
            delete x.type
            dispatch(setRuleRule(x, index))
        }

    }, [dropResult])
    //different styles of box based on the state
    let style = { backgroundColor: 'silver' }
    if (item.rule) {
        style = { backgroundColor: 'transparent' }
    }
    else if (canDrop && isOver) {
        style = { backgroundColor: 'green', color: 'white' }
    } else if (canDrop) {
        style = { backgroundColor: 'yellow' }
    }
    return (
        //ref to control the dropzone with
        <div ref={drop} className={"dropBoxRule" + (item.rule ? " contains" : "")}>

            {!item.rule ?
                //Show drop zone if there is no active ingredient
                <div className="dropRuleContainer"><div className="dropRule" style={style}> <span>{canDrop && isOver ? 'Release to drop statement' : 'Drag a statement here'}</span></div></div>
                :
                //Else show the active ingredient
                <Chip component="div" className="ruleChip" label={<section><div className="ruleWrap">{item.rule.label}</div></section>} onDelete={() => dispatch(deleteRuleRule(index))} />
            }

        </div>
    )
}

//Buttons to handle navigation
export const FinalButtons = () => {
    const dispatch = useDispatch()
    const rules = useSelector(state => state.buildPage_buildRule)
    var disabled = !rules.every(item => item.rule)
    return (<>
        <Button onClick={() => dispatch(setBuildPage(0))}>Return to select and score</Button>
        <Popup className="dropdownTooltip" content={disabled ? "Please ensure every rule has a statement to continue" : 'Continue to final stage of rule development'} size='mini' inverted position='top center' trigger={
            <Button className={disabled ? "buttonDisable" : ""} onClick={() => dispatch(setBuildPage(2))}>Continue to review</Button>} />
    </>)
}