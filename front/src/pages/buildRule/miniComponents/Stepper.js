import React from "react"
import { Step } from "semantic-ui-react"
import Icon from "../../../bootstrap/icons"
import { useSelector, useDispatch } from "react-redux"
import { setBuildPage } from "../../../redux/actions"

//This component represents the stepper that shows the current stage of the rule being built
const Stepper = () => {
    const dispatch = useDispatch()
    const page = useSelector(state => state.buildPage_page)
    return (
        //The first stage
        <Step.Group fluid className="stepper">
            <Step active={page === 0}>
                <div className="flex-center">
                    <span><Icon type='truck' /></span>
                    <span className="ml-3"> <Step.Title onClick={() => dispatch(setBuildPage(0))}>Select and Sort</Step.Title>
                        <Step.Description>Select applicable scores and corresponding opinions and set order of outcomes</Step.Description></span>
                </div>
            </Step>
           
            {/**The second stage */}
            <Step active={page === 1} disabled={page < 1}>
                <div className="flex-center">
                    <span><Icon type='code' /></span>
                    <span className="ml-3"> <Step.Title onClick={() => dispatch(setBuildPage(1))}>Add Logic</Step.Title>
                        <Step.Description>Combine logical statements and assign conditions to each outcome</Step.Description></span>
                </div>
            </Step>

            {/**The final stage */}
            <Step active={page === 2} disabled={page < 2}>
                <div className="flex-center">
                    <span><Icon type='circle check' /></span>
                    <span className="ml-3"> <Step.Title onClick={() => dispatch(setBuildPage(2))}>Review Rule</Step.Title>
                    <Step.Description>Visualize and approve rule before publishing</Step.Description></span>
                </div>
            </Step>
        </Step.Group>
    )
}
export default Stepper