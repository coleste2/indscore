import React, { useEffect, useState } from "react"
import { useSnackbar } from "notistack"
import { Popup } from 'semantic-ui-react'
import { Button } from "react-bootstrap"
import { useDispatch, useSelector } from "react-redux"
import { deleteStatement, addStatement } from "../../../redux/actions"
import { useDrag, useDragLayer } from 'react-dnd'
import DropTypes from "../DropTypes"
import Chip from "@material-ui/core/Chip"
import { getEmptyImage } from 'react-dnd-html5-backend'
import { isIE } from 'react-device-detect'




//This component handles the entire box that both holds statements and combines them
const StatementList = () => {
    const statements = useSelector(state => state.buildPage_builtStatements)
    //Whether statements can actively clicked and combined
    const [combineMode, setCombineMode] = useState(false)
    //Keep count of all of the components that have been clicked
    const [clickedStatements, setClickedStatements] = useState([])
    const dispatch = useDispatch()
    const { enqueueSnackbar } = useSnackbar();

    //If we click a statement, add it if we dont have it, else, remove it
    const clickedStatement = (item) => {
        for (var i = 0; i < clickedStatements.length; i++) {
            if (item.label === clickedStatements[i].label) {
                var x = [...clickedStatements]
                x.splice(i, 1)
                setClickedStatements(x)
                return
            }
        }
        setClickedStatements([...clickedStatements, item])
    }
    //Check to see if a give element has been clicked
    const clickedContains = (item) => {
        for (var i = 0; i < clickedStatements.length; i++) {
            if (item.label === clickedStatements[i].label) return true
        }
        return false
    }

    //Switch mode so that component can now be or not be clicked
    const changeMode = () => {
        if (combineMode) setClickedStatements([])
        setCombineMode(!combineMode)
    }

    //Safely remove a statement by also removing from the list of clicked components
    const safeRemove = (item, index) => {
        for (var i = 0; i < clickedStatements.length; i++) {
            if (item.label === clickedStatements[i].label) {
                var x = [...clickedStatements]
                x.splice(i, 1)
                setClickedStatements(x)
            }
        }
        dispatch(deleteStatement(index))
    }
    //Comine statements with a given logical operator and reset the clicked statements tracker
    const combine = (linkage) => {
        if (clickedStatements.length < 2) {
            enqueueSnackbar("You must combine no less than two statement", { variant: "error" });
            return
        }
        var x = { combined: true, linkage: linkage, statements: clickedStatements }
        x.label = ""
        for (var i = 0; i < clickedStatements.length; i++) {
            x.label += clickedStatements[i].combined ? "(" + clickedStatements[i].label + ")" : clickedStatements[i].label
            if (i < clickedStatements.length - 1) x.label += linkage === "||" ? " OR " : " AND "
        }
        setClickedStatements([])
        setCombineMode(false)
        dispatch(addStatement(x))

    }

    return (<> {statements && statements.length > 0 ?
        <>
            <div className="combinerButtons">
                {combineMode &&
                //The buttons to combine...the buttons are disabled if the pre-conditions are not met
                <>
                    <Popup className="dropdownTooltip" content={clickedStatements.length < 2 ? "Cannot combine a single statement" : "Combine Statements"} size='mini' inverted position='top center' trigger={
                        <Button className={(clickedStatements.length < 2 ? "buttonDisable " : "") + "mr-2"} variant="secondary" onClick={() => combine("||")}>Combine via OR</Button>} />

                    <Popup className="dropdownTooltip" content={clickedStatements.length < 2 ? "Cannot combine a single statement" : "Combine Statements"} size='mini' inverted position='top center' trigger={
                        <Button className={(clickedStatements.length < 2 ? "buttonDisable " : "") + "mr-2"} variant="secondary" onClick={() => combine("&&")}>Combine via AND</Button>} />
                </>}
                {/** The button to change the mode...the button is disabled if the pre-conditions are not met*/}
                <Popup className="dropdownTooltip" content={statements.length <= 1 ? "Cannot combine a single statement" : combineMode ? "Cancel combination" : "Combine statements"} size='mini' inverted position='top center' trigger={
                    <Button className={statements.length <= 1 ? "buttonDisable" : ""} variant={combineMode ? "danger" : "primary"} onClick={() => changeMode()} >{combineMode ? "Cancel" : "Combine"}</Button>} />

            </div>
            <div className="ingredientsDragContainer">
                {/**Map all of the ingredients that are in the array below. These ingredients are movable cookies */}
                {statements.map((item, index) => {
                    //Render all statements and track clicks only if we are in the combine mode
                    if (!combineMode) return <Statement key={index} statement={item} index={index} remove={() => dispatch(deleteStatement(index))} />
                    else return <Statement key={index} statement={item} index={index} remove={() => safeRemove(item, index)} active={clickedContains(item)} onClick={(e, item) => clickedStatement(item)} />
                })}
            </div>
        </>
        :
        <div className="exportNotification"><h3 >Export ingredients from the prep box to deploy or combine them</h3></div>}</>)
}

export default StatementList




//This component renders a dragable cookie
export const Statement = ({ statement, index, onClick, active, remove }) => {
    //drag configuration
    const [{ isDragging }, drag, preview] = useDrag({
        item: { ...statement, type: DropTypes.statement },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
    })
    useEffect(() => {
        preview(getEmptyImage(), { captureDraggingState: true })
    }, [])
    const opacity = isDragging ? .4 : 1
    //The cookie that is dragable
    return (<>
        {isIE ? <div style={{ position: "relative", display: "inline-block" }}>
            <Chip ref={drag} className={active ? "selectedChip" : "prepChip"} style={{ opacity: isDragging ? 0 : opacity }} label={statement.label} onDelete={() => remove()} onClick={onClick ? (data) => onClick(data, statement, index) : undefined} />
            {isDragging && isIE && <Chip className={active ? "selectedChip" : "prepChip"} style={{ opacity: opacity, position: "absolute" }} label={statement.label} onDelete={() => remove()} onClick={onClick ? (data) => onClick(data, statement, index) : undefined} />}
        </div>
            :
            <Chip ref={drag} className={active ? "selectedChip" : "prepChip"} style={{ opacity: opacity }} label={statement.label} onDelete={() => remove()} onClick={onClick ? (data) => onClick(data, statement, index) : undefined} />
        }
{/**Show an active drag layer instead of the default screenshot because the screenshot breaks if component is too long...and statements can get long */}
        <DragLayer active={active} label={statement.label} />
    </>)
}

const layerStyles = {
    position: 'fixed',
    pointerEvents: 'none',
    zIndex: 100,
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
}
function getItemStyles(initialOffset, currentOffset) {
    if (!initialOffset || !currentOffset) {
        return {
            display: 'none',
        }
    }
    let { x, y } = currentOffset

    const transform = `translate(${x}px, ${y}px)`
    return {
        transform,
        WebkitTransform: transform,
    }
}

//Here is the creation of a drag layer to override the default screenshot method
export const DragLayer = ({ active, label }) => {
    const {
        itemType,
        isDragging,
        item,
        initialOffset,
        currentOffset,
    } = useDragLayer((monitor) => ({
        item: monitor.getItem(),
        itemType: monitor.getItemType(),
        initialOffset: monitor.getInitialSourceClientOffset(),
        currentOffset: monitor.getSourceClientOffset(),
        isDragging: monitor.isDragging(),
    }))
    function renderItem() {
        switch (itemType) {
            case DropTypes.statement:
                return <Chip className={active ? "selectedChip" : "prepChip"} style={{ opacity: .8 }} label={label} />

            default:
                return null
        }
    }
    if (!isDragging || label !== item.label || item.type !== "statement") {
        return null
    }
    return (
        <div style={layerStyles}>
            <div
                style={getItemStyles(initialOffset, currentOffset)}
            >
                {renderItem()}
            </div>
        </div>
    )
}
