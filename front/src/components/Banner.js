import React, { useState, useEffect } from "react"
import { Modal, Button } from "react-bootstrap"
import "./Banner.css"
import "../bootstrap/css/bootstrap.css"
import axios from "axios"
import Icon from "../bootstrap/icons"
import ReactPlayer from 'react-player'
import { useSelector } from "react-redux"
import { Accordion, Icon as SemanticIcon } from 'semantic-ui-react'
import { useLocation, useHistory } from "react-router-dom"

function Banner() {
    const user = useSelector(state => state.userInfo)
    //Setup state for whether the user modal is open or closed

    //Setup state for whether the about modal is open or closed\
    const { pathname } = useLocation()
    const history = useHistory()
    const [aboutModal, setAboutModal] = useState(pathname === "/info")

    //Setup state for the API endpoint checks

    const closeModal = () => {
        if (pathname === "/info"){
            history.push('/')
        }
            setAboutModal(false)
        
    }


    return (
        <div className="bannerHolder">
            <div className="CUSTOM_banner">
                <div className="ml-4 frontAndCenter">
                    <img className="DHSLogo" src={`${process.env.PUBLIC_URL}/logoPNG.png`} alt="DHS logo" />
                    <div>
                        <div className="titleText"><b>DEPARTMENT OF HOMELAND SECURITY</b></div>
                        <div className="titleText">AUTOMATED SCORING AND FEEDBACK</div>
                    </div>
                </div>
                <div className="pl-6 switchBanner">
                    <div >{user.username} <span className="ml-1 h2 "><Icon type="person" /></span></div>
                    <div onClick={() => setAboutModal(true)} style={{ fontSize: "1rem", cursor: "pointer" }}>About/Support <span className="ml-1 h2"  ><Icon type="tools" /></span></div>

                </div>
            </div>

            {/*The about the application and video tutorial section modal*/}
            {aboutModal && <Modal show={aboutModal} onHide={() => closeModal()} size="lg" centered style = {{zIndex:99999999999999999}} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        AUTOMATED SCORING AND FEEDBACK
              <div className="ribbon" style={{ pointerEvents: "none" }}><span>Support</span></div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <>
                        <div className="aboutAndSupport">
                            <p class="mb-0"><img class="logo supportLogo mr-4 mb-0" alt="dhs logo" src={`${process.env.PUBLIC_URL}/logo.svg`} width="200" height="200" /></p>
                            <div>
                                <p class="supportText"><b>Version:</b> 1.4</p>
                                <p class="supportText"><b>Deployed Date:</b> 12/23/2020</p>
                                <p class="ProductOwnerTitle"><b>Product Owner(s):</b> Marlon Taylor, Kevin Klein</p>
                                <p class="ProductOwnerTitle"><b>Developer POC:</b> Matthew Ferrari</p>
                            </div>
                        </div>
                        
                        <VideoDropdown />
                    </>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => closeModal()}>Close</Button>
                </Modal.Footer>
            </Modal>}
        </div >
    )
}
export default Banner

const VideoDropdown = () => {
    const [index, setIndex] = useState(0)

    return (
        <>
        <h3 style = {{textAlign:"center", marginTop:"0"}}>Video Demos and Walkthroughs</h3>
        <Accordion fluid styled>
            <Accordion.Title
                active={index === 0}
                index={0}
                onClick={() => setIndex(0)}
            >
                <SemanticIcon name='dropdown' />
    Part 1: Viewing and Filtering Scored Indicators
        </Accordion.Title>
            <Accordion.Content active={index === 0}>
                <ReactPlayer className='react-player' url={`${process.env.PUBLIC_URL}/ais_demo_p1.mp4`} width='100%' height='200px' controls={true} />
            </Accordion.Content>

            <Accordion.Title
                active={index === 1}
                index={1}
                onClick={() => setIndex(1)}
            >
                <SemanticIcon name='dropdown' />
          Part 2: Creating and Understanding Rules
        </Accordion.Title>
            <Accordion.Content active={index === 1}>
                <ReactPlayer className='react-player' url={`${process.env.PUBLIC_URL}/ais_demo_p2.mp4`} width='100%' height='200px' controls={true} />

            </Accordion.Content>

            <Accordion.Title active={index === 2} index={2} onClick={() => setIndex(2)}>
                <SemanticIcon name='dropdown' />Part 3: Comparing Rules in the Sandbox
        </Accordion.Title>
            <Accordion.Content active={index === 2}>
                <ReactPlayer className='react-player' url={`${process.env.PUBLIC_URL}/ais_demo_p3.mp4`} width='100%' height='200px' controls={true} />
            </Accordion.Content>
        </Accordion>
        </>
    )
}
