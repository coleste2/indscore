import React from "react"

import mermaid from 'mermaid'
import { isIE } from 'react-device-detect'
import { FallBack } from "./Error"
import { ErrorBoundary } from "react-error-boundary"
export default function Graph({ rules }) {
    return (<>{isIE ?
        //Mermaid does not work with IE so show error message instead

        <p>Cannot display chart in IE</p> :
        <ErrorBoundary FallbackComponent = {FallBack}>
        <div style={{ overflow: "auto" }}><Mermaid>
            graph TD;

        {/* Render a start header for the chart to occur */}

A{"([Start])"}-->

{/* Leave the back side of the connector blank and let the next in line take car of it
 This holds true until the default statement in which it only renders itself and leaves
nothing hanging for another node because no more are coming */}

            {rules.map((rule, index) => {
                //Render the back side, an arrow that show the result if it is true, and the backside of the next node if it is false and we continue down the chain
                if (!rule.isDefault) return `${alphabet[index + 1]}("${rule.rule.label.replace(/'/g, "#rsquo;").replace(/"/g, "#quot;")}");${alphabet[index + 1]}--Yes-->${alphabet[index + 1]}1{{${rule.scoreName} - ${rule.score}}};${alphabet[index + 1]}--No-->`
                //Render the last node and just use it as a cap to ceil the chart
                else return `${alphabet[index + 1]}{{${rule.scoreName} - ${rule.score}}};`
            })}
        </Mermaid></div>
        </ErrorBoundary>
    }</>)
}

const alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

mermaid.initialize({
    startOnLoad: true,
});

class Mermaid extends React.Component {
    componentDidMount() {
        mermaid.contentLoaded();
    }
    render() {
        return <div className="mermaid">{this.props.children}</div>;
    }
}
