import React from 'react';
import { Navbar, Nav } from "react-bootstrap"
import 'bootstrap/dist/css/bootstrap.min.css';
import { NavLink, useLocation } from 'react-router-dom'

import './Navbar.css';
function MyNavBar() {
    const { pathname } = useLocation()
    return (
        <div>
            <Navbar bg="dark" className={`blue ${pathname === "/" || pathname === "/info" ? "notBottomElt" : ""}`} expand="lg">
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">

                        {/* Link to the component that the button represent, and if the current url matches 
                    the link it is linking to, then add the underline style*/}
                        <NavLink 
                        isActive={(match) => {
                            if (match || pathname === "/info") {
                                return true;
                            }
                        }}
                        exact to="/" className="MyButton" activeClassName="activeLinkButton" > <div >Scored Indicators</div></NavLink>
                        <NavLink exact to="/build" className="MyButton" activeClassName="activeLinkButton"><div >Build Rule</div></NavLink>
                        <NavLink to="/rules" className="MyButton" activeClassName="activeLinkButton"><div >Rules</div></NavLink>
                        <NavLink to="/sandbox" className="MyButton" activeClassName="activeLinkButton"><div >Sandbox</div></NavLink>

                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </div>
    );
}

export default MyNavBar;