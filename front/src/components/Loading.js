import React from "react"
import { Spinner } from "react-bootstrap"
import logo from "../bootstrap/img/cisa500.png"
import "./Loading.css"
import { Loader } from 'react-loaders';
import 'loaders.css/loaders.min.css';

const LoadingScreen = ({ status }) => {
    return (<LoadingLogo >
        {status && <div className="loadingMessage">
                <h2  style={{ textAlign: "center", margin:"0px", padding:"0px" }}>{status}</h2>
                <Loader type="ball-pulse" color="black" className="loaderAnimation" />
        </div>}
    </LoadingLogo>)
}
export default LoadingScreen
export const LoadingLogo = ({ children }) => {
    return (<div >
        <div className="cursorHack" />
        <div className="mb-4">
            <img className="SpinnerImage" alt="" src={logo} />
            <Spinner size="lg" className="Spinner" animation="border" />
        </div>
        {children}
    </div>)
}
