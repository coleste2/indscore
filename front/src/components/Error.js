import React from "react"
import {useHistory} from "react-router-dom"
import {Button} from "@material-ui/core"
//404 Page Not Found screen
export default function Error({ error, title, message,children, fullScreen = true}) {
  return (
    <div  style={fullScreen ?{ marginTop:"5%",width: "100%", height: "100%", display: "flex", justifyContent: "center", alignItems: "center" }:{ width: "100%", height: "100%", display: "flex", justifyContent: "center", alignItems: "center" }}>
      <div style={{ display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column" }}>
        <CartFire width="20rem" />
        <h1><span style={{ fontSize: "40px", fontWeight: "600", color: "red" }}>{error}:&nbsp;</span>{title}</h1>
        <p>{message}</p>
        {children}
      </div>
    </div>)
}

export const FallBack = ({error, componentStack,fullScreen=false}) =>{ 
const history = useHistory()
  return (
  <Error fullScreen = {fullScreen} error = "Error" title = {error ? error.message : ""} message = {componentStack}>
    <Button variant = "contained" color = "primary"
            onClick={() => {
       history.go(0)
      }}
    >
      Reset Page
    </Button>
  </Error>
)}
function CartFire(props) {
  return (
    <svg
      id="prefix__Layer_1"
      data-name="Layer 1"
      width={512}
      height={400.89}
      viewBox="0 0 512 680.89"
      {...props}
    >
      <defs>
        <style>
          {
            ".prefix__cls-1{fill:#ff6536}.prefix__cls-2{fill:#cacacd}.prefix__cls-3{fill:#5b5b5f}.prefix__cls-4{fill:#ff421d}"
          }
        </style>
      </defs>
      <path
        className="prefix__cls-1"
        d="M116.57 331.24v33.4h79l8.28 66.27h-62.93v33.4H208l8.29 66.28H167V564h286.8L512 331.24zm203.86 99.67v-66.27h57.82L370 430.91zm45.36 33.4l-8.28 66.28h-37.08v-66.28zm46.12-99.67h57.31l-16.57 66.27h-49zm-182.7 0H287v66.27h-49.51zm12.45 99.67H287v66.28h-37zm149.51 66.28l8.28-66.28h44.85l-16.56 66.28z"
        transform="translate(0 -21.5)"
      />
      <path
        className="prefix__cls-2"
        d="M402.35 623.11H172.96L82.8 262.51H0v-33.4h108.88l90.15 360.6h203.32v33.4z"
      />
      <path
        className="prefix__cls-3"
        d="M178.32 702.39a45.59 45.59 0 1145.58-45.59 45.64 45.64 0 01-45.58 45.59z"
        transform="translate(0 -21.5)"
      />
      <path
        className="prefix__cls-2"
        d="M178.32 671.58a14.78 14.78 0 1114.78-14.78 14.8 14.8 0 01-14.78 14.78z"
        transform="translate(0 -21.5)"
      />
      <path
        className="prefix__cls-3"
        d="M400.53 702.39a45.59 45.59 0 1145.59-45.59 45.64 45.64 0 01-45.59 45.59z"
        transform="translate(0 -21.5)"
      />
      <path
        className="prefix__cls-2"
        d="M400.53 671.58a14.78 14.78 0 1114.78-14.78 14.8 14.8 0 01-14.78 14.78z"
        transform="translate(0 -21.5)"
      />
      <path
        className="prefix__cls-4"
        d="M512 331.24H303.71V564h150.1zM357.51 530.59h-37.08v-66.28h45.36zM370 430.91h-49.57v-66.27h57.82zm57.76 99.68h-36.59l8.28-66.28h44.85zm-24.1-99.68l8.28-66.27h57.31l-16.57 66.27z"
        transform="translate(0 -21.5)"
      />
      <path
        className="prefix__cls-1"
        d="M158.29 178.41s14.53 18.67 45 34.48c0 0-22.05-163.07 121.65-191.39-36.82 118.21 46.83 151.55 77.32 89.1C453.16 170.08 414.5 222 414.5 222c20.88 2.63 38.41-17.33 38.41-17.33.16 2.46.25 4.94.25 7.43 0 72.44-67.13 131.17-149.93 131.17S153.3 284.51 153.3 212.07a115.94 115.94 0 014.99-33.66z"
        transform="translate(0 -21.5)"
      />
      <path
        className="prefix__cls-4"
        d="M453.39 204.64S435.86 224.6 415 222c0 0 38.66-51.89-12.26-111.37-30.49 62.45-114.14 29.11-77.32-89.1a178 178 0 00-21.69 5.67v316.04c82.8 0 149.93-58.73 149.93-131.17-.02-2.5-.11-4.97-.27-7.43z"
        transform="translate(0 -21.5)"
      />
      <path
        d="M250 288.12c0 26.66 24 48.27 53.57 48.27s53.58-21.61 53.58-48.27c0-14.26-6.88-27.09-17.8-35.92-20.71 25.35-50.23-13.11-27.23-41.2.02 0-62.12 7-62.12 77.12z"
        transform="translate(0 -21.5)"
        fill="#fbbf00"
      />
      <path
        d="M357.41 288.12c0-14.26-6.88-27.09-17.8-35.92-20.71 25.35-50.23-13.11-27.23-41.2a60.06 60.06 0 00-8.55 1.86v123.53c29.59 0 53.58-21.61 53.58-48.27z"
        transform="translate(0 -21.5)"
        fill="#ffa900"
      />
    </svg>
  )
}
