import React, { useEffect } from "react"
import Banner from "./components/Banner"
import Navbar from "./components/Navbar"
import ScoredIndicators from "./pages/scoredIndicators"
import BuildRule from "./pages/buildRule"
import Sandbox from "./pages/sandbox"
import Rules from "./pages/rules"
import Rule from "./pages/rules/rule"
import Error from "./components/Error"
import { Route, Switch } from "react-router-dom"
import { ErrorBoundary } from 'react-error-boundary'
import { FallBack } from "./components/Error"
import axios from "axios"
import api from "./components/api"
import { useDispatch } from "react-redux"
import { setUserData } from "./redux/actions"
function App() {

    const dispatch = useDispatch()

    useEffect(() => {
        axios.defaults.headers['Pragma'] = 'no-cache'
        loginUser()
    }, [])

    const loginUser = async () => {
        try {
            const { data } = await axios.get(`${api}/login.php`)
            dispatch(setUserData(data))
        }
        catch (err) {
            console.log(err)
        }
    }

    return (
        <div style={{ display: "flex", flexDirection: "column" }}>
            <ErrorBoundary FallbackComponent={FallBack}>
                <Banner />
            </ErrorBoundary>
            <ErrorBoundary FallbackComponent={FallBack}>
                <Navbar />
            </ErrorBoundary>

            {/*Render either proper body depending on the route but always render the top banner and navbar */}
            <ErrorBoundary FallbackComponent={FallBack}>
                <Switch>
                    <Route exact path="/" component={ScoredIndicators} />
                    <Route exact path="/info" component={ScoredIndicators} />
                    <Route exact path="/build" component={BuildRule} />
                    <Route exact path="/rules" component={Rules} />
                    <Route exact path="/rules/:id" component={Rule} />
                    <Route exact path="/sandbox" component={Sandbox} />
                    <Route path="/" component={Error404} />
                </Switch>
            </ErrorBoundary>
        </div>
    )
}
export default App

const Error404 = () => <Error error="404" title="Page not found" message="You have tried to reach a page that is not currently part of the AIS application" />