
//Actions are simply envelopes with an address (type). To see where they end up, check the reducers file

//Action to set the user login data
export const setUserData = (info) => {
    return {
        type: "SET_USER_INFO",
        payload:{
            userInfo: info
        }
    }
}

//Action to set the highlight type on the score details page
export const setHighlightDetails = (type)=>{
    return {
        type: "SET_HIGHLIGHT_TYPE",
        payload:{
            highlightType: type
        }
    }
}
//Action to set the display type on the score details page
export const setDisplayDetails = (type)=>{
    return {
        type: "SET_DISPLAY_TYPE",
        payload:{
            displayType: type
        }
    }
}
//Action to set the sort type on the details page
export const setSortDirection = (dir)=>{
    return {
        type: "SET_DIRECTION",
        payload:{
            direction: dir
        }
    }
}

//Action to set the sort by property on score details page
export const setSortProp = (prop)=>{
    return {
        type: "SET_PROP",
        payload:{
            prop: prop
        }
    }
}
//Action to set the filter on the score page
export const setFilter = (filter)=>{
    return {
        type: "SET_FILTER",
        payload:{
            filter: filter
        }
    }
}

//Middleware action to remove an item from the filter
export const knockFilter = (knock)=>{
    return {
        type: "KNOCK_FILTER",
        payload:{
            knock: knock
        }
    }
}

//Action to set the build page on the build rule page
export const setBuildPage = (page)=>{
    return {
        type: "SET_PAGE",
        payload:{
            page: page
        }

    }
}

//Action to add a rule to the list of rules on the build rule page
export const addRule = (rule)=>{
    return {
        type: "ADD_RULE",
        payload:{
            rule: rule
        }

    }
}
export const clearRules = ()=>{
    return {
        type: "CLEAR_RULES",
        }
}

//Action to remove a rule to the list of rules on the build rule page
export const deleteRule = (index)=>{
    return {
        type: "DELETE_RULE",
        payload:{
            index: index
        }

    }
}

//Action to change the properties (opinion, category, score, etc.) of the rule on the build rule page
export const setRuleBase = (rule, index)=>{
    return {
        type: "SET_RULE_BASE",
        payload:{
            rule: rule,
            index: index
        }

    }
}

//Action to set the actual logical rule to a specific score outcome on the build rule page
export const setRuleRule = (rule, index)=>{
    return {
        type: "SET_RULE_RULE",
        payload:{
            rule: rule,
            index: index
        }

    }
}

//Action to remove the actual logical rule from a specific score outcome on the build rule page
export const deleteRuleRule = (index)=>{
    return {
        type: "DELETE_RULE_RULE",
        payload:{
            index: index
        }

    }
}
//Action to swap the index of a rule and move everything up
export const moveRule = (oldIndex, newIndex)=>{
    return {
        type: "MOVE_RULE",
        payload:{
            oldIndex: oldIndex,
            newIndex: newIndex
        }

    }
}

//Action to add a statement (prepared ingredient) to the list of statements
export const addStatement = (statement)=>{
    return {
        type: "ADD_STATEMENT",
        payload:{
            statement: statement,
        }

    }
}

//Action to remove a statement (prepared ingredient) from the list of statements
export const deleteStatement = (index)=>{
    return {
        type: "DELETE_STATEMENT",
        payload:{
            index: index,
        }

    }
}
export const setSandboxSearch = (search)=>{
    return {
        type: "SET_SANDBOX_SEARCH",
        payload:{
            search: search,
        }

    }
}