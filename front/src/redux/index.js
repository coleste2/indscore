import { highlight, display, filter, sort, filterKnocker, buildPage, buildRule, builtStatements, userInfo, sandboxSearch } from "./reducers"
import { combineReducers } from "redux"

//This takes all of the reducers and combines them into one to create the store with
const funnelReducer = combineReducers({
    userInfo,
    results_highlightDetails: highlight,
    results_displayDetails: display,
    results_sort: sort,
    results_filter: filter,
    results_filterKnocker: filterKnocker,
    buildPage_page: buildPage,
    buildPage_buildRule: buildRule,
    buildPage_builtStatements: builtStatements,
    sandbox_search:sandboxSearch
})
export default funnelReducer