import arrayMove from 'array-move';

export const userInfo = (state = { username: "", id: 1 }, action) => {
    switch (action.type) {
        case "SET_USER_INFO":
            return action.payload.userInfo
        default:
            return state
    }
}

//mini piece of store that controls how to higlight the scored indicators
export const highlight = (state = "none", action) => {
    switch (action.type) {
        case "SET_HIGHLIGHT_TYPE":
            return action.payload.highlightType
        default:
            return state
    }
}

//mini piece of state that controls how to display the scored indicators
export const display = (state = "list", action) => {
    switch (action.type) {
        case "SET_DISPLAY_TYPE":
            return action.payload.displayType
        default:
            return state
    }
}

//mini piece of state that controls how to sort the scored indicators
export const sort = (state = { direction: "d", prop: "Date Scored" }, action) => {
    switch (action.type) {
        case "SET_DIRECTION":
            var x = JSON.parse(JSON.stringify(state))
            x.direction = action.payload.direction
            return x
        case "SET_PROP":
            var y = JSON.parse(JSON.stringify(state))
            y.prop = action.payload.prop
            return y
        default:
            return state
    }
}

//mini piece of state that controls the active filter that filters the scored indicators
export const filter = (state = {}, action) => {
    switch (action.type) {
        case "SET_FILTER":
            return action.payload.filter
        default:
            return state
    }
}

//mini piece of state that tracks what is about to be removed from filter so that components can prepare for the change
export const filterKnocker = (state = { key: null, index: 0 }, action) => {
    switch (action.type) {
        case "KNOCK_FILTER":
            return action.payload.knock
        default:
            return state
    }
}

//mini piece of state that controls what page the build rule is on
export const buildPage = (state = 0, action) => {
    switch (action.type) {
        case "PREV_PAGE":
            return state - 1
        case "NEXT_PAGE":
            return state + 1
        case "SET_PAGE":
            return action.payload.page
        default:
            return state
    }
}
//mini piece of state that controls the rules that are being created
export const buildRule = (state = [{ category: 'Logical', score: 30, scoreName: "Doubtful", opinion: "Disagree", isDefault: 1, rule: { label: "default", isDefault: true } }], action) => {
    switch (action.type) {
        case "ADD_RULE":
            return [{ ...action.payload.rule }, ...state]
        case "DELETE_RULE":
            var x = [...state]
            x.splice(action.payload.index, 1)
            return x
        case "DELETE_RULE_RULE":
            x = [...state]
            delete x[action.payload.index].rule
            return x
        case "SET_RULE_BASE":
            x = [...state]
            x[action.payload.index] = { ...action.payload.rule }
            return x
        case "SET_RULE_RULE":
            x = [...state]
            x[action.payload.index].rule = { ...action.payload.rule }
            return x
        case "MOVE_RULE":
            return arrayMove([...state], action.payload.oldIndex, action.payload.newIndex)
        case "CLEAR_RULES":
            return [{ category: 'Logical', score: 30, scoreName: "Doubtful", opinion: "Disagree", isDefault: 1, rule: { label: "default", isDefault: true } }]
        default:
            return state
    }
}

//mini piece of state that controls the prepared rules
export const builtStatements = (state = [], action) => {
    switch (action.type) {
        case "ADD_STATEMENT":
            for (var i = 0; i < state.length; i++) {
                if (state[i].label === action.payload.statement.label) return state
            }
            return [...state, action.payload.statement]
        case "DELETE_STATEMENT":
            var x = [...state]
            x.splice(action.payload.index, 1)
            return x
        default:
            return state
    }
}

//mini piece of state that controls the prepared rules
export const sandboxSearch = (state = null, action) => {
    switch (action.type) {
        case "SET_SANDBOX_SEARCH":
            return action.payload.search
        default:
            return state
    }
}