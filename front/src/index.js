import "react-app-polyfill/ie11"
import "react-app-polyfill/stable"
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App.js';
import { Provider } from "react-redux"
import { createStore } from "redux"
import funnel from "./redux"
import { BrowserRouter } from "react-router-dom"
import Icon from "./bootstrap/icons"
import { SnackbarProvider } from "notistack"
import { ErrorBoundary } from "react-error-boundary"
import { pathname } from "./components/api"
import {FallBack} from "./components/Error"
//Create a redux store that supports development tools chrome extension
const store = createStore(
  funnel, /* preloadedState, */ window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  /*Wrap entire program in Error Handler*/
  <ErrorBoundary FallbackComponent={({error, componentStack, resetErrorBoundary})=><FallBack error = {error} resetErrorBoundary = {resetErrorBoundary} componentStack = {componentStack} fullScreen = {true}/>}>
    {/*Wrap entire program in router*/}
    <BrowserRouter basename={pathname}>
      {/*Wrap entire program in the redux store*/}
      <Provider store={store}>
        <React.StrictMode>
          {/*Wrap entire program in a global notification delivery system*/}
          <ErrorBoundary FallbackComponent={({error, componentStack, resetErrorBoundary})=><FallBack error = {error} resetErrorBoundary = {resetErrorBoundary} componentStack = {componentStack} fullScreen = {true}/>}>
            <NotificationWrapper >
              {/*The Application */}
              <App />
            </NotificationWrapper>
          </ErrorBoundary>
        </React.StrictMode>
      </Provider>
    </BrowserRouter>
  </ErrorBoundary>,
  document.getElementById('root')
);

//Wrapper of application that allows us to send alerts globaly with ease
function NotificationWrapper(props) {
  const notistackRef = React.createRef();
  const onClickDismiss = key => () => {
    notistackRef.current.closeSnackbar(key);
  }
  return (
      <SnackbarProvider maxSnack={3} iconVariant={
        //Change the error icon to something better 
        { error: <span className="mr-2"><Icon type="alert" /></span> }}
        ref={notistackRef}
        //Add dismiss button to alerts
        action={(key) => (<b style={{ color: "black", cursor: "pointer", margin: "0" }} onClick={onClickDismiss(key)}>Dismiss</b>)}>
        {props.children}
      </SnackbarProvider>
  );
}