const cors = require("cors");

var indicatorResource1 = {
    links: ["google.com", "amazon.com"],
    id: 121,
    type: "domain",
    value: { name: "0.bg" },
    benign: {value: false},
    tip: "undetermined",
    verified: false,
    reportCount: 0,
    lastHit: "2020-10-10",
    tasked: false,
    hitCount: 20000,
    reportedDates: [{ date: "2020-06-12" }, { date: "2020-06-12" }, { date: "2020-06-12" }, { date: "2020-06-12" }, { date: "2020-11-9" }],
}

var indicatorResource2 = {
    links: ["goo''gle.com", "a'mazon.com"],
    id: 122,
    lastHit: "2020-10-10",

    type: "email",
    value: { name: "hello" },
    tlp: "undetermined",
    tasked: false,
    benign: {value: true},
    hitCount: 0,
    verified: true,
    reportCount: 1,
    reportedDates: [{ date: "2020-06-12" }, { date: "2020-06-12" }, { date: "2020-06-12" }, { date: "2020-06-12" }, { date: "2020-11-9" }],

}
var indicatorResource3 = {
    links: ["google.com", "amazon.com"],
    lastHit: "2020-10-10",
    hitCount: 20000,
    id: 123,
    type: "email",
    value: { name: "status u" },
    tlp: "undetermined",
    tasked: false,
    benign: {value: false},
    status:'u',
    reportCount: 5,
    verified: false,
    reportedDates: [{ date: "2020-06-12" }, { date: "2020-06-12" }, { date: "2020-06-12" }, { date: "2020-06-12" }, { date: "2020-11-9" }],
}
var indicatorResource4 = {
    links: ["google.com", "amazon.com"],
    lastHit: "2020-10-10",
    benign: {value: false},
    status:'ar',
    id: 124,
    value: { name: "status ar" },
    type: "email",
    tasked: false,
    reportedDates: [{ date: "2020-06-12" }, { date: "2020-06-12" }, { date: "2020-06-12" }, { date: "2020-06-12" }, { date: "2020-11-9" }],
    tlp: "undetermined",
    hitCount: 0,
    reportCount: 3,
}

var evidenceResource1 = {
    attackPattern: "pattern",
    links: ["google.com", "amazon.com"],
    id: 123456,
    title: { name: "myTitle" },
    type: "image",
    sourceURL: "myEvidence.com",
    sourceTitle: "FEDGOV",

    sourceId: 1,
    fileSize: 2000,
    fileName: "myImage.png",
    description: { name: "An image that is significant contains important evidence :)" },
    activityDate: "05/05/2005",
    reportedDates: [{ date: "2020-06-12" }],
    actor: "Dwayne 'The Rock' Johnson",
    exploitStage: "Metlife Stadium",
    targets: "Bullseye",
    analyzedSatus: "r",
    indicatorStatus: "g",
}
var evidenceResource2 = {
    attackPattern: "pattern",
    links: ["google.com", "amazon.com"],
    id: 999123123,
    sourceTitle: "he'y",
    sourceId: 5,
    type: "image",
    sourceURL: "myEvidence.com",
    fileSize: 2000,
    fileName: "myImage.png",
    description: { name: "An image that is significant contains important evidence :)" },
    activityDate: "05/05/2005",
    reportedDates: [{ date: "2020-06-12" }],
    actor: "Dwayne 'The Rock' Johnson",
    exploitStage: "Metlife Stadium",
    targets: "Bullseye",
    analyzedSatus: "r",
    indicatorStatus: "g",
}

var evidenceResource3 = {
    attackPattern: "pattern",
    links: ["google.com", "amazon.com"],
    id: 1234568,
    sourceTitle: "today",

    title: { name: "myTitle" },
    type: "image",
    sourceId: 126969,
    sourceURL: "myEvidence.com",
    fileSize: 2000,
    fileName: "myImage.png",
    description: { name: "An image that is significant contains important evidence :)" },
    activityDate: "05/05/2005",
    reportedDates: [{ date: "2020-06-12" }],
    actor: "Dwayne 'The Rock' Johnson",
    exploitStage: "Metlife Stadium",
    targets: "Bullseye",
    analyzedSatus: "r",
    indicatorStatus: "g",
}

const express = require('express')

var app = express()
app.use(cors())

const log = (req, res, next) => {
    console.log(req.url)
    next()
}
const actorEndpoint = (req, res) => {
    console.log("hit")
    console.log(req.query.id)
    try{
        if (req.query.id) {
            return res.send({ ...response3, id: req.query.id });
        }
        else return res.send(response4)
    } catch (error) {
        console.log(error);
        throw error;
    }
};
const malwareEndpoint = (req, res) => {
    try {
        return res.send("malware endpoint hit!" + req.params.value);
    } catch (error) {
        console.log(error);
        throw error;
    }
};
const indicatorEndpoint = (req, res) => {
    try {
        return res.send("indicator endpoint hit!" + req.params.value);
    } catch (error) {
        console.log(error);
        throw error;
    }
};
const evidenceEndpoint = (req, res) => {
    try {
        return res.send("evidence endpoint hit!" + req.params.value);
    } catch (error) {
        console.log(error);
        throw error;
    }
};

app.get(("/api/1_0/evidence"), log, (req, res) => res.send(evidenceResultsPage))
app.get(("/api/1_0/evidence/1234568/indicator"), log, (req, res) => res.send(indicatorResultsPage3))
app.get(("/api/1_0/evidence/123456/indicator"), log, (req, res) => res.send(indicatorResultsPage1))
app.get(("/api/1_0/evidence/123123/indicator"), log, (req, res) => res.send(indicatorResultsPage2))
app.get(("/api/1_0/evidence/999123123/indicator"), log, (req, res) => res.send(indicatorResultsPage2))

app.get(("/api/1_0/evidence/123456"), log, (req, res) => res.send(evidenceResource1))
app.get(("/api/1_0/evidence/123123"), log, (req, res) => res.send(evidenceResource2))
app.get(("/api/1_0/evidence/1234568"), log, (req, res) => res.send(evidenceResource3))
app.get(("/api/1_0/evidence/999123123"), log, (req, res) => res.send(evidenceResource3))

app.get(("/api/1_0/indicator/121/evidence"), log, (req, res) => res.send(evidenceResultsPageSmall1))
app.get(("/api/1_0/indicator/122/evidence"), log, (req, res) => res.send(evidenceResultsPageSmall1))
app.get(("/api/1_0/indicator/123/evidence"), log, (req, res) => res.send(evidenceResultsPageSmall2))
app.get(("/api/1_0/indicator/124/evidence"), log, (req, res) => res.send(evidenceResultsPageSmall2))

app.get(("/api/1_0/indicator/121"), log, (req, res) => res.send(indicatorResource1))
app.get(("/api/1_0/indicator/122"), log, (req, res) => res.send(indicatorResource2))
app.get(("/api/1_0/indicator/123"), log, (req, res) => res.send(indicatorResource3))
app.get(("/api/1_0/indicator/124"), log, (req, res) => res.send(indicatorResource4))

app.get(("/api/1_0/indicator"), log, (req, res) => res.send(indicatorResultsPage1))

app.listen(5000, () => console.log("Listening on port 5000"))

app.get("/actors", log, actorEndpoint);
app.get("/malware/:value", log, malwareEndpoint);
app.get("/indicator/:value", log, indicatorEndpoint);
app.get("/evidence/:value", log, evidenceEndpoint);

app.get("/getAttackPatterns", log, (req, res) => res.send(namePairList));
app.get("/getExploitStages", log, (req, res) => res.send(namePairList));
app.get("/getMalwares", log, (req, res) => res.send(malwaresList));
app.get("/getTargets", log, (req, res) => res.send(namePairList));


var namePairList = {
    results: Array.from(Array(50)).map(item => { return {name:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Illi enim inter se dissentiunt.", id: Math.round(Math.random()*100)}}),
    pageSize: 10,
    page: 1,
    totalResults: 3,
    totalPages: 3,
}
var malwaresList = {
    results: Array.from(Array(50)).map(item => { return {title:{name:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Illi enim inter se dissentiunt."}, id: Math.round(Math.random()*100)}}),
    pageSize: 10,
    page: 1,
    totalResults: 3,
    totalPages: 3,
}
var evidenceResultsPage = {
    results: Array.from(Array(50)).map(item => Math.random() > .5 ? evidenceResource2 : evidenceResource1),
    pageSize: 10,
    page: 1,
    totalResults: 3,
    totalPages: 100,
    links: ["google.com", "amazon.com"]
}
// var evidenceResultsPage = {
//     results: [evidenceResource2, evidenceResource1, evidenceResource3],
//     pageSize: 10,
//     page: 1,
//     totalResults: 3,
//     totalPages: 100,
//     links: ["google.com", "amazon.com"]
// }

var evidenceResultsPageSmall1 = {
    results: [evidenceResource1, evidenceResource2],
    pageSize: 10,
    page: 1,
    totalResults: 1,
    totalPages: 2,
    links: ["google.com", "amazon.com"]
}
var evidenceResultsPageSmall2 = {
    results: [evidenceResource2],
    pageSize: 10,
    page: 1,
    totalResults: 1,
    totalPages: 1,
    links: ["google.com", "amazon.com"]
}
var indicatorResultsPage1 = {
    results: [indicatorResource1, indicatorResource2],
    pageSize: 10,
    page: 1,
    totalResults: 4,
    totalPages: 1,
    links: ["google.com", "amazon.com"]
}
var indicatorResultsPage2 = {
    results: [indicatorResource3, indicatorResource4, indicatorResource3],
    pageSize: 100,
    page: 1,
    totalResults: 3,
    totalPages: 1,
    links: ["google.com", "amazon.com"]
}
var indicatorResultsPage3 = {
    results: [indicatorResource1, indicatorResource3],
    pageSize: 100,
    page: 1,
    totalResults: 2,
    totalPages: 1,
    links: ["google.com", "amazon.com"]
}

const response1 = require("./responses/response_1.json");
const response2 = require("./responses/response_2.json");
const response3 = require("./responses/response_3.json");
const response4 = require("./responses/response_4.json");
const response5 = require("./responses/response_5.json");

/**
 *
 * @description replicating illuminate's actors endpoint for local environment.
 */
