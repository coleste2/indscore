-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: indscore
-- ------------------------------------------------------
-- Server version	5.7.30-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ais_indicator_scoring`
--

DROP TABLE IF EXISTS `ais_indicator_scoring`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ais_indicator_scoring` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL,
  `evidence_id` int(11) NOT NULL,
  `evidence_source_name` varchar(50) DEFAULT NULL,
  `evidence_source_id` int(11) DEFAULT NULL,
  `indicator_id` int(11) NOT NULL,
  `datetime_of_score` datetime NOT NULL,
  `score_value` smallint(6) NOT NULL,
  `score_name` varchar(50) NOT NULL,
  `score_explanation` varchar(200) NOT NULL,
  `opinion_value` varchar(20) NOT NULL,
  `indicator_value` varchar(1028) DEFAULT NULL,
  `indicator_type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rule_id` (`rule_id`),
  CONSTRAINT `ais_indicator_scoring_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `ais_indicator_scoring_rules` (`rule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=388 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ais_indicator_scoring_analysts`
--

DROP TABLE IF EXISTS `ais_indicator_scoring_analysts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ais_indicator_scoring_analysts` (
  `analyst_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `privilege_level` int(11) DEFAULT '0',
  `login_hits` int(11) DEFAULT '1',
  PRIMARY KEY (`analyst_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ais_indicator_scoring_content`
--

DROP TABLE IF EXISTS `ais_indicator_scoring_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ais_indicator_scoring_content` (
  `id` int(11) NOT NULL,
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `idLinkForContent` FOREIGN KEY (`id`) REFERENCES `ais_indicator_scoring` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ais_indicator_scoring_cron`
--

DROP TABLE IF EXISTS `ais_indicator_scoring_cron`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ais_indicator_scoring_cron` (
  `last_evidence_id` int(11) NOT NULL,
  PRIMARY KEY (`last_evidence_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ais_indicator_scoring_rules`
--

DROP TABLE IF EXISTS `ais_indicator_scoring_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ais_indicator_scoring_rules` (
  `rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `rule` text NOT NULL,
  `analyst_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`rule_id`),
  KEY `analyst_id_idx` (`analyst_id`),
  KEY `rule_status_idx` (`status`),
  CONSTRAINT `analyst_id2` FOREIGN KEY (`analyst_id`) REFERENCES `ais_indicator_scoring_analysts` (`analyst_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rule_status` FOREIGN KEY (`status`) REFERENCES `ref_rules_status` (`status`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ais_indicator_scoring_rules_workflow`
--

DROP TABLE IF EXISTS `ais_indicator_scoring_rules_workflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ais_indicator_scoring_rules_workflow` (
  `new_status` tinyint(1) DEFAULT NULL,
  `analyst_id` int(11) NOT NULL,
  `message` varchar(250) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `comment` tinyint(1) NOT NULL DEFAULT '0',
  `posted` datetime NOT NULL,
  KEY `rule_id_idx` (`rule_id`),
  KEY `analyst_id_idx` (`analyst_id`),
  CONSTRAINT `analyst_id` FOREIGN KEY (`analyst_id`) REFERENCES `ais_indicator_scoring_analysts` (`analyst_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rule_id` FOREIGN KEY (`rule_id`) REFERENCES `ais_indicator_scoring_rules` (`rule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_allowlist`
--

DROP TABLE IF EXISTS `ref_allowlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ref_allowlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(1028) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `allowlist_values` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=126448 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_ignorelist`
--

DROP TABLE IF EXISTS `ref_ignorelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ref_ignorelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(1028) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_ignorelist_cron`
--

DROP TABLE IF EXISTS `ref_ignorelist_cron`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ref_ignorelist_cron` (
  `last_ignorelist_value` varchar(1028) NOT NULL,
  PRIMARY KEY (`last_ignorelist_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_rules_status`
--

DROP TABLE IF EXISTS `ref_rules_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ref_rules_status` (
  `status` tinyint(1) NOT NULL,
  `meaning` varchar(20) NOT NULL,
  PRIMARY KEY (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-07 14:05:14
