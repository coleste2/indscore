-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: indscore
-- ------------------------------------------------------
-- Server version	5.7.30-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `ais_indicator_scoring_rules_workflow`
--

LOCK TABLES `ais_indicator_scoring_rules_workflow` WRITE;
/*!40000 ALTER TABLE `ais_indicator_scoring_rules_workflow` DISABLE KEYS */;
INSERT INTO `ais_indicator_scoring_rules_workflow` VALUES (0,1,'Rule created',59,0,'2020-12-14 19:39:19'),(1,1,'Testing',59,0,'2020-12-14 23:48:33'),(0,1,'Rule created',61,0,'2020-12-21 23:43:31'),(0,1,'Rule created',62,0,'2020-12-22 01:19:14'),(0,9,'Rule created',63,0,'2020-12-23 15:08:56'),(NULL,9,'wassup',63,1,'2020-12-23 15:09:19'),(NULL,9,'Hi',63,1,'2020-12-23 15:09:43'),(NULL,9,'Hello\n',63,1,'2020-12-23 15:10:23'),(NULL,9,'j\nljn',63,1,'2020-12-23 15:10:26'),(NULL,9,'lkn',63,1,'2020-12-23 15:10:42'),(NULL,9,'hh',63,1,'2020-12-23 15:12:30'),(0,9,'Rule created',65,0,'2020-12-24 14:44:00'),(NULL,9,'ef',65,1,'2020-12-24 14:51:23'),(NULL,9,'ef',65,1,'2020-12-24 14:51:24'),(1,9,'dd',61,0,'2020-12-24 15:12:52'),(1,9,'sg',65,0,'2020-12-24 15:29:29'),(0,9,'Rule created',66,0,'2021-03-29 17:25:14'),(0,9,'Rule created',67,0,'2021-03-29 17:25:14'),(0,9,'Rule created',68,0,'2021-04-15 15:59:25'),(0,9,'Rule created',69,0,'2021-04-22 17:37:54'),(0,9,'Rule created',70,0,'2021-04-22 17:37:54'),(1,9,'Test',70,0,'2021-04-22 17:38:10'),(0,9,'Rule created',71,0,'2021-04-22 18:01:12'),(0,9,'Rule created',72,0,'2021-04-22 18:01:12'),(1,9,'r',72,0,'2021-04-22 18:01:21'),(0,9,'Rule created',73,0,'2021-04-22 18:26:40'),(0,9,'Rule created',74,0,'2021-04-22 18:26:40'),(1,9,'dd',74,0,'2021-04-22 18:26:49'),(0,9,'Rule created',75,0,'2021-05-01 16:36:08'),(0,9,'Rule created',76,0,'2021-05-01 16:36:08'),(1,9,'For testing',76,0,'2021-05-01 16:36:21'),(1,9,'test',76,0,'2021-05-01 16:36:37'),(1,9,'s',75,0,'2021-05-01 16:41:49'),(1,9,'d',76,0,'2021-05-01 16:45:23'),(1,9,'f',75,0,'2021-05-01 16:45:51'),(1,9,'test',76,0,'2021-05-01 16:47:43'),(0,9,'Rule created',77,0,'2021-05-01 16:52:19'),(0,9,'Rule created',78,0,'2021-05-01 16:52:19'),(0,9,'Rule created',79,0,'2021-05-01 16:54:52'),(1,9,'ddd',79,0,'2021-05-01 16:55:31');
/*!40000 ALTER TABLE `ais_indicator_scoring_rules_workflow` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-07  8:55:34
