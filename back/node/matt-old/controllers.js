const axios = require("axios");
const https = require("https");
const config = require("./config")
const agent = new https.Agent({ rejectUnauthorized: false });
const api = require("./api")


/**
 *@
 * @route POST /mitre/api/actor
 * @description restructuring for layer format
 * @returns {Array} -converted Actor's data to layer format v2.2
 */

exports.specifiedEvidencePackages = async (req, res) => {
	try {
		var evidencePackages = req.body.data.split(/[ ,]+/).splice(0, 100);

		var data = []

		for (let index = 0; index < evidencePackages.length; index += 20) {
			await axios.all(evidencePackages.slice(index, Math.min(index + 20, evidencePackages.length)).map(id => axios.get(api.asaf({ id }).evidenceById, config.env.NODE_ENV === "development" ? undefined : { httpsAgent: agent }).then(request => {
				data.push(request.data)
				return null;
			}).catch(err => { console.log(err); return [] })))
		}
		//console.log("final length: " + data.length)
		res.send({ data })
	} catch (error) {
		console.log(error)
		res.send({ data: [] })
	}
}

exports.rapidRandomEvidencePackages = async (req, res) => {
	try {
		var evidencePackages = await axios.get(api.asaf({ page: Math.round(Math.random() * 4800000 / req.body.data), pageSize: req.body.data }).evidence, config.env.NODE_ENV === "development" ? undefined : { httpsAgent: agent }).then(request => {
			return request.data.results.splice(0, req.body.data).map(({ id, sourceTitle, sourceId }) => { return { id, sourceTitle, sourceId } })
		}).catch(err => { console.log(err); return { id: null, sourceTitle: null, sourceId: null } })

		for (let index = 0; index < req.body.data; index += 20) {
			//console.log("fetching eps " +index + " - " + (index+20) )
			await axios.all(evidencePackages.slice(index, Math.min(index + 20, req.body.data)).map(({ id, sourceTitle, sourceId }, iterIndex) => axios.get(api.asaf({ id, sourceTitle, sourceId, isRapid: true }).evidenceById, config.env.NODE_ENV === "development" ? undefined : { httpsAgent: agent }).then(request => {
				evidencePackages[iterIndex + index] = request.data
				return null
			}).catch(err => { console.log(err); return [] })))
		}
		//console.log("final length: " + evidencePackages.length)
		res.send({ data: evidencePackages })
	} catch (error) {
		console.log(error)
		res.send({ data: [] })
	}
}


exports.trueRandomEvidencePackages = async (req, res) => {
	//console.log("hit")
	try {
		var evidencePackages = [];
		for (let index = 0; index < req.body.data; index += 25) {
			//console.log("fetching ids " +index + " - " + (index+25) )
			await axios.all(sequence(Math.min(index + 25, req.body.data), index).map(attempt => axios.get(api.asaf({ page: Math.round(Math.random() * 4800000), pageSize: 1 }).evidence, config.env.NODE_ENV === "development" ? undefined : { httpsAgent: agent }).then(request => {
				evidencePackages.push(request.data.results[0])
				return null
			}).catch(err => { console.log(err); return [] })))
		}
		//console.log("final length of ids: " + evidencePackages.length)

		for (let index = 0; index < req.body.data; index += 20) {
			//console.log("fetching eps " +index + " - " + (index+20) )
			await axios.all(evidencePackages.slice(index, Math.min(index + 20, req.body.data)).map(({ id, sourceTitle, sourceId }, iterIndex) => axios.get(api.asaf({ id, sourceTitle, sourceId }).evidenceById, config.env.NODE_ENV === "development" ? undefined : { httpsAgent: agent }).then(request => {
				evidencePackages[iterIndex + index] = request.data;
				return null
			}).catch(err => { console.log(err); return [] })))
		}

		//console.log("final length: " + evidencePackages.length)
		res.send({ data: evidencePackages })
	} catch (error) {
		console.log(error)
		res.send({ data: [] })
	}
}

exports.sourceCount = async (req, res) => {
	//console.log("hit")
	try {
		var set = new Set();

		var numEvidencePackages = await axios.get(api.asaf({ id: req.params.id, page: 1, pageSize: 100 }).evidenceByInd, config.env.NODE_ENV === "development" ? undefined : { httpsAgent: agent }).then(({ data }) => {
			for (var i = 0; i < data.results.length; i++) {
				if (data.results[i].sourceTitle.indexOf("AIS-General") !== -1 || data.results[i].sourceTitle.indexOf("FEDGOV") !== -1) {
					data.results[i].sourceId = 100069;
				}
				set.add(data.results[i].sourceId)
			}
			return data.totalPages
		}).catch(err => { console.log(err); return 1 })

		//console.log(numEvidencePackages + " evidence packages needed")
		//console.log("Intitial Size: " + set.size)
		//console.log("Short Circuit: "+ (req.params.shortCircuit ? req.params.shortCircuit : "N/A"))
		//Throttle requests in increments of 25
		for (let index = 0; index <= numEvidencePackages; index += 25) {
			if (req.params.shortCircuit && parseInt(req.params.shortCircuit) <= set.size) {
				res.send({ data: set.size })
				return;
			}

			//console.log("	Fetching EPs " + index + " - " + (index+25))

			await axios.all(sequence(Math.min(index + 25, numEvidencePackages), index).map(attempt => axios.get(api.asaf({ id: req.params.id, page: attempt, pageSize: 100 }).evidenceByInd, config.env.NODE_ENV === "development" ? undefined : { httpsAgent: agent }).then(({ data }) => {
				for (var i = 0; i < data.results.length; i++) {
					if (data.results[i].sourceTitle.indexOf("AIS-General") !== -1 || data.results[i].sourceTitle.indexOf("FEDGOV") !== -1) {
						data.results[i].sourceId = 100069;
					}
					set.add(data.results[i].sourceId)
				}
				return data.totalPages
			}).catch(err => { console.log(err); return 1 })))
				;
		}

		res.send({ data: set.size })

	} catch (error) {
		console.log(error)
		res.send({ data: 0 })
	}
}

/**
 *
 * @param {number} page -pagination page.
 * @param {number} pageSize -pagination pageSize.
 * @param {number} totalPages - pagination totalPages.
 * @param {string} term - search term from the user.
 * @returns {Object} returns actor's searchTerm data from Illuminate API.
 */
const sequence = (num, start = 1) => {
	var arr = []
	for (var i = start; i < num; i++) {
		arr.push(i)
	}
	return arr
}
