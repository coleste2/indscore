const express = require("express");
const router = express.Router();
const controllers = require("./controllers");


router.route("/specifiedEvidencePackages").post(controllers.specifiedEvidencePackages);
router.route("/rapidRandomEvidencePackages").post(controllers.rapidRandomEvidencePackages);
router.route("/trueRandomEvidencePackages").post(controllers.trueRandomEvidencePackages);
router.route("/sourceCount/:id/:shortCircuit?").get(controllers.sourceCount);

router.route("/*").get((req, res)=>res.send("Indscore API is running!"));
module.exports = router;
