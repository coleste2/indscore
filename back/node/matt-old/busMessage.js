const fs = require("fs")
const path = require('path');
const { Connection } = require("rhea-promise");
const config = require("./config")

exports.handleBusMessage = (message) => {
    console.log("gotMessage:  ", message);
    // 			[opinionObjects] = send the evidence id to the php file that scores evidence packages
    //          append stix id in message to opinion objects
    //          [stix] = convert opinion object to stix with MPE
    //          send [stix] back to BUS
}

// exports.sendBusMessage = async (message) => {
//     const connection = new Connection({
//         host: config.amqp.host,
//         port: config.amqp.port,
//         username: config.amqp.username,
//         password: config.amqp.password,

//         transport: 'tls',
//         key: fs.readFileSync(path.resolve(__dirname, 'client-key.pem')),
//         cert: fs.readFileSync(path.resolve(__dirname, 'client-cert.pem')),
//         ca: [fs.readFileSync(path.resolve(__dirname, 'ca-cert.pem'))]
//     });

//     await connection.open();

//     const sender = await connection.createAwaitableSender({
//         name: config.amqp.senderName,
//         target: {
//             address: config.amqp.senderAddress
//         },
//         sendTimeoutInSeconds: 30
//     });

//     await sender.send({ body: message, message_id: Math.random(), header: "header" })
//     await sender.close();
//     await connection.close();
// }
