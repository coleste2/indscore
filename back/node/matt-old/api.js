const { NODE_ENV } = require("./config").env

exports.asaf = ({ page, id, sourceTitle, pageSize = 100, sourceId, isRapid}) => {
    if (NODE_ENV === "development") {
        return {
            evidenceByInd: `http://localhost:5000/api/1_0/indicator/${id}/evidence?page=${page}&pageSize=${pageSize}`,
            evidenceById: `http://localhost/api/scrapeIndicators.php?id=${id}${sourceTitle ? `&sourceTitle=${sourceTitle}` : ""}${sourceId ? `&sourceId=${sourceId}`:""}${isRapid ? `&isRapid=${true}`:""}`,
            evidence: `http://localhost:5000/api/1_0/evidence?page=${page}&pageSize=${pageSize}`,
        }
    }
    else {
        return {
            evidenceByInd:`https://sweb7-qa.bronze.us-cert.gov/illuminateapi/interface.php/getEvidencePackageFromIndicatorId?id=${id}&page=${page}&pageSize=${pageSize}`,
            evidenceById: `https://sweb7-qa.bronze.us-cert.gov/indscore/api/scrapeIndicators.php?id=${id}${sourceTitle ? `&sourceTitle=${sourceTitle}` : ""}${sourceId ? `&sourceId=${sourceId}`:""}${isRapid ? `&isRapid=${true}`:""}`,
            evidence: `https://sweb7-qa.bronze.us-cert.gov/illuminateapi/interface.php/getEvidencePackages?page=${page}&pageSize=${pageSize}`,
        }
    }
}