/**
 * NodeJS & ExpressJS Import
 */
const express = require("express");
const app = express();
const cors = require("cors");
const port = 4300;
const bodyParser = require("body-parser");
const container = require("rhea")
const config = require("./config")
const routes = require("./routes");
const { handleBusMessage } = require("./busMessage")
const fs = require("fs")
const path = require('path');

container.connect({
	host: config.amqp.host,
	port: config.amqp.port,
	username: config.amqp.username,
	password: config.amqp.password,

	transport: 'tls',
	key: fs.readFileSync(path.resolve(__dirname, 'client-key.pem')),
	cert: fs.readFileSync(path.resolve(__dirname, 'client-cert.pem')),
	ca: [fs.readFileSync(path.resolve(__dirname, 'ca-cert.pem'))],
}).open_receiver(config.amqp.receiverAddress).on("message", (m) => handleBusMessage(m));


app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(port, () => {
	console.log("----------------------------------------------------")
	console.log(`| ExpressJS app listening at http://localhost:${port} |`)
	console.log("---------------------------------------------------");
});

