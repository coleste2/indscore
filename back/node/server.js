// Configuration File
const config = require('./config.json')

// DSB Connector
const fs = require('fs');
const container = require('rhea');

// RESTful API
const express = require('express');
const path = require('path');
const { request } = require('http');
const app = express()

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/*********************** DSB Receiver ***********************/
let connection = null;

const initializeDsb = () => {
    connection = container.connect({
        host: config.amqp.host,
        port: config.amqp.port,
        username: config.amqp.username,
        password: config.amqp.password,

        transport: 'tls',
        key: fs.readFileSync(path.resolve(__dirname, config.amqp.key)),
        ca: [fs.readFileSync(path.resolve(__dirname, config.amqp.ca))],
        cert: fs.readFileSync(path.resolve(__dirname, config.amqp.cert))
    });

    const receiverUrl = "topic://" + config.amqp.receiverAddress;

    connection.open_receiver({
        source: {
            address: receiverUrl,
            durable: true
        },
        name: "AS&F-DSB-Node-Subscriber",
        autoaccept: false,
        autosettle: true
    });

    connection.on("accepted", (ctx) => {
        console.log("PUBLISHER --- Message accepted");
        ctx.delivery.update(true);
    });

    connection.on("released", (ctx) => {
        console.log("PUBLISHER --- Message released");
    });

    connection.on("rejected", (ctx) => {
        console.log("PUBLISHER --- Message rejected");
        console.log(ctx.delivery.remote_state.error);
    });

    connection.on("message", (m) => {
        console.log("RECEIVER --- Message received");
        if (m.message) {
            console.log(m.message.body);
        } else
            console.log(m);
        m.delivery.accept();
    })
}

initializeDsb();
/*********************** ROUTES ***********************/

// Get Status of Node Server w/o Sending Opinion
app.get('/status', (_, __) => {
    res.sendStatus(200);
})

// Send Opinion Object from Indicator Scoring
app.post('/publish-opinion', (req, res) => {
    console.log("API --- Request received!");

    if (connection.is_closed()) {
        console.log("Connection is closed. Restarting connection...");
        initializeDsb();
    }

    const connectionUrl = "topic://" + config.amqp.senderAddress;

    connection.open_sender({
        target: {
            address: connectionUrl,
            durable: true
        },
        name: "AS&F-DSB-Node-Publisher",
        source: connectionUrl,
        autosettle: false
    });

    connection.once("sendable", (ctx) => {
        // if (req.body.objects.length > 0) {
        //     req.body.objects[0].confidence = parseInt(req.body.objects[0].confidence);
        //     if (!req.body.objects[0].elevated)
        //         req.body.objects[0].elevated = "0";
        //     if (!req.body.objects[0].object_marking_refs)
        //         req.body.objects[0].object_marking_refs = [];
        // }

        const publishMessage = {
            application_properties: {
                origin: "AS&F",
                feed: "Dissemination",
                type: "STIX",
                version: "2.1",
                source: "Non-Federal",
                productType: "TBD",
                contentId: req.body.id,
                dataLength: Buffer.byteLength(JSON.stringify(req.body)),
            },
            body: JSON.stringify(req.body)
        };

        console.log("--- PUBLISHED MESSAGE ---");
        console.log(JSON.stringify(publishMessage, null, 4));

        ctx.sender.send(publishMessage);

        console.log("API --- Message sent");
    });

    res.sendStatus(200)
})

app.listen(config.express.port, () => console.log(`DSB Node Server Listening on Port ${config.express.port}`));

// Close Connection Gracefully so Messages get Resolved Properly
process.on("SIGINT", () => {
    console.log("Gracefully shutting down from SIGINT (Ctrl-C)");
    connection.close();

    process.exit(1);
})