// Configuration File
const config = require('/opt/indscorebackend/redesign.json')

// DSB Connector
const fs = require('fs');
const container = require('rhea');

// RESTful API
const express = require('express');
const path = require('path');
const app = express();

// HTTP Request
const https = require('https');
const request = require('request');

// MySQL Connector
const mysql = require('mysql');

// Bunyan Logger
const bunyan = require('bunyan');
const infoLogger = bunyan.createLogger({
    name: 'Ignore-List-Checker',
    streams: [{
        level: 'info',
        type: 'rotating-file',
        path: config.logger.infoFileLocation,
        period: '1d',
        count: 5
    }],
    serializers: { req: bunyan.stdSerializers.req }
});
const errLogger = bunyan.createLogger({
    name: 'Ignore-List-Checker',
    streams: [{
        type: 'rotating-file',
        path: config.logger.errorFileLocation,
        period: '1d',
        count: 10
    }],
    serializers: { req: bunyan.stdSerializers.req }
});
const httpLogger = bunyan.createLogger({
    name: 'Ignore-List-Checker',
    streams: [{
        type: 'rotating-file',
        path: config.logger.httpFileLocation,
        period: '1d',
        count: 10
    }],
    serializers: { req: bunyan.stdSerializers.req }
})

// CRON
const CronJob = require('cron').CronJob;
const { exit } = require('process');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/*********************** DSB Receiver ***********************/
const conn = mysql.createConnection({
    host: config.mysql.host,
    user: config.mysql.username,
    password: config.mysql.password,
    database: config.mysql.database
});
let connection = null; // DSB Container
let ignoreListIndicators = [];

const checkForDuplicate = async (indicatorUuid) => {
    return new Promise((resolve, _) => {
        infoLogger.debug(`Checking for duplicate UUID ${indicatorUuid}`);
        console.log(`Checking for duplicate UUID ${indicatorUuid}`);

        conn.query(config.mysql.checkForDuplicateQuery, [indicatorUuid], (err, result) => {
            if (err) {
                console.error("There was an error executing the duplicate query");
                console.error(err);
                throw err;
            }

            if (result.length > 0) {
                console.log(`Duplicate Indicator UUID found ${indicatorUuid}`);
                resolve(true);
            } else 
                console.log(`${indicatorUuid} is not a duplicate.`);

            resolve(false);
        });
    });
}

const connectToDatabase = () => {
    return new Promise((resolve, _) => {
        conn.connect((err) => {
            if (err) {
                console.error("There was an error connecting to the database");
                console.error(err);
                throw err;
            }

            console.log("Connected");
            resolve();
        })
    })
}

const initializeDsb = () => {
    infoLogger.info("Initialize DSB started");
    console.info("Started");
    connection = container.connect({
        host: config.amqp.host,
        port: config.amqp.port,
        username: config.amqp.username,
        password: config.amqp.password,

        transport: 'tls',
        key: fs.readFileSync(path.resolve(__dirname, config.amqp.key)),
        ca: [fs.readFileSync(path.resolve(__dirname, config.amqp.ca))],
        cert: fs.readFileSync(path.resolve(__dirname, config.amqp.cert))
    });

    const receiverUrl = "topic://" + config.amqp.receiverAddress;

    connection.open_receiver({
        source: {
            address: receiverUrl,
            durable: true
        },
        name: "AS&F-Ignorelist-Testing",
        autoaccept: false,
        autosettle: true
    });

    connection.on("message", async (m) => {
        if (m.message && m.message.body) {
            console.log("NEW MESSAGE");
            try {
                const parsedStixMessage = JSON.parse(m.message.body);
                let indicatorId = null;
                let indicator = null;
                let indicatorSTIXBenignList = null;
                let indicatorFound = false;
                let isDuplicate = false;

                for (let i = 0; i < parsedStixMessage.objects.length; i++) {
                    if (parsedStixMessage.objects[i].type === "indicator") {
                        const indicatorSTIXMessage = parsedStixMessage.objects[i].pattern;
                        indicatorSTIXBenignList = parsedStixMessage.objects[i].indicator_types;
                        let parsedIndicatorMessage = indicatorSTIXMessage.substr(indicatorSTIXMessage.indexOf("'") + 1);
                        parsedIndicatorMessage = parsedIndicatorMessage.substr(0, parsedIndicatorMessage.indexOf("]") - 1);

                        infoLogger.info("Indicator Extracted: ", parsedIndicatorMessage);
                        console.log(parsedIndicatorMessage);
                        indicator = parsedIndicatorMessage;
                        indicatorId = parsedStixMessage.objects[i].id;
                        indicatorFound = true;
                        isDuplicate = await checkForDuplicate(parsedStixMessage.objects[i].id);
                    }
                }

                if (indicatorFound || isDuplicate) {
                    if (ignoreListIndicators.includes(indicator)) {
                        // CHECK IGNORE LIST AND THEN SCORE
                        infoLogger.info("RECEIVER --- Message received --- ", Date.now());
                        infoLogger.info(`Found indicator on the ignore list. Scoring ${indicator}`);
                        scoreIndicator(indicator, indicatorId, indicatorSTIXBenignList.includes("benign"), false);
                    } else if (isDuplicate) {
                        infoLogger.info("RECEIVER --- Message received --- ", Date.now());
                        infoLogger.info(`Found duplicate Indicator UUID ${indicatorId}. Scoring ${indicator}`);
                        console.log(`Found duplicate Indicator UUID ${indicatorId}. Scoring ${indicator}`);
                        // Sending in dummy data for the other variables because getIndicators.php doesn't use them; only the Indicator UUID is used
                        // This prevents an error on the request.post() due to null body parameters
                        scoreIndicator('duplicate', indicatorId, false, isDuplicate);
                    } else {
                        infoLogger.info("RECEIVER --- Message received --- ", Date.now());
                        infoLogger.info(`Indicator ${indicator} not found in ignore list. Skipping...`);
                    }
                }
            } catch (err) {
                console.warn(err);
                infoLogger.info("RECEIVER --- Message received --- ", Date.now());
                errLogger.warn("Error parsing STIX message. See received message below.");
                errLogger.warn(m.message);
            }
        } else {
            infoLogger.info("RECEIVER --- Message received --- ", Date.now());
            errLogger.warn("Message or message body not present in message received");
            errLogger.warn(m);
        }
        m.delivery.accept();
    })
}

const refreshIgnoreList = () => {
    ignoreListIndicators = [];

    return new Promise((resolve, _) => {
        infoLogger.info("Ignore List CRON - Starting");

        // Retrieve Ignore List
        conn.query(config.mysql.getIgnoreListQuery, (err, result) => {
            if (err) {
                errLogger.fatal("There was an error executing ignore list query!");
                errLogger.fatal(err);
                throw err;
            }

            result.forEach(row => ignoreListIndicators.push(row.value));

            infoLogger.info(ignoreListIndicators.length);
            infoLogger.info("Successfully refreshed ignore list");
            resolve();
        })
    });
}

const scoreIndicator = (indicator, indicatorUuid, isBenign, isDuplicate) => {
    infoLogger.info(`Indicator ${indicatorUuid} | Benign ${isBenign} received | Duplicate ${isDuplicate} received. Sending to AS&F for scoring`);
    console.log(`Indicator ${indicatorUuid} | Benign ${isBenign} received | Duplicate ${isDuplicate} received. Sending to AS&F for scoring`);

    return new Promise((resolve, reject) => {
        request.post({
            url: `https://${config.getIndicators.hostname}:${config.getIndicators.port}${config.getIndicators.path}`,
            formData: {
                indicator: indicator,
                indicator_uuid: indicatorUuid,
                is_benign: isBenign ? "true" : "false",
                is_duplicate: isDuplicate ? "true" : "false",
                request_type: isDuplicate ? config.getIndicators.duplicateRequestType : config.getIndicators.ignoreListRequestType
            }
        }, (err, response, _) => {
            if (err) {
                console.error(err);
                errLogger.error(err);
                reject();
            } else {
                infoLogger.info("Successfully scored indicator - opinion sent off to DSB");
                console.log("Successfully scored indicator - opinion sent off to DSB");
                infoLogger.info(response.body);
                console.log(response.body);
                resolve();
            }
        });
    });
}

connectToDatabase().then(() => {
    console.log("Successfully connected to the MySQL Database");
    infoLogger.info("Successfully connected to the MySQL Database");

    refreshIgnoreList().then(initializeDsb).catch((err) => {
        console.error("Error retrieving initial ignore list with error ", err);
        errLogger.fatal(err);
    });
});

try {
    const job = new CronJob('0 */5 * * * *', refreshIgnoreList);
    job.start();
} catch (e) {
    console.error(e);
}

/*********************** ROUTES ***********************/
// Get Status of Node Server w/o Sending Opinion
app.get('/status', (req, res) => {
    httpLogger.info({ req: req }, "Status endpoint reached");
    res.sendStatus(200);
})

app.listen(config.express.port, () => infoLogger.info(`AS&F Ignorelist Listener on Port ${config.express.port}`));

// Close Connection Gracefully so Messages get Resolved Properly
process.on("SIGINT", () => {
    infoLogger.info("Gracefully shutting down from SIGINT (Ctrl-C)");
    connection.close();

    process.exit(1);
})
