<?php

//This is an API endpoint to fetch and score new Indicators and add them to the DB

//Import env variables and DB, and connect the connect to db
require "/opt/indscore/api/config.php";
require "/opt/indscore/api/UUID.php";
$env = new env();
$db = $env->connectDB();

$OPINION_STATUS_CODES = new stdClass();
$OPINION_STATUS_CODES->OPINION_SUBMISSION_SUCCESS = 1;
$OPINION_STATUS_CODES->OPINION_SUBMISSION_ERROR = -1;
// $OPINION_STATUS_CODES->ACS_MARKINGS_FAILURE = -2;
// $OPINION_STATUS_CODES->STIX_TREE_FAILURE = -3;
// $OPINION_STATUS_CODES->RELATIONSHIPS_OUTGOING_FAILURE = -4;

if (isset($_POST['request_type'])) {
    switch ($_POST['request_type']) {
        case 'ignorelist':
            if (isset($_POST['indicator']) && isset($_POST['is_benign']) && isset($_POST['indicator_uuid'])) {
                echo "Indicator received '" . $_POST['indicator'] . "'\n";
                echo "Indicator Benign Status: " . $_POST['is_benign'] . "\n";

                // $activeRule = getActiveRule($db);

                $receivedIndicator = new stdClass();
                $receivedIndicator->id = null;
                $receivedIndicator->value = new stdClass();
                $receivedIndicator->value->name = $_POST['indicator'];
                $receivedIndicator->benign = new stdClass();
                $receivedIndicator->benign->value = $_POST['is_benign'];
                $receivedIndicator->sources = 0;
                $receivedIndicator->totalMentions = 0;
                $receivedIndicator->type = 'ignorelist'; // This may change - can't reliably extract type of STIX message currently
                $receivedIndicator->stixObjects = [];
                $receivedIndicator->hitCount = 0;
                $receivedIndicator->received = false;

                // Score Ignorelist Indicator
                $receivedIndicator->score = score($receivedIndicator, $db, $env);

                // Generate Opinion Object
                $opinionObject = generateIgnorelistOpinionObject($receivedIndicator, $_POST['indicator_uuid'], $db);

                // Submit Opinion Object
                processOpinionObjects([$opinionObject], $db, $env, $OPINION_STATUS_CODES);

                echo "END >>> Processing complete\n";
            } else
                echo "Request for ignorelist indicators must provide indicator, is_benign, and indicator_uuid!\n END >>> Processing complete\n";

            break;

        case 'duplicate':
            if ($_POST['indicator_uuid']) {
                $indicatorUuid = $_POST['indicator_uuid'];

                echo "Duplicate indicator received with Indicator UUID: {$indicatorUuid}\n";
                $analystOneId = lookupAnalyst1IdFromIndicatorUuid($indicatorUuid, $db);

                if ($analystOneId == 0)
                    die("ERROR >>> No Analyst1 ID was found for Indicator UUID: {$indicatorUuid}\n");
                else if ($analystOneId == -1)
                    die("ERROR >>> There was an error looking up corresponding Analyst1 ID for Indicator UUID: {$indicatorUuid}. Exiting...\n");

                echo "Analyst1 ID Found: {$analystOneId}\n";

                $indicator = new stdClass();
                $indicator->id = $analystOneId;

                $scoredIndicators = scoreIndicators([$indicator], $db, $env);

                if (count($scoredIndicators) > 1)
                    die("ERROR >>> Multiple scored indicators received when only one duplicate Indicator UUID was provided. Exiting...\n");


                echo "Removing other Indicator UUIDs from scored indicators object...\n";
                for ($i = 0; $i < count($scoredIndicators[0]->stixObjects); $i++) {
                    if ($scoredIndicators[0]->stixObjects[$i]->id === $indicatorUuid) {
                        echo "Duplicate Indicator UUID found in Stix Objects array. Removing others...\n";
                        $scoredIndicators[0]->stixObjects = [$scoredIndicators[0]->stixObjects[$i]];
                        break;
                    }
                }

                // Generate Opinion Objects
                $opinionObjects = generateOpinionObjectsForScoredIndicators($scoredIndicators, $db, $env);

                // Submit Opinion Objects
                processOpinionObjects($opinionObjects, $db, $env, $OPINION_STATUS_CODES);

                echo "END >>> Processing complete\n";
            } else
                echo "Request for duplicate indicator must provide indicator_uuid!\n END >>> Processing complete\n";

            break;

        default:
            echo "Request type in post body detected but value was not recognized. Supported request types are ['duplicate', 'ignorelist'].\n";
            echo "END >>> Processing complete\n";
            break;
    }
} else {
    // Grab Last Sensor Version from Database
    $lastVersionNumber = grabLastSensorVersion($db);
    if ($lastVersionNumber == -1)
        cleanupAndClose(false, null, null);

    // Auto-Mitigate All Pending Indicator Taskings
    $autoMitigationResult = file_get_contents($env->newApi("auto-mitigate-taskings", "", "", ""), false, $env->getPostContext());
    if ($autoMitigationResult === false) {
        echo "ERROR >>> Occurred while automitigating taskings. Check php log for additional information.\n";
        cleanupAndClose(false, null, null);
    }

    // Wait Until Processing Is Finished
    $isProcessing = true;
    while ($isProcessing) {
        $isProcessingCheck = file_get_contents($env->newApi("auto-mitigate-progress", "", "", ""), false, $env->getContext());
        if ($isProcessingCheck === FALSE) {
            $isProcessing = false;
            echo "ERROR >>> Checking for mitigation progress. Check php log for additional information.\n";
            cleanupAndClose(false, null, null);
        }

        if ($isProcessingCheck === "false") {
            echo "Processing config completed. Pulling taskings...\n";
            $isProcessing = false;
        } else {
            echo "Still processing\n";
            sleep(1); // Wait 1 second, then check again
        }
    }

    // Get Active Rule
    // Commented out since custom rule is in effect (see below - scoreWithNewRule())
    // $activeRule = getActiveRule($db);

    $indicatorsAdded = null;   // Indicators to be scored - indicators added to the sensor
    $indicatorsRemoved = null; // Indicators to be removed - indicators removed from the sensor

    $lastVersionNumber = null;
    if ($lastVersionNumber) {
        // Find diff between last processed version and newest version
        echo "Previous version detected. Last version processed was {$lastVersionNumber}\n";

        // Grab Diff Taskings
        $taskings = grabDiffTaskings($env, $lastVersionNumber);

        if ($taskings->version === $taskings->latestVersion) {
            echo "Version and Latest Version are the same. Nothing new to process. Exiting...\n";
            return;
        } else {
            echo "\n--- NEW VERSION DETECTED ---\n";
            echo "Old version -> " . $taskings->version . "\n";
            echo "New version -> " . $taskings->latestVersion . "\n\n";
        }

        $latestVersion = $taskings->latestVersion;

        $indicatorsAdded = $taskings->indicatorsAdded;
        $indicatorsRemoved = $taskings->indicatorsRemoved;
    } else {
        // (First Run) Grab all indicators
        echo "No previous version detected. Grabbing all taskings for most recent sensor version\n";

        // Grab All Taskings
        $taskings = grabTaskings($env);

        $latestVersion = $taskings->version;
        $indicatorsAdded = $taskings->indicators;
        // Score Indicators (Used for Debugging ONLY)
        // $arr = [];
        // for ($d = 0; $d < count($taskings->indicators); $d++) {
        //     // echo $taskings->indicators[$d]->id . "\n";
        //     if ($taskings->indicators[$d]->id > 3609653) {
        //         array_push($arr, $taskings->indicators[$d]);
        //     }
        // }
        // print_r($arr);
        // $indicatorsAdded = $arr;
        $testingIndicator = new stdClass();
        $testingIndicator->id = '10260684';
        $arr = [];
        array_push($arr, $testingIndicator);
        $indicatorsAdded = $arr;
    }

    if (is_int($taskings) && $taskings == -1) {
        echo "ERROR >>> Attempted to grab taskings unsuccessfully\n";
        cleanupAndClose(false, null, null);
    }

    // Score Indicators
    $scoredIndicators = scoreIndicators($indicatorsAdded, $db, $env);

    // Generate Opinion Objects
    $opinionObjects = generateOpinionObjectsForScoredIndicators($scoredIndicators, $db, $env);

    // Submit Opinion Objects
    processOpinionObjects($opinionObjects, $db, $env, $OPINION_STATUS_CODES);

    // Save Progress
    saveNewVersion(null, $latestVersion, $db);

    echo "END >>> Processing complete\n";
}

function checkIfScorePreviously(&$db, $indicatorUuid)
{
    $sql = "select indicator_uuid from ais_indicator_scoring where indicator_uuid = '{$indicatorUuid}'";

    try {
        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $err) {
        echo "ERROR >>> Couldn't check for duplicate. Indicator UUID: " . $indicatorUuid . " - with error message " . $err->getMessage() . "\n";
    }

    return count($result) > 0;
}

function cleanupAndClose($isSuccessful, $lastVersionNumber, $latestVersion)
{
    if ($isSuccessful) {
        saveNewVersion($lastVersionNumber, $latestVersion, $db);
        die("Scoring successfully completed. Saving new version " . $latestVersion . "\n");
    } else {
        die("ERROR >>> Scoring was unsuccessful. Not saving new version...\n");
    }
}

function generateIgnorelistOpinionObject(&$indicator, $indicatorUuid, &$db)
{
    // We want to save the score even if the opinion object generation was unsuccessful
    $submissionId = saveIndicatorScore($indicator->id, $indicatorUuid, $indicator, $indicator->score, $db);
    if ($submissionId == -1) {
        echo "    ERROR >>> Attempted to save indicator score\n";
        cleanupAndClose(false, null, null);
    }

    // Save the Submission ID as a part of the indicator so we can update the opinion status later
    $indicator->submissionId = $submissionId;

    date_default_timezone_set("UTC");
    $dateFields = date("Y-m-d\TH:i:s.000\Z", time());

    // Stores Full Object (ACS Markings Definition Object + Opinion Object)
    $opinionObject = [];

    // Opinion Object
    $opinionObjectScoring = new stdClass();
    $opinionObjectScoring->type = "opinion";
    $opinionObjectScoring->spec_version = "2.1";
    $opinionObjectScoring->id = "opinion--" . UUID::v4();
    $opinionObjectScoring->created_by_ref = "identity--b3bca3c2-1f3d-4b54-b44f-dac42c3a8f01";
    $opinionObjectScoring->created = $dateFields;
    $opinionObjectScoring->modified = $dateFields;
    //$opinionObjectScoring->object_marking_refs = [];
    // $opinionObjectScoring->authors = ["AS&F Rule 1000"]; // ASK MARLON in next meeting
    $opinionObjectScoring->opinion = strtolower($indicator->score->opinion);
    // $opinionObjectScoring->confidence = intval($indicator->score->score);
    $opinionObjectScoring->explanation = $indicator->score->reason;
    $opinionObjectScoring->object_refs = [$indicatorUuid];
    // $opinionObjectScoring->tlp = $indicator->tlp;
    $opinionObjectScoring->submissionId = $indicator->submissionId;

    array_push($opinionObject, $opinionObjectScoring); // Push Opinion Object
    // array_push($opinionObject, $acsMarkingDefinition); // Push ACS Markings Definition Object

    return $opinionObject;
}

// Returns the "objects" portion for the opinion bundle
// Opinion Object (used to be + ACS Markings Definitions)
function generateOpinionObject($indicator, $indicatorScore, $indicatorUuid, $indicatorReportingSourceId, $env)
{
    // Checking for Elevated Flag
    $indicatorStixObject = file_get_contents($env->newApi("object-stix", '', $indicatorReportingSourceId, $indicatorUuid), false, $env->getContext());
    $indicatorStixObject = json_decode($indicatorStixObject);

    // $isElevated = false;
    // if (isset($indicatorStixObject) && isset($indicatorStixObject->source->labels)) {
    //     if (in_array("elevated", $indicatorStixObject->source->labels)) {
    //         $isElevated = true;
    //         echo "    Elevated flag detected. Flag will be present on opinion object.\n";
    //     } else
    //         echo "    No elevated flag was detected.\n";
    // }

    date_default_timezone_set("UTC");
    $dateFields = date("Y-m-d\TH:i:s.000\Z", time());

    // Stores Full Object (Opinion Object)
    // Used to store ACS and Opinion
    $opinionObject = [];

    // Opinion Object
    $opinionObjectScoring = new stdClass();
    $opinionObjectScoring->type = "opinion";
    $opinionObjectScoring->spec_version = "2.1";
    $opinionObjectScoring->id = "opinion--" . UUID::v4();
    $opinionObjectScoring->created_by_ref = "identity--b3bca3c2-1f3d-4b54-b44f-dac42c3a8f01";
    $opinionObjectScoring->created = $dateFields;
    $opinionObjectScoring->modified = $dateFields;
    //$opinionObjectScoring->object_marking_refs = [];
    // $opinionObjectScoring->authors = ["AS&F Rule 1000"]; // ASK MARLON in next meeting
    $opinionObjectScoring->opinion = strtolower($indicatorScore->opinion);
    // $opinionObjectScoring->confidence = intval($indicatorScore->score);
    $opinionObjectScoring->explanation = $indicatorScore->reason;
    $opinionObjectScoring->object_refs = [$indicatorUuid];
    $opinionObjectScoring->submissionId = $indicator->submissionId; // Storing here temporarily until right before opinion submission

    array_push($opinionObject, $opinionObjectScoring);          // Push Opinion Object

    // if ($isElevated)
    //     $opinionObjectScoring->elevated = true;
    // else
    //     $opinionObjectScoring->elevated = false;
    return $opinionObject;
}

function generateOpinionObjectsForScoredIndicators(&$scoredIndicators, &$db, $env)
{
    $opinionObjects = [];

    echo "--- GENERATING OPINION OBJECTS ---\n";
    echo "Generating opinion objects for " . count($scoredIndicators) . "\n";

    for ($i = 0; $i < count($scoredIndicators); $i++) {
        $scoredIndicator = $scoredIndicators[$i];

        for ($j = 0; $j < count($scoredIndicator->stixObjects); $j++) {
            echo "----------------\n";
            echo "    Saving score for indicator UUID {$scoredIndicator->stixObjects[$j]->id}\n";

            // We are currently scoring duplicates. IF logic on duplicates changes, use this to detect if we've
            //   scored this indicator UUID previously.

            // $duplicateCheck = checkIfScorePreviously($db, $scoredIndicator->stixObjects[$j]->id);
            // if (is_int($duplicateCheck) && $duplicateCheck > 0) {
            //     echo "    Duplicate indicator UUID detected: {$scoredIndicator->stixObjects[$j]->id}. Skipping \n";
            // }

            // Save new indicator score in the database prior to generating opinion object.
            // We want to save the score even if the opinion object generation was unsuccessful
            $submissionId = saveIndicatorScore($scoredIndicator->id, $scoredIndicator->stixObjects[$j]->id, $scoredIndicator, $scoredIndicator->score, $db);
            if ($submissionId == -1) {
                echo "    ERROR >>> Attempted to save indicator score\n";
                cleanupAndClose(false, null, null);
            }

            // Save the Submission ID as a part of the indicator so we can update the opinion status later
            $scoredIndicator->submissionId = $submissionId;

            echo "    Generating opinion object for indicator UUID {$scoredIndicator->stixObjects[$j]->id} || Reporting Source ID {$scoredIndicator->stixObjects[$j]->reportingSourceId} || Analyst 1 ID {$scoredIndicator->id}\n";

            $generatedOpinionObject = generateOpinionObject($scoredIndicator, $scoredIndicator->score, $scoredIndicator->stixObjects[$j]->id, $scoredIndicator->stixObjects[$j]->reportingSourceId, $env);
            if (is_int($generatedOpinionObject) && $generatedOpinionObject == -1)
                echo "    ERROR >>> Couldn't generate opinion object for indicator UUID {$scoredIndicator->stixObjects[$j]->id}, reporting source ID {$scoredIndicator->stixObjects[$j]->reportingSourceId}, Analyst1 ID {$scoredIndicator->id}. Skipping opinion object generation...\n";
            else {
                array_push($opinionObjects, $generatedOpinionObject);
                echo "    SUCCESS >>> Opinion object generated successfully!\n";
            }

            echo "----------------\n";
        }
    }

    return $opinionObjects;
}

function getActiveRule(&$db)
{
    try {
        $sql = "SELECT rule, rule_id FROM ais_indicator_scoring_rules WHERE status = 1 ORDER BY rule_id DESC LIMIT 1";
        $stmt = $db->query($sql);
        $activeRule = $stmt->fetchAll(PDO::FETCH_OBJ);
        if (count($activeRule) === 0) {
            die("ERROR >>> No active rule");
        }
        $activeRule = $activeRule[0];
        $activeRule->code = unserialize($activeRule->rule);
        $activeRule->code = $activeRule->code->code;

        return $activeRule;
    } catch (PDOException $err) {
        die("ERROR >>> Pulling active rule with message " . $err->getMessage() . "\n");
    }
}

function grabDiffTaskings($env, $lastVersionNumber)
{
    try {
        $taskings = file_get_contents($env->newApi("diff-version-taskings", $lastVersionNumber, "", ""), false, $env->getContext());
        if ($taskings === false) {
            echo "ERROR >>> Getting diff taskings for sensor. The previous version processed was {$lastVersionNumber}\n";
            return -1;
        } else {
            $taskings = json_decode($taskings);

            return $taskings;
        }
    } catch (PDOException $err) {
        echo "ERROR >>> {$err->getMessage()}\n";
        return -1;
    }
}

function grabLastSensorVersion(&$db)
{
    try {
        $sql = "SELECT last_version_id FROM ais_indicator_scoring_cron ORDER BY last_version_id DESC LIMIT 1";
        $stmt = $db->query($sql);
        $lastVersionId = $stmt->fetchAll(PDO::FETCH_OBJ);

        if (count($lastVersionId) > 0) {
            echo "Last Run Version: " . $lastVersionId[0]->last_version_id . "\n";
            return $lastVersionId[0]->last_version_id;
        } else {
            return null;
        }
    } catch (PDOException $err) {
        echo "ERROR >>> {$err->getMessage()}\n";
        return -1;
    }
}

function grabNewestTaskings($env, $lastVersionNumber)
{
    // Last Version ID was found - find diff
    $taskings = file_get_contents($env->newApi("diff-version-taskings", $lastVersionNumber, "", ""), false, $env->getContext());
    if ($taskings === FALSE) {
        echo "ERROR >>> Getting taskings for sensor for diff version " . $lastVersionNumber . "\n";
        return -1;
    }

    $taskings = json_decode($taskings);

    if ($taskings->version === $taskings->latestVersion) {
        echo "Version and Latest Version are the same. Nothing new to process. Exiting...\n";
        return;
    } else {
        echo "\n\n--- NEW VERSION DETECTED ---\n\n";
        echo "Old version -> " . $taskings->version . "\n";
        echo "New version -> " . $taskings->latestVersion . "\n\n";
    }

    return $taskings;
}

function grabTaskings($env)
{
    try {
        $taskings = file_get_contents($env->newApi("current-version-taskings", "", "", ""), false, $env->getContext());
        if ($taskings === false) {
            echo "ERROR >>> Getting taskings for sensor. This is the first run for this sensor (no previous version was found).\n";
            return -1;
        } else {
            $taskings = json_decode($taskings);

            // Score Indicators (Used for Debugging ONLY)
            // $arr = [];
            // for ($d=0; $d < count($taskings->indicators); $d++) { 
            //     echo $taskings->indicators[$d]->id . "\n";
            //     if ($taskings->indicators[$d]->id === <PUT SPECIFIC INDICATOR ID HERE>) {
            //         array_push($arr, $taskings->indicators[$d]);
            //         break;
            //     }
            // }

            return $taskings;
        }
    } catch (PDOException $err) {
        echo "ERROR >>> {$err->getMessage()}\n";
        return -1;
    }
}

function listContains(&$db, $env, $value, $list)
{
    $sql = "SELECT id from ref_" . addslashes($list) . " where value = '" . addslashes($value) . "' LIMIT 1";
    try {
        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $err) {
        echo "ERROR >>> Couldn't execute list contains with message {$err->getMessage()}\n";
        if (strpos($err->getMessage(), 'SQLSTATE[HY000]: General error: 2006') !== false) {
            echo ("     Timeout error...trying to save\n");
            try {
                $db = null;
                $db = $env->connectDB();
                $stmt = $db->query($sql);
                $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (PDOException $err) {
                echo "     Timout save failed\n";
                echo "ERROR >>> Couldn't reconnect to the database with message {$err->getMessage()} \n";
            }
        }
    }
    return count($result) > 0;
}

function lookupAnalyst1IdFromIndicatorUuid($indicatorUuid, &$db)
{
    echo "Looking up Analyst1 ID for Indicator UUID {$indicatorUuid}\n";
    $sql = "SELECT indicator_id FROM ais_indicator_scoring WHERE indicator_uuid = '" . addslashes($indicatorUuid) . "' ORDER BY datetime_of_score DESC LIMIT 1";
    try {
        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        if (count($result) > 0)
            return $result[0]->indicator_id;

        return 0;
    } catch (PDOException $err) {
        echo "ERROR >>> There was an error attempting to find the Analyst1 ID for Indicator UUID {$indicatorUuid}\n";
        return -1;
    }
}

function processOpinionObjects($opinionObjects, &$db, $env, $OPINION_STATUS_CODES)
{
    echo "--- PROCESSING OPINION OBJECTS ---\n";
    echo "Processing opinion objects for " . count($opinionObjects) . "\n";

    for ($i = 0; $i < count($opinionObjects); $i++) {
        echo "----------------\n";

        $opinionObject = null;

        for ($j = 0; $j < count($opinionObjects[$i]); $j++) {
            // Grab the Opinion Object out of the objects array
            // Needed for the Opinion UUID and Indicator UUID
            echo "\n";
            if ($opinionObjects[$i][$j]->type === "opinion") {
                $opinionObject = $opinionObjects[$i][$j];
                break;
            }
        }

        if ($opinionObject) {
            echo "    Submitting opinion object for Opinion UUID {$opinionObject->id} and Indicator UUID {$opinionObject->object_refs[0]}\n";
            $submissionId = $opinionObject->submissionId;
            $result = submitOpinionObject($opinionObjects[$i], $opinionObject->id, $opinionObject->object_refs[0], $env);

            if (is_int($result) && $result == -1) {
                echo "    There was an error submitting opinion object for Opinion UUID {$opinionObject->id} || Indicator UUID {$opinionObject->object_refs[0]} || Submission ID {$submissionId}\n";
                updateOpinionObjectStatus($submissionId, $db, $OPINION_STATUS_CODES->OPINION_SUBMISSION_ERROR);
            } else {
                echo "    SUCCESS >>> DSB Publication Return: " . $result . "\n";
                echo "    SUCCESS >>> Opinion object submitted successfully for Opinion UUID {$opinionObject->id} || Indicator UUID {$opinionObject->object_refs[0]} || Submission ID {$submissionId}\n";
                updateOpinionObjectStatus($submissionId, $db, $OPINION_STATUS_CODES->OPINION_SUBMISSION_SUCCESS);
            }
        } else {
            echo "    ERROR >>> Couldn't grab opinion object out of objects array\n";
            print_r($opinionObjects[$i]);
            echo "\n";
        }
        echo "----------------\n";
    }
}

function saveIndicatorScore($analystOneId, $indicatorUuid, $indicator, $indicatorScore, &$db)
{
    if (isset($analystOneId))
        $indicatorId = $analystOneId;
    else
        $indicatorId = "NULL";

    $shortenedExplanation = addslashes(substr($indicatorScore->reason, 0, 100)) . "...";

    // Using rule 76 ID
    // Needs to change but doing this to avoid foreign key constraint
    $sql = "INSERT INTO ais_indicator_scoring (
            rule_id, 
            indicator_id, 
            indicator_uuid, 
            datetime_of_score, 
            score_value, 
            score_name, 
            score_explanation, 
            opinion_value, 
            opinion_submission_status, 
            indicator_value, 
            indicator_type
        ) VALUES (
            5, 
            {$indicatorId}, 
            '{$indicatorUuid}', " .
        "'" . gmdate('Y-m-d H:i:s') . "', " .
        "{$indicatorScore->score}, " .
        "'{$indicatorScore->response}', " .
        "'{$shortenedExplanation}', " .
        "'{$indicatorScore->opinion}', " .
        "0, " .
        "'" . addslashes($indicator->value->name) . "', " .
        "'" . addslashes($indicator->type) . "'" .
        ")";

    // Run the insert query for data
    try {
        $db->query($sql);
    } catch (PDOException $err) {
        echo $sql . "\n";
        echo "    ERROR >>> Inserting scoring data into the database for indicator {$indicatorUuid} with error " . $err->getMessage() . "\n";
        return -1;
    }

    echo "    Saved score for indicator {$indicatorUuid} successfully. Attempting to save scoring content...\n";

    // Generate JSON Content for Archival
    $jsonContent = new stdClass();
    $jsonContent->indicator = $indicator;
    $jsonContent->indicatorMentions = new stdClass();
    $jsonContent->indicatorMentions->sources = $indicator->sources;
    $jsonContent->indicatorMentions->totalMentions = $indicator->reportCount;

    //Serialize the class so that it can be stored in the DB
    $jsonContent = json_encode($jsonContent);
    $jsonContent = addslashes($jsonContent);

    try {
        $stmt = $db->query("SELECT last_insert_id() as id");
        $subm = $stmt->fetchAll(PDO::FETCH_OBJ);
        $subm = $subm[0];
        $stmt = $db->query("INSERT INTO ais_indicator_scoring_content (id, content) VALUES ({$subm->id},'{$jsonContent}')");
    } catch (PDOException $err) {
        echo "    ERROR >>> Content insertion error from indicator id {$indicatorUuid} with message {$err->getMessage()}\n";
        return -1;
    }

    echo "    Saved scoring content for indicator {$indicatorUuid} successfully.\n";

    return $subm->id;
}

function saveNewVersion($lastVersionNumber, $newVersionNumber, &$db)
{
    $truncateVersionTable = "TRUNCATE ais_indicator_scoring_cron";
    $updateNewVersion = "INSERT INTO ais_indicator_scoring_cron VALUES ({$newVersionNumber})";

    // Upsert (Insert or Update)
    // Instead of running multiple checks to see if there's something in the version table,
    //   we just truncate the table and insert the new version. We don't care about saving the
    //   the previous version so we can do this comfortably and it makes the upsert much easier
    //   and less complex.
    try {
        $db->query($truncateVersionTable);
        $db->query($updateNewVersion);
        echo "Version updated successfully!\n";
    } catch (PDOException $err) {
        echo "ERROR >>> Saving new version number " . $newVersionNumber . " with error message " . $err->getMessage() . "\n";
        if (isset($lastVersionNumber)) {
            echo "Old version was " . $lastVersionNumber . "\n";
        }
    }
}

function score($indicator, &$db, $env)
{
    $obj = new stdClass();

    if (listContains($db, $env, $indicator->value->name, 'ignorelist') === true || listContains($db, $env, $indicator->value->name, 'allowlist') === true) {
        if ($indicator->benign->value === true) {
            $obj->response = "Confirmed";
            $obj->score = 90;
            $obj->reason = "Confirmed; likely malicious (if marked or otherwise evaluated as malicious-activity) or benign (if marked as benign); consistent with other information on the subject known to the opinion author. Please see AIS Scoring Framework used for Indicator Enrichment at https://www.cisa.gov/ais.";
            $obj->opinion = "strongly-agree";
        } else {
            $obj->response = "Improbable";
            $obj->score = 10;
            $obj->reason = "Not confirmed; likely not malicious (if marked or otherwise evaluated as malicious-activity) or benign (if marked as benign); contradicted by other information on the subject known to the opinion author. Please see AIS Scoring Framework used for Indicator Enrichment at https://www.cisa.gov/ais.";
            $obj->opinion = "strongly-disagree";
        }
    } else if ($indicator->hitCount > 0) {
        if ($indicator->benign->value === false) {
            $obj->response = "Confirmed";
            $obj->score = 90;
            $obj->reason = "Confirmed; likely malicious (if marked or otherwise evaluated as malicious-activity) or benign (if marked as benign); consistent with other information on the subject known to the opinion author. Please see AIS Scoring Framework used for Indicator Enrichment at https://www.cisa.gov/ais.";
            $obj->opinion = "strongly-agree";
        } else {
            $obj->response = "Improbable";
            $obj->score = 10;
            $obj->reason = "Not confirmed; likely not malicious (if marked or otherwise evaluated as malicious-activity) or benign (if marked as benign); contradicted by other information on the subject known to the opinion author. Please see AIS Scoring Framework used for Indicator Enrichment at https://www.cisa.gov/ais.";
            $obj->opinion = "strongly-disagree";
        }
    } elseif ($indicator->verified === true) {
        $obj->response = "Probably True";
        $obj->score = 70;
        $obj->reason = "Not confirmed; likely malicious (if marked or otherwise evaluated as malicious-activity) or benign (if marked as benign); consistent with other information on the subject known to the opinion author. Please see AIS Scoring Framework used for Indicator Enrichment at https://www.cisa.gov/ais.";
        $obj->opinion = "agree";
    } elseif (sourceCount($indicator->sources, 1, '>')) {
        $obj->response = "Possibly True";
        $obj->score = 50;
        $obj->reason = "Not confirmed; possibly malicious (if marked or otherwise evaluated as malicious-activity) or benign (if marked as benign); agrees with some other information on the subject known to the opinion author. Please see AIS Scoring Framework used for Indicator Enrichment at https://www.cisa.gov/ais.";
        $obj->opinion = "neutral";
    } else {
        $obj->response = "Doubtful";
        $obj->score = 30;
        $obj->reason = "Not confirmed; possibly malicious (if marked or otherwise evaluated as malicious-activity) or benign (if marked as benign); no other information on the subject known to the opinion author. Please see AIS Scoring Framework used for Indicator Enrichment at https://www.cisa.gov/ais.";
        $obj->opinion = "disagree";
    };

    return $obj;
}

function scoreIndicators($indicators, &$db, $env)
{
    $scoredIndicators = [];

    echo "--- SCORING INDICATORS ---\n";
    for ($i = 0; $i < count($indicators); $i++) {
        $indicator = file_get_contents($env->api("i", $indicators[$i]->id, "", ""), false, $env->getContext());
        $indicator = json_decode($indicator);

        if ($indicator === false) {
            echo "ERROR >>> Failed to obtain details for indicator " . $indicators[$i]->id . ". Exiting...\n";
            cleanupAndClose(false, null, null);
        }

        // Score Indicator
        echo "Scoring indicator {$indicator->id} || value {$indicator->value->name}\n";
        $indicator->score = score($indicator, $db, $env);

        // Check for an error scoring the indicator
        if (is_int($indicator->score) && $indicator->score == -1) {
            echo "ERROR >>> INDICATOR SCORING: There was an error scoring indicator with Analyst1 ID {$indicator->id} and Value {$indicator->value->name}. Exiting...\n";
        } else if (!(count($indicator->stixObjects) > 0)) {
            echo "ERROR >>> STIX2 VALIDATION: No STIX Objects found for indicator {$indicator->id} and value {$indicator->value->name}. This indicator isn't STIX2.x. Skipping...\n";
        } else
            array_push($scoredIndicators, $indicator);
    }

    return $scoredIndicators;
}

function sourceCount($indicatorMentions, $n, $operation)
{
    $uniqueSources = count($indicatorMentions);
    return eval("return(" . $uniqueSources . "${operation} ${n});");
}

function submitOpinionObject($opinionObject, $opinionUuid, $indicatorUuid, $env)
{
    // Remove the Submission ID
    for ($i = 0; $i < count($opinionObject); $i++) {
        if (isset($opinionObject[$i]->submissionId)) {
            unset($opinionObject[$i]->submissionId);
            break;
        }
    }

    $opinionObject = (array) $opinionObject;

    $stix2Reply = array(
        "type" => "bundle",
        "id" => "bundle--" . UUID::v4(),
        "objects" => $opinionObject,
    );

    // Send Opinion to DSB
    $postdata = http_build_query(
        $stix2Reply
    );

    $opts = array(
        'http' => array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata,
        ),
    );

    $context = stream_context_create($opts);

    echo "    Submitting Bundle to DSB\n";
    echo "     - Bundle ID: {$stix2Reply["id"]}\n";
    echo "     - Opinion UUID: {$opinionUuid}\n";
    echo "     - Indicator UUID: {$indicatorUuid}\n";

    $result = file_get_contents($env->api("publish"), false, $context);

    if (!$result) {
        echo "   ERROR >>> There was an error submitting opinion object\n";
        print_r($opinionObject);
        echo "\n";
        return -1;
    }

    return $result;
}

function updateOpinionObjectStatus($submissionId, &$db, $opinionStatusCode)
{
    //Run the insert query for the bulk indicator content object
    try {
        $db->query("UPDATE ais_indicator_scoring SET opinion_submission_status = " . addslashes($opinionStatusCode) . " where id = {$submissionId}");
        echo "    Updated opinion status (code: {$opinionStatusCode}) for Submission ID {$submissionId} successfully\n";
        return 0;
    } catch (PDOException $err) {
        echo ("    ERROR >>> Couldn't update opinion status (code: {$opinionStatusCode}) for Submission ID {$submissionId} with message {$err->getMessage()}\n");
        return -1;
    }
}
