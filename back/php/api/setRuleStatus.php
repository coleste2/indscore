<?php

//This is an API endpoint that displays the blacklist in the DB

//allow cors
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');

//import db and env variables
require "./config.php";

$env = new env();
$db = $env->connectDB();

$data = json_decode(file_get_contents('php://input'));
$explanation = addslashes($data->explanation);
$id = -1;
try {
    $username = $env->getId();
    $sql = "SELECT analyst_id as id, username, privilege_level from ais_indicator_scoring_analysts where username = '{$username}'";
    $stmt = $db->query($sql);
    $user = $stmt->fetchAll(PDO::FETCH_OBJ);
    $id = $user[0]->id;
    if ((int) $user[0]->privilege_level < 3) {
        $err = new stdClass();
        $err->error = 'You do not have permissions to change to status';
        die(json_encode($err));
    }
} catch (PDOException $err) {
    die('{"error":{"text":' . $err->getMessage() . '}');
}

if ($data->status !== 1) {
    $sql = "UPDATE ais_indicator_scoring_rules SET status = " . addslashes($data->status) . " where rule_id = " . addSlashes($data->id);
    try {

        $stmt = $db->query($sql);

        //close connection

        $sql = "INSERT INTO ais_indicator_scoring_rules_workflow (new_status, analyst_id, message, rule_id) VALUES (" . addslashes($data->status) . ", {$id}, '{$explanation}', " . addSlashes($data->id) . ")";
        $stmt = $db->query($sql);


        $db = null;

        //echo results
        exit("Rule with id {$data->id} changed to status ");
    } catch (PDOException $err) {
        die('{"error":{"text":' . $err->getMessage() . '}');
    }
}
$sql = "SELECT rule_id from ais_indicator_scoring_rules where status = 1";
try {

    //query db
    $stmt = $db->query($sql);

    //fetch results
    $active = $stmt->fetchAll(PDO::FETCH_OBJ);
} catch (PDOException $err) {
    die('{"error":{"text":' . $err->getMessage() . '}');
}
for ($i = 0; $i < count($active); $i++) {
    $sql = "UPDATE ais_indicator_scoring_rules SET status = 2 where rule_id = " . addslashes($active[$i]->rule_id);
    try {
        $stmt = $db->query($sql);
    } catch (PDOException $err) {
        die('{"error":{"text":' . $err->getMessage() . '}');
    }
}
$sql = "UPDATE ais_indicator_scoring_rules SET status = 1 where rule_id = " . addslashes($data->id);
try {
    $stmt = $db->query($sql);
    $sql = "INSERT INTO ais_indicator_scoring_rules_workflow (new_status, analyst_id, message, rule_id,posted) VALUES (" . addslashes($data->status) . ", {$id}, '{$explanation}', " . addslashes($data->id) . ",'" . gmdate('Y-m-d H:i:s') . "')";
    $stmt = $db->query($sql);
    $db = null;
} catch (PDOException $err) {
    die('{"error":{"text":' . $err->getMessage() . '}');
}


echo "Rule with id {$data->id} changed to status ";
