<?php

//This is an API endpoint that shows the env setting of the server

//enable cors
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');

//import env
require "./config.php";

$env = new env();

$sourceTitle;
$sourceId;
$isRapid = isset($_GET['isRapid']);
if (isset($_GET['sourceTitle']) && isset($_GET['sourceId'])) {
    $sourceId = $_GET['sourceId'];
    $sourceTitle = $_GET['sourceTitle'];    
} else {
    $json = json_decode(file_get_contents($env->api("ep", $_GET['id'])));
    $sourceId = $json->sourceId;
    $sourceTitle = $json->sourceTitle;
}

$indicatorPage = 1;
$resp = [];
$ep = new stdClass();

$ep->id = $_GET['id'];
$ep->type = "evidencePackage";
$ep->isEvidence = true;
$ep->sourceTitle = $sourceTitle;
$ep->sourceId = $sourceId;

$resp[] = $ep;
$contIndicator = true;
while ($contIndicator === true) {

    $indicators = json_decode(file_get_contents($env->api("ep-i", $_GET['id'], $page)));

    for ($i = 0; $i < count($indicators->results); $i++) {
        //Before adding, we can remove properties if we want
        if ($isRapid === true && $indicators->results[$i]->reportCount > 10000){
            continue;
        }
        $indicator = new stdClass();
        
        $indicator->sourceTitle = $sourceTitle;
        $indicator->id = $indicators->results[$i]->id;
        $indicator->type = $indicators->results[$i]->type;
        $indicator->value = $indicators->results[$i]->value;

        $indicator->reportedDates = $indicators->results[$i]->reportedDates;
        $indicator->status = $indicators->results[$i]->status;

        $indicator->active = $indicators->results[$i]->active;
        $indicator->benign = $indicators->results[$i]->benign;

        $indicator->lastHit = $indicators->results[$i]->lastHit;
        $indicator->firstHit = $indicators->results[$i]->firstHit;
        $indicator->hitCount = $indicators->results[$i]->hitCount;
        $indicator->reportCount = $indicators->results[$i]->reportCount;
        $indicator->verified = $indicators->results[$i]->verified;
        $indicator->tasked = $indicators->results[$i]->tasked;


        $resp[] = $indicator;
    }

    if ($indicators->totalPages <= $indicatorPage) {
        $contIndicator = false;
    }
    $indicatorPage++;
}
echo json_encode($resp);
