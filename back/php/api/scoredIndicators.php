<?php

//This is an API endpoint to fetch all scored indicators from the DB

//allow cors
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');

//get ENV variables and the DB 
require "./config.php";


$data = $_GET;
if (isset($_GET['filter'])) {
    $data['filter'] = json_decode($data['filter']);
}

$data = new stdClass();
if (isset($_GET['page'])) {
    $data->page = $_GET['page'];
}
if (isset($_GET['pageSize'])) {
    $data->pageSize = $_GET['pageSize'];
}
if (isset($_GET['filter'])) {
    $data->filter = json_decode($_GET['filter']);
}
if (isset($_GET['sortBy'])) {
    $data->sortBy = $_GET['sortBy'];
}
if (isset($_GET['sortDir'])) {
    $data->sortDir = $_GET['sortDir'];
}

$where = filterConv($data);
if (isset($_GET['numbersOnly'])) {

    if (isset($_GET['bar'])) {
        try {
            if ($where === "") {

                $sql = "SELECT score_value, COUNT(*) AS count FROM ais_indicator_scoring GROUP BY score_value ORDER BY score_value";
            } else {
                $sql = "SELECT score_value, COUNT(*) AS count FROM ais_indicator_scoring WHERE {$where} GROUP BY score_value  ORDER BY score_value";
            }
            $db = new env();
            $db = $db->connectDB();
            //Query db and fetch data
            $stmt = $db->query($sql);
            $arr = $stmt->fetchAll(PDO::FETCH_OBJ);
            $total = 0;
            for ($i = 0; $i < count($arr); $i++) {
                $total += (int) $arr[$i]->count;
            }
            $obj = new stdClass();

            $obj->res = ["10", "30", "50", "70", "90"];
            $obj->per = [];
            $index = 0;
            for ($i = 0; $i < count($obj->res); $i++) {
                if ($obj->res[$i] === $arr[$index]->score_value) {
                    $obj->res[$i] =  (int) $arr[$index]->count;
                    $obj->per[] = round(((int) $arr[$index]->count)/$total, 1)*100;
                    $index++;
                } else {
                    $obj->res[$i] = 0;
                    $obj->per[] = 0;
                }
            }
            echo json_encode($obj);
            exit();
        } catch (\Exception $e) {
            echo $e;
        }
    }
    if (isset($_GET['radar'])) {
        try {
            if ($where === "") {
                $sql = "SELECT count(*) as counts, avg(score_value) as scores, indicator_type as types FROM ais_indicator_scoring GROUP BY indicator_type ORDER BY indicator_type";
            } else {
                $sql = "SELECT count(*) as counts, avg(score_value) as scores, indicator_type as types FROM ais_indicator_scoring WHERE {$where} GROUP BY indicator_type ORDER BY indicator_type";
            }

            $db = new env();
            $db = $db->connectDB();
            //Query db and fetch data
            $stmt = $db->query($sql);
            $arr = $stmt->fetchAll(PDO::FETCH_OBJ);
            $totalCounts = 0;
            for ($i = 0; $i < count($arr); $i++){
                $totalCounts += $arr[$i]->counts;
            }
            $labels = ["domain", "email", "file", "httpRequest", "ip", "ipv6", "mutex", "string", "url"];
            $obj = new stdClass();
            $obj->counts = [];
            $obj->scores = [];
            $obj->ratios = [];
            $pointIndex = 0;
            for ($i = 0; $i < count($labels); $i++) {
                if ($labels[$i] !== $arr[$pointIndex]->types) {
                    $obj->counts[] = 0;
                    $obj->scores[] = 0;
                    $obj->ratios[] = 0;
                } 
                else {
                    $obj->counts[] = (float)$arr[$pointIndex]->counts;
                    $obj->scores[] = round((float)$arr[$pointIndex]->scores,1);
                    $obj->ratios[] = (float)$arr[$pointIndex]->counts / $totalCounts;
                    $pointIndex++;
                }
            }
            echo json_encode($obj);

            exit();
        } catch (\Exception $e) {
            echo $e;
        }
    }
    // $sql = "SELECT indicator_id, score_value, CONCAT(REPLACE (datetime_of_score, ' ', 'T'), '+00:00') AS datetime_of_score FROM ais_indicator_scoring";
    elseif (isset($_GET['plot'])) {
        try {
            if ($where === "") {
                $sql = "SELECT count(*) as count, score_value, DATE(datetime_of_score) as datetime_of_score from ais_indicator_scoring  group by score_value, DATE(datetime_of_score) ORDER BY datetime_of_score";
            } else {
                $sql = "SELECT count(*) as count, score_value, DATE(datetime_of_score) as datetime_of_score from ais_indicator_scoring WHERE {$where} group by score_value, DATE(datetime_of_score)  ORDER BY datetime_of_score";
            }
            $db = new env();
            $db = $db->connectDB();
            $stmt = $db->query($sql);
            $arr = $stmt->fetchAll(PDO::FETCH_OBJ);

            $obj = new stdClass();
            for ($i = 0; $i < count($arr); $i++) {
                $str = "_" . $arr[$i]->score_value;
                $dp = new stdClass();
                $dp->y = (int) $arr[$i]->count;
                $dp->x = $arr[$i]->datetime_of_score;
                if (property_exists($obj, $str)) {
                    $obj->{$str}[] = $dp;
                } else {
                    $obj->{$str} = [$dp];
                }
                $arr[$i] = null;
            }
            $res = [10, 30, 50, 70, 90];

            for ($i = 0; $i < count($res); $i++) {
                $piece = new stdClass();
                if (property_exists($obj, "_" . $res[$i])) {
                    $piece->score = $res[$i];
                    $piece->data = $obj->{"_" . $res[$i]};
                } else {
                    $piece->score = $res[$i];
                    $piece->data = [];
                }
                $res[$i] = $piece;
            }

            echo json_encode($res);
            exit();
        } catch (\Exception $e) {
            echo $e;
        }
    } elseif (isset($_GET['statsp1'])) {
        try {

            if ($where === "") {
                $sql = "SELECT avg(score_value) as mean, stddev(score_value) as stdev, MAX(score_value) - MIN(score_value) AS dataRange, count(*) as count from ais_indicator_scoring";
            } else {
                $sql = "SELECT avg(score_value) as mean, stddev(score_value) as stdev, MAX(score_value) - MIN(score_value) AS dataRange, count(*) as count from ais_indicator_scoring  WHERE {$where}";
            }
            $obj = new stdClass();
            $db = new env();
            $db = $db->connectDB();
            $stmt = $db->query($sql);
            $arr = $stmt->fetchAll(PDO::FETCH_OBJ);
            $obj->mean = round((float) $arr[0]->mean, 1);
            $obj->stdev = round((float)$arr[0]->stdev, 1);
            $obj->range = (int)$arr[0]->dataRange;
            $obj->count = (int)$arr[0]->count;

            echo json_encode($obj);
        } catch (\Exception $e) {
            echo $e;
        }
        exit();
    } 
    elseif (isset($_GET['statsp2'])) {
        try {

            if ($where === "") {
                $sql = " SELECT score_value from ais_indicator_scoring  GROUP BY score_value having count(*) = (SELECT count(*) FROM  ais_indicator_scoring GROUP BY score_value ORDER BY COUNT(*) DESC LIMIT 1) ORDER By score_value;";
            } else {
                $sql = " SELECT score_value from ais_indicator_scoring WHERE {$where} GROUP BY score_value having count(*) = (SELECT count(*) FROM  ais_indicator_scoring WHERE {$where} GROUP BY score_value ORDER BY COUNT(*) DESC LIMIT 1) ORDER By score_value;";
            }
            $obj = new stdClass();
            $db = new env();
            $db = $db->connectDB();

            $stmt = $db->query($sql);
            $arr = $stmt->fetchAll(PDO::FETCH_OBJ);
            $obj->mode = [];
            for ($i = 0; $i < count($arr); $i++) {
                $obj->mode[] = $arr[$i]->score_value;
            }

            echo json_encode($obj);
        } catch (\Exception $e) {
            echo $e;
        }
        exit();
    }
    elseif (isset($_GET['quartiles'])) {
        try {

            if ($where === "") {
                $sql3 = "SET @rowindex := -1";
                $sql4 = "SELECT avg(ans) as score, quartile FROM (SELECT g.rowindex, g.grade as ans, if (g.rowindex = FLOOR(@rowindex / 4) OR  g.rowindex =  CEIL(@rowindex / 4), '25',if (g.rowindex = FLOOR(@rowindex / 2) OR  g.rowindex =  CEIL(@rowindex / 2), '50','75')) as quartile FROM  (SELECT @rowindex:=@rowindex + 1 AS rowindex, ais_indicator_scoring.score_value AS grade FROM ais_indicator_scoring  ORDER BY ais_indicator_scoring.score_value) AS g WHERE g.rowindex IN (FLOOR(@rowindex / 2) , CEIL(@rowindex / 2),FLOOR(@rowindex / 4) , CEIL(@rowindex / 4),FLOOR(@rowindex / 1.33) , CEIL(@rowindex / 1.33))) as tabl group by quartile order by quartile;";
            } else {
                $sql3 = "SET @rowindex := -1";
                $sql4 = "SELECT avg(ans) as score, quartile FROM (SELECT  g.rowindex, g.grade as ans, if (g.rowindex = FLOOR(@rowindex / 4) OR  g.rowindex =  CEIL(@rowindex / 4), '25',if (g.rowindex = FLOOR(@rowindex / 2) OR  g.rowindex =  CEIL(@rowindex / 2), '50','75')) as quartile FROM  (SELECT @rowindex:=@rowindex + 1 AS rowindex, ais_indicator_scoring.score_value AS grade FROM ais_indicator_scoring WHERE {$where} ORDER BY ais_indicator_scoring.score_value) AS g WHERE g.rowindex IN (FLOOR(@rowindex / 2) , CEIL(@rowindex / 2),FLOOR(@rowindex / 4) , CEIL(@rowindex / 4),FLOOR(@rowindex / 1.33) , CEIL(@rowindex / 1.33))) as tabl group by quartile order by quartile;";
            }
            $obj = new stdClass();
            $db = new env();
            $db = $db->connectDB();

            try {
                $stmt = $db->query($sql3);
                $stmt = $db->query($sql4);
                $arr = $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (PDOException $err) {
                die('{"error":{"text":' . $err->getMessage() . '}');
            }

            $obj->quartile25 = (float) $arr[0]->score;
            $obj->quartile75 = (float) $arr[2]->score ? (float) $arr[2]->score : $obj->quartile25;

            $obj->median = (float) $arr[1]->score ? (float) $arr[1]->score : ($obj->quartile25 + $obj->quartile75) / 2;


            $iqr = ((int) $obj->quartile75 - (int) $obj->quartile25);
            $ul = $obj->quartile75 +  1.5 *  $iqr;
            $ll = $obj->quartile25 -  1.5 *  $iqr;
            $obj->outliers = [];

            $sql = "SELECT min(score_value) as min, max(score_value) as max from ais_indicator_scoring where (score_value BETWEEN ${ll} AND ${ul})" . ($where !== "" ? " AND {$where}" : "");
            try {
                $stmt = $db->query($sql);
                $arr = $stmt->fetchAll(PDO::FETCH_OBJ);
                $obj->maxWhisker = (float)$arr[0]->max;
                $obj->minWhisker = (float) $arr[0]->min;
            } catch (PDOException $err) {
                die('{"error":{"text":' . $err->getMessage() . '}');
            }

            if ($ul <= 90 || $ll  >= 10) {
                $sql = "SELECT DISTINCT score_value from ais_indicator_scoring where (score_value NOT BETWEEN ${ll} AND ${ul})" . ($where !== "" ? " AND {$where}" : "");
                $stmt = $db->query($sql);
                $arr = $stmt->fetchAll(PDO::FETCH_OBJ);
                for ($i = 0; $i < count($arr); $i++) {
                    $obj->outliers[] = (int) $arr[$i]->score_value;
                }
            }
            echo json_encode($obj);
        } catch (\Exception $e) {
            echo $e;
        }
        exit();
    } elseif (isset($_GET['historics'])) {
        try {
            if ($where === "") {
                $sql1 = "SELECT avg(count) as average, max(count) as max, score_value from (SELECT score_value, count(*) as count from ais_indicator_scoring group by score_value, DATE(datetime_of_score)) as tab group by score_value";
                $sql2 = "SELECT avg(count) as average, max(count) as max, score_value from (SELECT score_value, count(*) as count from ais_indicator_scoring group by score_value, WEEK(datetime_of_score), YEAR(datetime_of_score)) as tab group by score_value";
            } else {
                $sql1 = "SELECT avg(count) as average, max(count) as max, score_value from (SELECT score_value, count(*) as count from ais_indicator_scoring where {$where} group by score_value, DATE(datetime_of_score)) as tab group by score_value";
                $sql2 = "SELECT avg(count) as average, max(count) as max, score_value from (SELECT score_value, count(*) as count from ais_indicator_scoring where {$where} group by score_value, WEEK(datetime_of_score), YEAR(datetime_of_score)) as tab group by score_value";
            }

            $obj = new stdClass();
            $db = new env();
            $db = $db->connectDB();

            try {
                $stmt = $db->query($sql1);
                $arr = $stmt->fetchAll(PDO::FETCH_OBJ);
                $res = new stdClass();
                $res->max = ["10", "30", "50", "70", "90"];
                $res->avg = ["10", "30", "50", "70", "90"];
                $index = 0;
                for ($i = 0; $i < count($res->max); $i++) {
                    if ($arr[$index]->score_value === $res->max[$i]) {
                        $res->max[$i] = (int) $arr[$index]->max;
                        $res->avg[$i] = (int) $arr[$index]->average;
                        $index++;
                    } else {
                        $res->max[$i] = 0;
                        $res->avg[$i] = 0;
                    }
                }
                $obj->daily = $res;
                $stmt = $db->query($sql2);
                $arr = $stmt->fetchAll(PDO::FETCH_OBJ);
                $res = new stdClass();
                $res->isWeek = true;
                $res->max = ["10", "30", "50", "70", "90"];
                $res->avg = ["10", "30", "50", "70", "90"];
                $index = 0;
                for ($i = 0; $i < count($res->max); $i++) {
                    if ($arr[$index]->score_value === $res->max[$i]) {
                        $res->max[$i] = (int) $arr[$index]->max;
                        $res->avg[$i] = (int) $arr[$index]->average;
                        $index++;
                    } else {
                        $res->max[$i] = 0;
                        $res->avg[$i] = 0;
                    }
                }
                $obj->weekly = $res;
            } catch (PDOException $err) {
                die('{"error":{"text":' . $err->getMessage() . '}');
            }
            echo json_encode($obj);
        } catch (\Exception $e) {
            echo $e;
        }
        exit();
    }
    exit();
}

$limit = pageSizeConv($data);
$offset = pageNumConv($data);
$orderBy = sortConv($data);
$sql1 = "SELECT id, indicator_id, ais_indicator_scoring.rule_id, ais_indicator_scoring_rules.name as rule_name, CONCAT(REPLACE (datetime_of_score, ' ', 'T'), '+00:00') AS datetime_of_score, score_value, score_name, score_explanation, opinion_value, indicator_value, indicator_type FROM ais_indicator_scoring INNER JOIN ais_indicator_scoring_rules on ais_indicator_scoring_rules.rule_id = ais_indicator_scoring.rule_id";
$sql2 = "SELECT count(*) AS count FROM ais_indicator_scoring";

if ($where !== "") {
    $sql1 .= " WHERE {$where} ";
    $sql2 .= " WHERE {$where} ";
}
if ($orderBy !== "") {
    $sql1 .= " ORDER BY {$orderBy} ";
}
if ($limit !== "") {
    $sql1 .= " LIMIT {$limit}";
}
if ($offset !== "") {
    $sql1 .= " OFFSET {$offset}";
}
try {

    //connecy to DB
    $db = new env();
    $db = $db->connectDB();
    //Query db and fetch data
    $stmt = $db->query($sql1);
    $arr = $stmt->fetchAll(PDO::FETCH_OBJ);

    $stmt = $db->query($sql2);
    $count = $stmt->fetchAll(PDO::FETCH_OBJ);

    $obj = new stdClass();
    $obj->totalCount = (int) $count[0]->count;
    $obj->data = $arr;

    //Close Connection
    $db = null;
    //echo all scored indicators
    echo json_encode($obj);
} catch (PDOException $err) {
    echo $sql1 . "\n\n";

    die('{"error":{"text":' . $err->getMessage() . '}');
}

function filterConv($data)
{

    if (!property_exists($data, 'filter')) return "";
    $filter = $data->filter;
    $where = "";
    $i = 0;
    foreach ($filter as $key => $value) {
        if ($i !== 0) {
            $where .= " AND ";
        }
        $where .= "(";
        if ($key === "scoreFilter" || $key === "opinionFilter" || $key === "indicatorTypes") {
            $where .= arrayFilter($value, $key);
        } elseif ($key === "dateFilter") {
            $where .= dateFilter($value);
        } elseif ($key === "indicatorTextFilter") {
            for ($j = 0; $j < count($value); $j++) {
                if ($j !== 0) {
                    $where .= " AND ";
                }
                $where .= textFilter($value[$j]);
            }
        }
        $where .= ")";
        $i++;
    }
    return $where;
}
function pageSizeConv($data)
{
    if (!property_exists($data, 'pageSize')) return "";

    return addslashes("" . $data->pageSize);
}
function pageNumConv($data)
{
    if (!property_exists($data, 'page') || !property_exists($data, 'pageSize')) return "";

    return addslashes("" . ($data->page - 1) * $data->pageSize);
}

function sortConv($data)
{
    if (!property_exists($data, 'sortBy') || !property_exists($data, 'sortDir')) return "";
    $prop = $data->sortBy;
    $dir = $data->sortDir;
    if ($prop === "Score") $sBy = "score_value";
    elseif ($prop === "Date Scored") $sBy = "datetime_of_score";
    elseif ($prop === "Indicator Type") $sBy = "indicator_type";
    elseif ($prop === "Indicator ID") $sBy = "indicator_id";

    if ($dir === "a") return "{$sBy} ASC";
    elseif ($dir === "d") return "{$sBy} DESC";
}
function textFilter($value)
{
    if (startsWith($value->filter, "Indicator ID")) endsWith($value->filter, "is")  ? $field = "indicator_id" : $field = "CONVERT(indicator_id, CHAR)";
    elseif (startsWith($value->filter, "Indicator Value")) $field = "indicator_value";
    $str = " {$field} ";

    if (endsWith($value->filter, "regex search")) {
        if (preg_match($value->value, null) === false) {
            $regex = "a^";
        } else {
            $regex = addslashes($value->value);
        }
        return $str . " REGEXP '{$regex}' ";
    }
    $value->value = addslashes($value->value);
    if (property_exists($value, "caseSensitive") && $value->caseSensitive === true)     $str .= " LIKE BINARY ";
    else $str .= " LIKE ";
    if (endsWith($value->filter, "starts with")) return $str . " '{$value->value}%' ";
    elseif (endsWith($value->filter, "ends with")) return $str . " '%{$value->value}' ";
    elseif (endsWith($value->filter, "contains")) return $str . " '%{$value->value}%' ";
    elseif (endsWith($value->filter, "is")) return $str . " '{$value->value}' ";
}

function dateFilter($value)
{
    date_default_timezone_set('UTC');
    $d1 = date('Y-m-d H:i:s', strtotime($value[0]));
    $d2 = date('Y-m-d H:i:s', strtotime($value[1]));
    return " datetime_of_score BETWEEN '{$d1}' AND '{$d2}' ";
}
function arrayFilter($value, $key)
{
    $str = "";
    if ($key === "scoreFilter") {
        $col = "score_value";
    } elseif ($key === "opinionFilter") {
        $col = "opinion_value";
    } elseif ($key === "indicatorTypes") {
        $col = "indicator_type";
    }
    for ($i = 0; $i < count($value); $i++) {
        if ($i !== 0) $str .= " OR ";
        $str .= " {$col} = " . parenWrapper($col, $value[$i]) . " ";
    }
    return $str;
}
function startsWith($string, $startString)
{
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}
function endsWith($string, $endString)
{
    $len = strlen($endString);
    if ($len == 0) {
        return true;
    }
    return (substr($string, -$len) === $endString);
}
function parenWrapper($field, $val)
{
    if ($field === "indicator_id") return is_string($val) ? "'" . addslashes($val) . "'"  : addslashes($val);
    elseif ($field === "indicator_value") return "'" . addslashes($val) . "'";
    elseif ($field === "score_value") return is_string($val) ? "'" . addslashes($val) . "'"  : addslashes($val);
    elseif ($field === "opinion_value") return "'" . addslashes($val) . "'";
    elseif ($field === "indicator_type") return "'" . addslashes($val) . "'";
}
