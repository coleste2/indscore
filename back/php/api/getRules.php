<?php

//This is an API endpoint to fetch all scored indicators from the DB

//allow cors
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');

//get ENV variables and the DB 
require "./config.php";

if (isset($_GET['id'])) {
    if (!isset($_GET['full'])){
        $sql = "SELECT ais_indicator_scoring_rules.rule_id, ref_rules_status.meaning as status,ais_indicator_scoring_rules.name,ais_indicator_scoring_rules.description FROM ais_indicator_scoring_rules INNER JOIN ref_rules_status ON ais_indicator_scoring_rules.status = ref_rules_status.status WHERE rule_id in (".toString($_GET['id']).")";
        $sql = "SELECT rule_id,name,description,ref_rules_status.meaning as status, ais_indicator_scoring_analysts.username as author FROM ais_indicator_scoring_rules INNER JOIN ais_indicator_scoring_analysts ON ais_indicator_scoring_rules.analyst_id = ais_indicator_scoring_analysts.analyst_id INNER JOIN ref_rules_status ON ais_indicator_scoring_rules.status = ref_rules_status.status WHERE rule_id in (".toString($_GET['id']).")";

        try {
            //connecy to DB
            $db = new env();
            $db = $db->connectDB();
            //Query db and fetch data
            $stmt = $db->query($sql);
            $rule = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            //echo all scored indicators
            echo json_encode($rule);
            exit();
        } catch (PDOException $err) {
            die('{"error":{"text":' . $err->getMessage() . '}');
        }
    }
    $id = addslashes($_GET['id']);

    //Get all fields from DB 
    $sql = "SELECT ais_indicator_scoring_rules.rule, ais_indicator_scoring_analysts.username as analyst, ref_rules_status.meaning as status, ais_indicator_scoring_rules.name, ais_indicator_scoring_rules.description FROM ais_indicator_scoring_rules INNER JOIN ref_rules_status ON ais_indicator_scoring_rules.status = ref_rules_status.status INNER JOIN ais_indicator_scoring_analysts ON ais_indicator_scoring_rules.analyst_id = ais_indicator_scoring_analysts.analyst_id WHERE ais_indicator_scoring_rules.rule_id in ({$id})";
    try {
        //connecy to DB
        $db = new env();
        $db = $db->connectDB();
        //Query db and fetch data
        $stmt = $db->query($sql);
        $rules = $stmt->fetchAll(PDO::FETCH_OBJ);
        $rules =  $rules[0];

        $rules->rule = unserialize($rules->rule);
        unset($rules->rule->code);
        $sql = "SELECT CONCAT(REPLACE (ais_indicator_scoring_rules_workflow.posted, ' ', 'T'), '+00:00') AS posted, ais_indicator_scoring_rules_workflow.rule_id, ais_indicator_scoring_rules_workflow.message,ais_indicator_scoring_analysts.username as analyst, ref_rules_status.meaning as new_status FROM ais_indicator_scoring_rules_workflow ";
        $sql .= "INNER JOIN ref_rules_status ON ais_indicator_scoring_rules_workflow.new_status = ref_rules_status.status ";
        $sql .= "INNER JOIN ais_indicator_scoring_analysts ON ais_indicator_scoring_rules_workflow.analyst_id = ais_indicator_scoring_analysts.analyst_id ";
        $sql .= " WHERE ais_indicator_scoring_rules_workflow.rule_id = {$id} AND ais_indicator_scoring_rules_workflow.comment = 0";
        
        $stmt = $db->query($sql);
        $history = $stmt->fetchAll(PDO::FETCH_OBJ);

        $rules->history = $history;

        $sql = "SELECT CONCAT(REPLACE (ais_indicator_scoring_rules_workflow.posted, ' ', 'T'), '+00:00') AS posted, ais_indicator_scoring_rules_workflow.rule_id, ais_indicator_scoring_rules_workflow.message,ais_indicator_scoring_analysts.username as analyst FROM ais_indicator_scoring_rules_workflow ";
        $sql .= "INNER JOIN ais_indicator_scoring_analysts ON ais_indicator_scoring_rules_workflow.analyst_id = ais_indicator_scoring_analysts.analyst_id ";
        $sql .= " WHERE ais_indicator_scoring_rules_workflow.rule_id = {$id} AND ais_indicator_scoring_rules_workflow.comment = 1";
        
        $stmt = $db->query($sql);
        $history = $stmt->fetchAll(PDO::FETCH_OBJ);

        $rules->comments = $history;

        //Close Connection
        $db = null;

        //echo all scored indicators
        echo json_encode($rules);
    } catch (PDOException $err) {
        die('{"error":{"text":' . $err->getMessage() . '}');
    }
} else {
   if (isset($_GET['contains'])) {
        $str = addslashes($_GET['contains']);
        $sql = "SELECT rule_id,name,description,ref_rules_status.meaning as status, ais_indicator_scoring_analysts.username as author FROM ais_indicator_scoring_rules INNER JOIN ais_indicator_scoring_analysts ON ais_indicator_scoring_rules.analyst_id = ais_indicator_scoring_analysts.analyst_id INNER JOIN ref_rules_status ON ais_indicator_scoring_rules.status = ref_rules_status.status WHERE ais_indicator_scoring_rules.status >= 0 AND (rule_id LIKE '%{$str}%' OR name LIKE '%{$str}%' OR ais_indicator_scoring_analysts.username LIKE '%{$str}%' OR description LIKE '%{$str}%' OR ref_rules_status.meaning LIKE '%{$str}%') ORDER BY rule_id LIMIT 50";
    } else {
        $sql = "SELECT rule_id,name,description,CONCAT(REPLACE (ais_indicator_scoring_rules.created, ' ', 'T'), '+00:00') AS created, ref_rules_status.meaning as status, ais_indicator_scoring_analysts.username as author FROM ais_indicator_scoring_rules INNER JOIN ais_indicator_scoring_analysts ON ais_indicator_scoring_rules.analyst_id = ais_indicator_scoring_analysts.analyst_id INNER JOIN ref_rules_status ON ais_indicator_scoring_rules.status = ref_rules_status.status WHERE ais_indicator_scoring_rules.status >= 0 ORDER BY rule_id";
    }

    try {
        //connecy to DB
        $db = new env();
        $db = $db->connectDB();
        //Query db and fetch data
        $stmt = $db->query($sql);
        $rules = $stmt->fetchAll(PDO::FETCH_OBJ);
        //Close Connection
        $db = null;

        //echo all scored indicators
        echo json_encode($rules);
    } catch (PDOException $err) {
        die('{"error":{"text":' . $err->getMessage() . '}');
    }
}
function toString($list)
{
    $str = "";
    for ($i = 0; $i < count($list); $i++) {
        if ($i !== 0) $str .= " , ";
        $str .= " " . addslashes($list[$i]) . " ";
    }
    return $str;
}
