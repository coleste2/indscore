<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');

//This is a class that stored the DB connection utilities and the server mode
class env
{
    //Server mode accessible for all files that import the class

    //TO DO WHEN DEPLOYING: Make import paths of getIndicators file absolute
    /* ONLY LOCATION ONE*/
    public $env = 'production'; /* ONLY LOCATION ONE*/
    public $MOCK_ILLUMINATE_URL = 'http://localhost:5000';
    //TO DO WHEN DEPLOYING: Make path getIndicators file absolute
    // ID of the Sensor providing STIX2 indicators (named taskings)
    public $STIX2_SENSOR_ID = '7671';

    public function getId()
    {
        $username = '';
        if ($this->env === "production") {
            $username = addslashes(substr($_SERVER["REMOTE_USER"], 0, -19));
        } else {
            $username = 'marty';
        }
        return $username;
    }

    public function getStix2Ids()
    {
        return array(
            '100083',
            '100084',
            '100085',
            '100086',
        );
    }

    public function getContext()
    {
        $context = null;

        $context = stream_context_create(array(
            'http' => array(
                'header' => "Authorization: Basic " . base64_encode("matthew.zackschewski@associates.cisa.dhs.gov" . ":" . "Helloworld1234567!@#$%^&"),
            ),
        ));

        return $context;
    }

    public function getPostContext() {
        $context = null;

        $context = stream_context_create(array(
            "http" => array(
                'header' => "Authorization: Basic " . base64_encode("matthew.zackschewski@associates.cisa.dhs.gov" . ":" . "Helloworld1234567!@#$%^&"),
                "method" => "POST"
            ),
        ));

        return $context;
    }

    public function connectDB()
    {
        //Set db creds depending on server mode
        $production = $this->env === "production";
        if ($production) {
            $keys = parse_ini_file("passwords.ini");
            $MYSQL_HOST = $keys["MYSQL_HOST"];
            $MYSQL_PORT = $keys["MYSQL_PORT"];
            $MYSQL_USER = $keys["MYSQL_USER"];
            $MYSQL_PASSWORD = $keys["MYSQL_PASSWORD"];
            $MYSQL_NAME = $keys["MYSQL_NAME"];
            // $this->$ANALYST_ONE_USERNAME = $keys["ANALYST_ONE_USERNAME"];
            // $this->$ANALYST_ONE_PASSWORD = $keys["ANALYST_ONE_PASSWORD"];
        } else {
            $MYSQL_HOST = 'localhost';
            $MYSQL_PORT = '3306';
            $MYSQL_USER = 'root';
            $MYSQL_PASSWORD = 'R3al$P@ssw0rd^';
            $MYSQL_NAME = 'indscore';
            // $this->$ANALYST_ONE_USERNAME = "testing";
            // $this->$ANALYST_ONE_PASSWORD = "testing";
        }

        //Create connection string, setup error handling, and return connection
        $SQL_CONNECT_STR = "mysql:host=$MYSQL_HOST;port=$MYSQL_PORT;dbname=$MYSQL_NAME";
        $dbh = new PDO($SQL_CONNECT_STR, $MYSQL_USER, $MYSQL_PASSWORD);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbh;
    }

    // Used for New Sensor Connection
    /**
     * newApi
     *  (REQUIRED) $routeKey - key that maps to a specific API route
     *  (OPTIONAL) $lastReceivedVersion - last version of the sensor that AS&F saw
     */
    public function newApi($routeKey, $lastReceivedVersion, $reportId, $guid) {
        switch ($routeKey) {
            case "current-version-taskings": {
                return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/sensors/{$this->STIX2_SENSOR_ID}/taskings";
                break;
            }
            case "diff-version-taskings": {
                return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/sensors/{$this->STIX2_SENSOR_ID}/taskings/diff/{$lastReceivedVersion}";
            }
            case "auto-mitigate-taskings": {
                return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/sensors/{$this->STIX2_SENSOR_ID}/taskings/approveAllAndUpdate";
            }
            case "auto-mitigate-progress": {
                return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/sensors/{$this->STIX2_SENSOR_ID}/taskings/config/isUpdating";
            }
            case "object-stix": {
                return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/objects/stix/{$reportId}/{$guid}";
            }
            case "relationships-outgoing": {
                return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/objects/stix/{$reportId}/{$guid}/relationships/outgoing";
            }
        }
    }

    public function api($route, $id = "", $page = "", $operation = "")
    {
        if ($this->env === "development") {
            if ($route === "ep") {
                if ($id === "") {
                    return "http://localhost:5000/api/1_0/evidence";
                } else {
                    return "http://localhost:5000/api/1_0/evidence/{$id}";
                }
            } elseif ($route === "ep-i") {
                return "http://localhost:5000/api/1_0/evidence/{$id}/indicator";
            } elseif ($route === "i-ep") {
                return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/indicator/{$id}/evidence?page={$page}&pageSize=100";
            } elseif ($route === "i") {
                return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/indicator/{$id}?expand=sources";
            }elseif ($route === "publish") {
                return "http://localhost:9000/publish-opinion";
            } elseif ($route === "trep") {
                return "http://localhost:4300/trueRandomEvidencePackages";
            } elseif ($route === "rrep") {
                return "http://localhost:4300/rapidRandomEvidencePackages";
            } elseif ($route === "sep") {
                return "http://localhost:4300/specifiedEvidencePackages";
            } elseif ($route === "sc") {
                $n = 0;
                if ($operation === ">") {
                    $n = $page + 1;
                } elseif ($operation === '>=') {
                    $n = $page;
                }
                return "http://localhost:4300/sourceCount/{$id}" . ($n !== 0 ? "/{$n}" : "");
            } elseif ($route === "amqp") {
                return "http://localhost:4300/sendToAIS";
            } else {
                return "";
            }

        } else {
            if ($route === "ep") {
                if ($id === "") {
                    return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/evidence?page={$page}&pageSize=100";
                } else {
                    return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/evidence/{$id}";
                }
            } elseif ($route === "ep-i") {
                return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/evidence/{$id}/indicator?page=1&pageSize=100&expand=sources";
            } elseif ($route === "i-ep") {
                return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/indicator/{$id}/evidence?page={$page}&pageSize=100";
            } elseif ($route === "i") {
                return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/indicator/{$id}";
            } elseif ($route === "publish") {
                return "http://localhost:9000/publish-opinion";
            } elseif ($route === "trep") {
                return "https://sweb7-qa.bronze.us-cert.gov:5000/indscore/api/trueRandomEvidencePackages";
            } elseif ($route === "rrep") {
                return "https://sweb7-qa.bronze.us-cert.gov:5000/indscore/api/rapidRandomEvidencePackages";
            } elseif ($route === "sep") {
                return "https://sweb7-qa.bronze.us-cert.gov:5000/indscore/api/specifiedEvidencePackages";
            } elseif ($route === "igList") {
                return "https://ctm-illuminate-dev-01.dev.dso.ncps.us-cert.gov/api/1_0/ignoreList/indicator?page={$page}&pageSize=100&sortBy=id&descSort=true";
            } elseif ($route === "sc") {
                $n = 0;
                if ($operation === ">") {
                    $n = $page + 1;
                } elseif ($operation === ">=") {
                    $n = $page;
                }
                return "https://sweb7-qa.bronze.us-cert.gov:5000/indscore/api/sourceCount/{$id}" . ($n !== 0 ? "/{$n}" : "");
            } elseif ($route === "amqp") {
                return "https://sweb7-qa.bronze.us-cert.gov:5000/indscore/api/sendToAIS";
            } else {
                return "";
            }

        }
    }
}
