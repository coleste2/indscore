<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');

//get ENV variables and the DB 
require "./config.php";

try {
    $env = new env();
    $db = $env->connectDB();

    $username = $env->getId();

    $sql = "SELECT analyst_id as id, username from ais_indicator_scoring_analysts where username = '{$username}'";
    $stmt = $db->query($sql);
    $analystID = $stmt->fetchAll(PDO::FETCH_OBJ);


    if (count($analystID) === 0) {
        $sql = "INSERT INTO ais_indicator_scoring_analysts (username) VALUES ('{$username}')";
        $stmt = $db->query($sql);  
        $sql = "SELECT analyst_id as id, username from ais_indicator_scoring_analysts where username = '{$username}'";
        $stmt = $db->query($sql);
        $analystID = $stmt->fetchAll(PDO::FETCH_OBJ);
    } else {
        $sql = "UPDATE ais_indicator_scoring_analysts SET login_hits = login_hits + 1 where username = '{$username}'";
        $stmt = $db->query($sql);
    }
    $analystID[0]->id = (int) $analystID[0]->id;
    echo (json_encode($analystID[0]));
} catch (PDOException $err) {
    die('{"error":{"text":' . $err->getMessage() . '}');
}