<?php
//This is an API endpoint that shows the stored content object of a given indicator

//allow cors
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');

//ensure there is an ID paramater named ID
if (isset($_GET['id'])) {
    require "./config.php";

    //get the content field from the DB where the passed in ID matches the unique ID in the DB
    $sql = "SELECT content FROM ais_indicator_scoring_content WHERE id = ".addslashes($_GET['id']);

    try {
        //Connect to db
        $db = new env();
        $db = $db->connectDB();

        //query and fetch content field
        $stmt = $db->query($sql);
        $subm = $stmt->fetchAll(PDO::FETCH_OBJ);

        //close connection
        $db = null;

        //get the content and unserialize it from a string into an object
        $json = $subm[0]->content;
        $json = json_decode($json);

        //If the serialized object cannot be converted back to object, send the string in an error, because something is better than nothing
        if ($json === false){
            $json = new stdClass();
            $json->error =  $subm[0]->content;
        }

        //Send content field, or the error and the serialized string
        echo json_encode($json);
    } catch (PDOException $err) {
        die('{"error":{"text":' . $err->getMessage() . '}');
    }
}
