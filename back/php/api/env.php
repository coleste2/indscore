<?php

//This is an API endpoint that shows the env setting of the server

//enable cors
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');

//import env
require "./config.php";

$env = new env();

//echo env setting
echo $env->env;