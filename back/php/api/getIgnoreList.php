<?php

//This is an API endpoint to get items from the ignoreList and add them to the DB

//Import env variables and DB, and connect the connect to db
require "/var/www/html/indscore/api/config.php";
$env = new env();
$db = $env->connectDB();

$pageNumber = 1;
try {
	$db->query("TRUNCATE ref_ignorelist");
} catch (PDOException $err) {
	die("Error occurred truncating the ignore list prior to refresh with error " . $err);
}

//Go until told to stop by inside the while loop finding the ID it is looking for
do {
    $json = file_get_contents($env->api("igList", "", $pageNumber), false, $env->getContext());
    if ($json === false) {
        die("Error getting contents\n");
    } else {
        $json = json_decode($json);
    }
    echo ("Fetching page " . $pageNumber . " of " . $json->totalPages . "\n");

    //Go through all packages returned on the page and stop the while loop only if we find the last one currently in the DB or less than for safety in case of corruption
    $ignoreListIndicators = "";
    for ($i = 0; $i < count($json->results); $i++) {
        if ($i != 0)
            $ignoreListIndicators = $ignoreListIndicators . ",";
        $ignoreListIndicators = $ignoreListIndicators . " ('" . addslashes($json->results[$i]->value) . "')";
    }

    try {
        $db->query("INSERT INTO ref_ignorelist (value) VALUES " . $ignoreListIndicators);
    } catch (PDOException $err) {
        print_r($err);
        die("Error occurred during insertion into MYSQL database with error " . $err);
    }

    $pageNumber++;
} while ($json->totalPages >= $pageNumber);

echo ("Ignore List refreshed successfully!\n");
