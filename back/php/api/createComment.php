<?php
//This is an API endpoint that shows the stored content object of a given indicator

//allow cors
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');
require "./config.php";




try {
    $entityBody = json_decode(file_get_contents('php://input'));

    $message = addslashes($entityBody->message);
    $rule_id = addslashes($entityBody->rule_id);

    $env = new env();
    $db = $env->connectDB();

    $username = $env->getId();
    $sql = "SELECT analyst_id as id, username from ais_indicator_scoring_analysts where username = '{$username}'";
    $stmt = $db->query($sql);
    $analyst_id = $stmt->fetchAll(PDO::FETCH_OBJ);
    $analyst_id = $analyst_id[0]->id;

    //get the content field from the DB where the passed in ID matches the unique ID in the DB
    $sql = "INSERT into ais_indicator_scoring_rules_workflow (message, rule_id, analyst_id, posted, comment) values ('{$message}', {$rule_id}, {$analyst_id}, '" . gmdate('Y-m-d H:i:s') . "',1)";


    //query and fetch content field
    $stmt = $db->query($sql);

    //close connection
    $db = null;

    //Send content field, or the error and the serialized string
    echo json_encode("Comment Posted");
} catch (PDOException $err) {
    die('{"error":{"text":' . $err->getMessage() . '}');
}
