<?php

//This is an exmaple php endpoint that gets the headers, body, and arguments from a request
require "./config.php";

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');
try {
    try {
        //Get data from request
        $data = json_decode(file_get_contents('php://input'));
        if (!$data->rule){
            die("No rule included");
        }
        $name = addslashes($data->name);
        $description = addslashes($data->description);

        $rule = $data->rule;
        
        //Save copy of rule to send to front end for rendering
        $saveRule = json_decode(json_encode($rule));

        $env = new env();
        $db = $env->connectDB();

        $username = $env->getId();

        //Get analyst ID to store with rule
        $sql = "SELECT analyst_id as id, username from ais_indicator_scoring_analysts where username = '{$username}'";
        $stmt = $db->query($sql);
        $analystID = $stmt->fetchAll(PDO::FETCH_OBJ);

        $user = $analystID[0]->id;

        $object = new stdClass();

        $statement = "";
        
        //Write logic of rule with if statements
        for ($i = 0; $i < count($rule); $i++) {
            //If rule is not the last rule in list
            if ($i < count($rule) - 1) {
                //Breakdown rule recursively
                $logic = parseLogic($rule[$i]->rule);
                //Convert the rule assignment into code
                $body = getBody($rule[$i]);
                if ($i === 0) {
                    $statement .= "if (" . $logic . "){" . $body . "}";
                } else {
                    $statement .= "elseif (" . $logic . "){" . $body . "}";
                }
            } else {
                //Convert the rule assignment into code
                $body = getBody($rule[$i]);
                $statement .= "else{" . $body . "}";
            }
        }
        $object->rule = $saveRule;
        $object->code = $statement;
        $object = serialize($object);
        $object = addslashes($object);
       
        //Insert Rule into DB
        $sql = "INSERT INTO ais_indicator_scoring_rules (analyst_id, rule, name, description, created) VALUES ({$user},'{$object}','{$name}','{$description}', '" . gmdate('Y-m-d H:i:s') . "')";
        $stmt = $db->query($sql);

    } catch (PDOException $err) {
        die('{"error":{"text":' . $err->getMessage() . '}');
    }

    try {
        //connect to DB

        //Query db and fetch data

        $sql = "SELECT last_insert_id() as rule_id";
        $stmt = $db->query($sql);
        $subm = $stmt->fetchAll(PDO::FETCH_OBJ);
        $subm = $subm[0];
        //Begin rule history
        $sql = "INSERT INTO ais_indicator_scoring_rules_workflow (analyst_id, rule_id, new_status, message, posted) VALUES ({$user},{$subm->rule_id}, 0,'Rule created', '" . gmdate('Y-m-d H:i:s') . "')";
        $stmt = $db->query($sql);

        //Close Connection
        $db = null;
        echo json_encode($subm);
    } catch (PDOException $err) {
        die('{"error":{"text":' . $err->getMessage() . '}');
    }
} catch (\Exception $err) {
    die($err);
}

function getBody($outcome)
{
    $body = "\$obj->response = \"" . $outcome->scoreName . "\";";
    $body .= "\$obj->score = " . $outcome->score . ";";
    $body .= "\$obj->reason = \"" . $outcome->rule->label . "\";";
    $body .= "\$obj->opinion = \"" . $outcome->opinion . "\";";
    return $body;
}

function parseLogic($rule)
{
    $logic = "";
    if ($rule->combined === true) {
        $logic = breakdownRule($rule);
    } else {
        $logic = convertRule($rule);
    }
    return $logic;
}

function breakdownRule($rule)
{
    $str = "";
    for ($i = 0; $i < count($rule->statements); $i++) {
        if ($rule->statements[$i]->combined === true) {
            $str .= "(" . breakdownRule($rule->statements[$i]) . ")";
        } else {
            $str .= convertRule($rule->statements[$i]);
        }
        if ($i < count($rule->statements) - 1) {
            $str .=  $rule->linkage;
        }
    }
    return $str;
}

function convertRule($rule)
{
    $str = "";
    if ($rule->prop === "allowlist" || $rule->prop === "ignorelist") {
        $str .= "listContains(\$db, \$env, \$indicator->value->name, '". $rule->prop . "')";
        $str .=  $rule->operation . $rule->value;
    } elseif ($rule->prop === "lastHit") {
        $str .= "\$indicator->lastHit !== null && floor((time() - strtotime(\$indicator->lastHit)) / (60 * 60 * 24))";
        $str .=  $rule->operation . $rule->value;
    } elseif ($rule->prop === "sourceCount") {
        //This function will take care of evaluation of operation because it enables internal short-circuit optimizations
        $str .= "sourceCount(\$indicatorMentions, " . $rule->value . ", '" . $rule->operation . "')";
    } elseif ($rule->operation === "regex") {
        $str .= "preg_match('" . $rule->value . "',\$indicator->" . $rule->prop . ")";
    } else {
        $rule->value = addslashes($rule->value);
        $str .= "\$indicator->" . $rule->prop;
        $stringValue = $rule->prop === "sourceTitle" || $rule->prop === "type" || $rule->prop === "value->name" || $rule->prop === "status";
        $str .=  $rule->operation . ($stringValue ? "'" . $rule->value . "'"  : $rule->value);
    }

    if ($rule->negate === true) {
        $str = "!(" . $str . ")";
    }
    return $str;
}
