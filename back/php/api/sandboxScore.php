<?php

//This is an exmaple php endpoint that gets the headers, body, and arguments from a request
require "./config.php";

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');

try {
    $data = json_decode(file_get_contents('php://input'));

    if (!$data->id) {
        die("Need ID");
    }

    $env = new env();
    $db = $env->connectDB();
    try {
        $sql = "SELECT ind_type, value FROM ref_illuminate_blacklist";
        $stmt = $db->query($sql);
        $blacklist = $stmt->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $err) {
        echo '{"error":{"text":' . $err->getMessage() . '}\n';
    }

    try {
        $ruleList = toString($data->id);

        $sql = "SELECT rule, rule_id FROM ais_indicator_scoring_rules WHERE rule_id IN ({$ruleList}) ORDER BY rule_id";
        $stmt = $db->query($sql);
        $rules = $stmt->fetchAll(PDO::FETCH_OBJ);
        for ($k = 0; $k < count($rules); $k++) {
            try {
                $rules[$k]->code = unserialize($rules[$k]->rule)->code;
            } catch (\Throwable $err) {
                echo $err;
            }
        }
    } catch (PDOException $err) {
        echo '{"error":{"text":' . $err->getMessage() . '}\n';
    }

    $indicators = [];
    $hasMentions = false;
    if (isset($data->specify)) {
        $indicators = json_decode(post($env->api("sep"), $data->specify))->data;
    } elseif ($data->rapid) {
        $indicators = json_decode(post($env->api("rrep"), $data->rapid))->data;
    } elseif (isset($data->random)) {
        $indicators = json_decode(post($env->api("trep"), $data->random))->data;
    } elseif (isset($data->psuedo)) {
        $hasMentions = true;
        try {
            try {
                $sql = "SELECT ais_indicator_scoring.evidence_id, ais_indicator_scoring.evidence_source_name, ais_indicator_scoring.evidence_source_id, ais_indicator_scoring_content.content FROM ais_indicator_scoring INNER JOIN ais_indicator_scoring_content ON ais_indicator_scoring.id = ais_indicator_scoring_content.id ORDER BY rand() LIMIT " . addslashes($data->psuedo);
                $stmt = $db->query($sql);
                $indSql = $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (PDOException $err) {
                echo '{"error":{"text":' . $err->getMessage() . '}\n';
            }
            for ($i = 0; $i < count($indSql); $i++) {

                $ep = new stdClass();
                $ep->isEvidence = true;
                $ep->type = "evidencePackage";
                $ep->id = $indSql[$i]->evidence_id;
                $ep->sourceId = $indSql[$i]->evidence_source_id;
                $ep->sourceTitle = $indSql[$i]->evidence_source_name;
                $indicators[] = $ep;
                $ind = json_decode($indSql[$i]->content);
                $indcount = count($ind->indicatorMentions->sources);
                $ind = $ind->indicator;
                $ind->sourceCount = $indcount;
                $indicators[] = $ind;
            }
            $indicators = [$indicators];
        } catch (\Throwable $err) {
            echo $err;
            $indicators = [];
        }
    } else {
        die("Invalid Scoring Mechanism");
    }

    $resp = [];
    for ($i = 0; $i < count($indicators); $i++) {
        for ($j = 0; $j < count($indicators[$i]); $j++) {

            if ($indicators[$i][$j]->isEvidence === true) {
                $resp[] = $indicators[$i][$j];
            } else {

                $indicator = $indicators[$i][$j];

                $indicatorResults = new stdClass();
                $indicatorResults->type = "indicator";
                $indicatorResults->value =  $indicator->value->name;
                $indicatorResults->valtype =  $indicator->type;
                $indicatorResults->id =  $indicator->id;
                $indicatorResults->results = [];

                //This value will be passed into source count function by eval code

                $indicatorMentions = new stdClass();
                if ($hasMentions === true) {
                    $indicatorMentions->sourceCount =  $indicator->sourceCount;
                } else {
                    //Enables API call
                    $indicatorMentions->env = $env;
                    //Pass indicator ID
                    $indicatorMentions->id = $indicatorResults->id;
                }


                for ($k = 0; $k < count($rules); $k++) {
                    try {
                        $obj = new stdClass();
                        eval($rules[$k]->code);
                        $obj->type = "Rule";
                        $obj->id = $rules[$k]->rule_id;
                        $indicatorResults->results[] = $obj;
                    } catch (\Throwable $err) {
                        echo $err;
                    }
                }
                $resp[] =  $indicatorResults;
            }
        }
    }
    echo json_encode($resp);
    exit();
} catch (\Throwable $err) {
    echo $err;
}

function toString($ruleList)
{
    $str = "";
    for ($i = 0; $i < count($ruleList); $i++) {
        if ($i !== 0) $str .= " , ";
        $str .= " " . addslashes($ruleList[$i]) . " ";
    }
    return $str;
}

function listContains(&$db, $env, $value, $list)
{
    $sql = "SELECT id from ref_" . addslashes($list) . " where value = '" . addslashes($value) . "' LIMIT 1";
    try {
        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $err) {
        echo "    db insertion error\n";
        if (strpos($err->getMessage(), 'SQLSTATE[HY000]: General error: 2006') !== false) {
            echo ("     Timeout error...trying to save\n");
            try {
                $db = null;
                $db = $env->connectDB();
                $stmt = $db->query($sql);
                $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (PDOException $err) {
                echo ("     Timout save failed\n");
                echo ('{"error":{"message":' . $err->getMessage() . '}\n');
            }
        } else {
            echo ("     NOT A Timeout error\n");
            echo ('{"error":{"message":' . $err->getMessage() . ',"query":' . $sql . '}\n');
        }
    }
    return count($result) > 0;
}

function sourceCount($indicatorMentions, $n, $operation)
{
    $sources = isset($indicatorMentions->sourceCount) ? $indicatorMentions->sourceCount : json_decode(file_get_contents($indicatorMentions->env->api("sc", $indicatorMentions->id, $n, $operation)))->data;
    return eval("return(" .  $sources . "${operation} ${n});");
}


function post($url, $data)
{
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/json",
            'method'  => 'POST',
            'content' => json_encode(array('data' => $data))
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    return $result;
}

function blacklistContains($blacklist, $value, $type)
{
    for ($i = 0; $i < count($blacklist); $i++) {
        if ($blacklist[$i]->value === $value && $blacklist[$i]->ind_type === $type) {
            return true;
        }
    }
    return false;
}