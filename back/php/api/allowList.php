<?php

//This is an API endpoint that displays the blacklist in the DB

//allow cors
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');

//import db and env variables
require "./config.php";
$env = new env();
$db = $env->connectDB();

$myfile = fopen("allow_shortUrls.txt", "r") or die("Unable to open file!");
while(!feof($myfile)) {
    $line =   rtrim(ltrim(fgets($myfile)));
    $db->query("INSERT INTO ref_allowlist (value) VALUES ('".addslashes($line)."')");
    echo($line . "\n");
}