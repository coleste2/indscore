<?php

//This is an API endpoint to fetch all scored indicators from the DB

//allow cors
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Pragma, pragma, Origin, Content-Type, X-Auth-Token, X-Requested-With, content-type');

//get ENV variables and the DB 
require "./config.php";

if (!isset($_GET["prop"]) || !isset($_GET["contains"])) {
    die("Need prop and contains");
}
$prop = addslashes($_GET['prop']);
$contains = addslashes($_GET['contains']);
$binary = isset($_GET["matchCase"]);

$sql = "SELECT {$prop} FROM ais_indicator_scoring WHERE {$prop} LIKE ";
if ($binary === true) $sql .= "BINARY ";
$sql .= "'%{$contains}%' GROUP BY {$prop} LIMIT 50";

try {
    //connecy to DB
    $db = new env();
    $db = $db->connectDB();
    //Query db and fetch data
    $stmt = $db->query($sql);
    $propArr = $stmt->fetchAll(PDO::FETCH_OBJ);
    for ($i = 0; $i < count($propArr); $i++) {
        $propArr[$i] = $propArr[$i]->{$prop};
    }
    //Close Connection
    $db = null;

    //echo all scored indicators
    echo json_encode($propArr);
} catch (PDOException $err) {
    die (json_encode([]));
}
