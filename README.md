# This project is built with React and PHP

## Welcome to the master branch!

**Structure**
The front folder holds all front end files and the back folder holds all php backend files and all node DBS server files. Both of these parts have their respective documentation

**Local**
To run the project locally, you must first install node, npm, php, and some sort of server like apache.

1) In your linux terminal, install Node
cmd: $ curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

2) In your linux terminal, install npm, php, and apache
cmd: $ sudo apt install nodejs php apache

Install master branch or install dev branch
master branch cmd: $ git clone https://MatthewGFerrari@bitbucket.org/coleste2/indscore.git
dev branch cmd: $ git clone -b dev https://MatthewGFerrari@bitbucket.org/coleste2/indscore.git

Then, move everything inside the php folder of back end to a sub-directory of your apache server (preferably named indscoredev)

cmd: $ cp -Ra ./back/php/* /var/www/html/indscoredev

Then, ensure your db credentials are accurate

cmd: $ vim ./back/:q:src/config.php

Then, Create neccesary database schema via DBL documents in SQL folder of backend

Then, navigate to the node server on the back end and run the command 'npm install'

cmd: $ cd back/node
cmd: $ npm install

After node finishes installing all dependencies, run the command 'npm start'

cmd: $ npm start

Then, navigate to the font end folder and run the command 'npm install'

cmd: $ cd ../../front
cmd: $ npm install

After node finishes installing all dependencies, run the command 'npm start', and the app will open in your system default browser

cmd: $ npm start

**Deploy**
To deploy the project, you must generate a build on the the front end and combine it with the back end

1) Generate build on front
cmd: $ cd front
cmd: $ npm run build

2) Combine with backend
cmd: $ cp -Ra /var/www/html/indscoredev ./front/build

Then, ensure that router basename and .htaccess redirect link match and that the server is in production mode

1) In index.js of root->front->src->index.js, ensure that the router basename matches the desired deployment subdomain

2) In .htaccess of root->front->build->.htaccess, ensure that the final line folder name matches the desired deployment subdomain

3) Navigate to server config file and make sure env is set to production

cmd: $ vim ./back/:q:src/config.php

4) Then, zip and send build folder with API to MOE 

5) Add to apache directory and change permissions of all files except getIndicators because it will be called by the php cron and not apache

6) Then you must copy over all files in the node folder except the server file itself to a directory on MOE

7) Install the dependencies and add the routes file to the node server management tool in the /opt directory 

